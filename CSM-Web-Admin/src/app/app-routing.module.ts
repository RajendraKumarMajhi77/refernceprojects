import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent,AuthComponent} from './layouts';
const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',loadChildren: () => import('./dashboard/components/main/main.module').then(module => module.MainModule)
      },
      {
        path: 'module',children: [
          {
            path: 'school',loadChildren: () => import('./school-module/components/main/main.module').then(module => module.MainModule)
          },
          // {
          //   path: 'college',loadChildren: () => import('./school-module/components/main/main.module').then(module => module.MainModule)
          // },
          // {
          //   path: 'training-institute',loadChildren: () => import('./school-module/components/main/main.module').then(module => module.MainModule)
          // },
          // {
          //   path: 'coaching-institute',loadChildren: () => import('./school-module/components/main/main.module').then(module => module.MainModule)
          // },
        ]
      },
      {  path: '**',redirectTo: 'dashboard',pathMatch: 'full' },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
