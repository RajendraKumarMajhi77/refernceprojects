import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationItem } from './app-navigation';
import { AdminComponent, AuthComponent } from './layouts';
import {
  MainMenuComponent, InnerMainMenuComponent, InnerMainMenuItemComponent,
  InnerMainMenuGroupComponent, InnerMainMenuCollapseComponent, HeaderComponent,FooterComponent
} from './navigation';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent, AuthComponent,
    MainMenuComponent, InnerMainMenuComponent, InnerMainMenuItemComponent, InnerMainMenuGroupComponent, InnerMainMenuCollapseComponent, HeaderComponent,FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [NavigationItem],
  bootstrap: [AppComponent]
})
export class AppModule { }
