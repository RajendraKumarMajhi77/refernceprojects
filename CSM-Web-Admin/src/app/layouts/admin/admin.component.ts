import { Component, OnInit } from '@angular/core';
import { NavigationItem } from '../../app-navigation';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  public Navigation: any;

  constructor(public navigationItem: NavigationItem) {
    this.Navigation = this.navigationItem.get();
  }
  ngOnInit() {
  }
}
