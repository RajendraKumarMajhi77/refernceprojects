import { Component, Input, OnInit,ChangeDetectionStrategy ,ChangeDetectorRef} from '@angular/core';

@Component({
  selector: 'app-inner-main-menu',
  templateUrl: './inner-main-menu.component.html',
  styleUrls: ['./inner-main-menu.component.scss']
})
export class InnerMainMenuComponent implements OnInit {
  @Input() MenuItem :any;

  constructor() {

  }

  ngOnInit() {
    console.log(this.MenuItem)
  }
}
