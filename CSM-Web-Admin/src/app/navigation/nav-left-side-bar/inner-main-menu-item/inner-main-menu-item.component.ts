import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-inner-main-menu-item',
  templateUrl: './inner-main-menu-item.component.html',
  styleUrls: ['./inner-main-menu-item.component.scss']
})
export class InnerMainMenuItemComponent implements OnInit {
  @Input() item :any;
  constructor() { }

  ngOnInit() {
  }

}
