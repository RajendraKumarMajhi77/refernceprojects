import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-inner-main-menu-group',
  templateUrl: './inner-main-menu-group.component.html',
  styleUrls: ['./inner-main-menu-group.component.scss']
})
export class InnerMainMenuGroupComponent implements OnInit {
  @Input() item :any;
  constructor() { }

  ngOnInit() {
  }

}
