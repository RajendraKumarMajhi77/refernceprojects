import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-inner-main-menu-collapse',
  templateUrl: './inner-main-menu-collapse.component.html',
  styleUrls: ['./inner-main-menu-collapse.component.scss']
})
export class InnerMainMenuCollapseComponent implements OnInit {
  @Input() item :any;
  constructor() { }

  ngOnInit() {
  }

}
