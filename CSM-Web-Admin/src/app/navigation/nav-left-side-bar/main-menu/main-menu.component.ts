import { AfterViewInit, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit {
  @Input() Navigation :any;
  public MenuItem :any;
  id:number | undefined

  constructor() {

  }

  ngOnInit() {
    // when project load for the first time,
    // Then reflect only the dashboard menus.
    this.allowMenus(0);
  }

  allowMenus(index:number){
    this.MenuItem = (this.Navigation.length > 0) ? this.Navigation[index] : [];
  }

}
