import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main.component';
import { SchoolTypeListingComponent } from '..';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      { path: 'school-type',component: SchoolTypeListingComponent},




    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainRoutingModule { }
