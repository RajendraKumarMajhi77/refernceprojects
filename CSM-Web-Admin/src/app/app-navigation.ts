import {Injectable} from '@angular/core';

export interface NavigationItem {
  id: string;
  title: string;
  type: 'item' | 'collapse' | 'group';
  translate?: string;
  icon?: string;
  hidden?: boolean;
  url?: string;
  classes?: string;
  exactMatch?: boolean;
  external?: boolean;
  target?: boolean;
  breadcrumbs?: boolean;
  function?: any;
  badge?: {
    title?: string;
    type?: string;
  };
  children?: Navigation[];
}

export interface Navigation extends NavigationItem {
  children?: NavigationItem[];
}

const NavigationItems = [
  {
    id: 'dashboard',
    title: 'Dashboard',
    type: 'item',
    icon: 'ti-smart-home',
    children: [
      {
        id: 'analytics',
        title: 'Analytics',
        type: 'item',
        url: '/dashboard'
      }
    ]
  },
  {
    id: 'master',
    title: 'Master',
    type: 'item',
    icon: 'ti-apps',
    children: [
      {
        id: 'school',
        title: 'School',
        type: 'collapse',
        children: [
          {
            id: 'school-type',
            title: 'School Type',
            type: 'item',
            url: '/module/school/school-type'
          },
          {
            id: 'school-boards',
            title: 'School Boards',
            type: 'item',
            url: '/module/school/school-boards'
          },
          {
            id: 'school-boards',
            title: 'School Boards',
            type: 'item',
            url: '/module/school/school-boards'
          },
          {
            id: 'school-boards',
            title: 'School Boards',
            type: 'item',
            url: '/module/school/school-boards'
          }
        ]
      },
      {
        id: 'college',
        title: 'College',
        type: 'collapse'
      },
      {
        id: 'training-institute',
        title: 'Training Institute',
        type: 'collapse'
      },
      {
        id: 'coaching-institute',
        title: 'Coaching-Institute',
        type: 'collapse'
      }
    ]
  }
];

@Injectable()
export class NavigationItem {
  public get() {
    return NavigationItems;
  }
}
