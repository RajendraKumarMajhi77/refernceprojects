const nodemailer = require("nodemailer");
const config = require("../app/app.config")

// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
	host: config.maillHost,
	port: config.mailPortNo,
	//secure: config.mailSecure, // lack of ssl commented this. You can uncomment it.
	auth: {
		user: config.mailFrom,
		pass: config.mailPassword
	}
});

exports.send = function (_mailOptions) {	
	return transporter.sendMail(_mailOptions, (error, info) => {
		if (error) {
			return console.log('/....... Error ......../', error);
		}
		console.log('Message sent: %s', info.messageId);
		return info.messageId;
	});
};

/*
* * SAMPLE CODE REFERENCE ONLY  
send mail with defined transport object
visit https: //nodemailer.com/ for more options
return transporter.sendMail({
	from: config.mailFrom, // sender address e.g. no-reply@xyz.com or "Fred Foo 👥👻" <foo@example.com>
	to: to, // list of receivers e.g. bar@example.com, baz@example.com
	subject: subject, // Subject line e.g. 'Hello ✔'
	html: html // html body e.g. '<b>Hello world?</b>'
	//text: text, // plain text body e.g. Hello world?
}); */