const CryptoJS = require('crypto-js');
const config = require("../app/app.config");

exports.encrypt = function (textToEncrypt) {
    return CryptoJS.AES.encrypt(textToEncrypt, config.secretKey).toString();
};
exports.decrypt = function (textToDecrypt) {
    return CryptoJS.AES.decrypt(textToDecrypt, config.secretKey).toString(CryptoJS.enc.Utf8);
};