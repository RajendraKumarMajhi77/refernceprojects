const {
    body,
    query,
    param,
    check,
    validationResult
} = require("express-validator");
const mongoose = require("mongoose");
// Validate fields for different apis 
module.exports = {
    _idValidation: [
        check("_id").isMongoId().trim().withMessage("Invalid Id.")
    ],
    //=============================================================================================================
    //                                 ||    auth Validations||                                       ||
    //=============================================================================================================
    _signup: [
        body("firstName").isLength({
            min: 1
        }).trim().withMessage("First name must be specified.")
        .isAlphanumeric().withMessage("First name has non-alphanumeric characters."),
        body("lastName").isLength({
            min: 1
        }).trim().withMessage("Last name must be specified.")
        .isAlphanumeric().withMessage("Last name has non-alphanumeric characters."),
        body("email").isLength({
            min: 1
        }).trim().withMessage("Email must be specified.")
        .isEmail().withMessage("Email must be a valid email address."),
        body("contactNo").isLength({
            min: 10,
            max: 10
        }).trim().withMessage("ContactNo must be 10 digit.")
        .isNumeric().withMessage("ContactNo must be numeric."),
        body("password").isLength({
            min: 6
        }).trim().withMessage("Password must be 6 characters or greater."),
    ],
    _signin: [
        body("email").isLength({
            min: 1
        }).trim().withMessage("Email must be specified.")
        .isEmail().withMessage("Email must be a valid email address."),
        body("password").isLength({
            min: 1
        }).trim().withMessage("Password must be specified.")
    ],
    _logout: [
        body("logoutMode").isLength({
            min: 1
        }).trim().withMessage("logoutMode must be specified.\n ")
    ],
    _forgotpassword: [
        param("_email").isLength({
            min: 1
        }).trim().withMessage("Email must be specified.")
        .isEmail().withMessage("Email must be a valid email address.")
    ],
    // =========================================================================
    // ========================    Country state city Validations   ============
    // =========================================================================

    _getstatelist: [
        query("countryId").isLength({
            min: 1
        }).trim().withMessage("countryId must be specified.")
    ],
    _getcitylist: [
        query("stateId").isLength({
            min: 1
        }).trim().withMessage("stateId must be specified.")
    ],
    // =========================================================================
    // ========================    role Validations   ==========================
    // =========================================================================
    _addrole: [
        body("roleName").isLength({
            min: 1
        }).trim().withMessage("roleName must be specified."),
        body("roleShortName").isLength({
            min: 1
        }).trim().withMessage("roleShortName must be specified."),
        body("level").isLength({
            min: 1
        }).trim().withMessage("level must be specified."),
        body("userId").isLength({
            min: 1
        }).trim().withMessage("userId must be specified.")
    ],
    _updateroleliststatus: [
        body("statusType").isLength({
            min: 1
        }).trim().withMessage("statusType must be specified."),
        body("status").isLength({
            min: 1
        }).trim().withMessage("status must be specified."),
        body("userId").isLength({
            min: 1
        }).trim().withMessage("userId must be specified."),
        body("_id").isLength({
            min: 1
        }).trim().withMessage("_id must be specified.")
    ],

    // =========================================================================
    // ========================    role Validations   ==========================
    // =========================================================================

    _adstypemaster: [
        body("adsSectionId").isLength({
            min: 1
        }).trim().withMessage("ads SectionId must not be empty."),
        body("adsPositionsId").isLength({
            min: 1
        }).trim().withMessage("ads PositionsId must not be empty."),
        body("adsType").isLength({
            min: 1
        }).trim().withMessage("Ads Type must not be empty."),
        body("adsWidth").isNumeric({
            min: 1
        }).trim().withMessage("Ads Width must be numeric."),
        body("adsHeight").isNumeric({
            min: 1
        }).trim().withMessage("Ads Height must be numeric."),
        body("userId").isLength({
            min: 1
        }).trim().withMessage("userId must not be empty.")

    ],





















































    // =========================================================================
    // ==================    domain Listing Validations   ======================
    // =========================================================================

    _adddomain: [
        body("domainName").isLength({
            min: 1
        }).trim().withMessage("domainName must be specified."),
        body("url").isLength({
            min: 1
        }).trim().withMessage("Domain URL must be specified."),
        body("userId").isLength({
            min: 1
        }).trim().withMessage("userId must be specified.")
    ],
    _editdomain: [
        query("_id").isLength({
            min: 1
        }).trim().withMessage("_id must be specified.")
    ],
    _updatedomainliststatus: [
        body("statusType").isLength({
            min: 1
        }).trim().withMessage("statusType must be specified."),
        body("status").isLength({
            min: 1
        }).trim().withMessage("status must be specified."),
        body("userId").isLength({
            min: 1
        }).trim().withMessage("userId must be specified."),
        body("_id").isLength({
            min: 1
        }).trim().withMessage("_id must be specified.")
    ],

    // =========================================================================
    // ========================    add Size Validations   ======================
    // =========================================================================
    _adssizemaster: [
        body("adsPositionId").isLength({
            min: 1
        }).trim().withMessage("ads PositionId must not be empty."),
        body("adsType").isLength({
            min: 1
        }).trim().withMessage("Ads Type must not be empty."),
        body("adsWidth").isNumeric({
            min: 1
        }).trim().withMessage("Ads Width must be numeric."),
        body("adsHeight").isNumeric({
            min: 1
        }).trim().withMessage("Ads Height must be numeric."),
        body("userId").isLength({
            min: 1
        }).trim().withMessage("userId must not be empty.")

    ],
    _editadssize: [
        query("_id").isLength({
            min: 1
        }).trim().withMessage("_id must be specified.")
    ],
    _updateadssizeliststatus: [
        body("statusType").isLength({
            min: 1
        }).trim().withMessage("statusType must be specified."),
        body("status").isLength({
            min: 1
        }).trim().withMessage("status must be specified."),
        body("userId").isLength({
            min: 1
        }).trim().withMessage("userId must be specified."),
        body("_id").isLength({
            min: 1
        }).trim().withMessage("_id must be specified.")
    ],
    // =========================================================================
    // ========================    add menu Validations   ======================
    // =========================================================================
    _addmenu: [
        body("tittle").isLength({
            min: 1
        }).trim().withMessage("Menu must not be empty."),
        body("isGrouped").isLength({
            min: 1
        }).trim().withMessage("isGrouped must not be empty."),
    ],

    // =========================================================================
    // ======================    user Master Validations   =====================
    // =========================================================================
    _userregistration: [
        body("firstName").isLength({
            min: 1
        }).trim().withMessage("First name must be specified.")
        .isAlphanumeric().withMessage("First name has non-alphanumeric characters."),
        body("lastName").isLength({
            min: 1
        }).trim().withMessage("Last name must be specified.")
        .isAlphanumeric().withMessage("Last name has non-alphanumeric characters."),
        body("email").isLength({
            min: 1
        }).trim().withMessage("Email must be specified.")
        .isEmail().withMessage("Email must be a valid email address."),
        body("contactNo").isLength({
            min: 10,
            max: 10
        }).trim().withMessage("ContactNo must be 10 digit.")
        .isNumeric().withMessage("ContactNo must be numeric."),
        body("roleId").isLength({
            min: 10
        }).trim().withMessage("roleId must be specified."),
        //     body("status").isLength({
        //         min: 3
        //     }).trim().withMessage("1. Status must be specified.\n 2. Status must be in Upper Case. \n 3. By Default you can specify INACTIVE OR ACTIVE.")
        // 
    ],

    // ==============================================================================================
    // =====================    <><><><><> sECURITY mODULE Validations <><><><><>  ==================
    // ==============================================================================================

    // ======================    Role Permission Page all validations   ===================== //

    _updateRP: [
        body("_id").isLength({
            min: 16
        }).trim().withMessage("_id must be specified.")
        .withMessage("_id must be a 16 digit characters.")
    ],


    // ==============================================================================================
    // =====================    <><><><><> Masters mODULE Validations <><><><><>  ==================
    // ==============================================================================================

    // ======================    Ads  Locations Page all validations   ===================== //
    
    _getdomains: [
        query("userId").isLength({
            min: 1
        }).trim().withMessage("userId must be specified.").isMongoId().trim().withMessage("Invalid userId.")
    ],

    // ======================    Ads Payment Page all validations   ===================== //
    
    _updateadspayment: [
        body("_id").isLength({
            min: 16
        }).trim().withMessage("_id must be specified.")
        .withMessage("_id must be a 16 digit characters."),
        body("adsSizes_id").isLength({
            min: 16
        }).trim().withMessage("adsSizes_id must be specified.")
        .withMessage("adsSizes_id must be a 16 digit characters."),
        body("userId").isLength({
            min: 16
        }).trim().withMessage("userId must be specified.")
        .withMessage("userId must be a 16 digit characters."),
        body("Payment").isLength({
            min: 1
        }).trim().withMessage("Payment must be specified.")
    ],

       // ======================    Ads Payment Page all validations   ===================== //

       _updateadssubscription: [
        body("_id").isLength({
            min: 16
        }).trim().withMessage("_id must be specified.")
        .withMessage("_id must be a 16 digit characters."),
        body("adsSizes_id").isLength({
            min: 16
        }).trim().withMessage("adsSizes_id must be specified.")
        .withMessage("adsSizes_id must be a 16 digit characters."),
        body("userId").isLength({
            min: 16
        }).trim().withMessage("userId must be specified.")
        .withMessage("userId must be a 16 digit characters."),
        body("Payment").isLength({
            min: 1
        }).trim().withMessage("Payment must be specified.")
    ],
}