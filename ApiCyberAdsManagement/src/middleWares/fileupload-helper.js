//===== require file uploading dependency 
let path = require('path');
let fs = require('fs');
var mv = require('mv');
const _ = require('lodash');


module.exports.uploadSingleFile = function (req,subDirPath,imageLink) {
    try {
      
        // Check Root folder exists or not
        var _rootDirExists = fs.existsSync("wwwroot");
        // if storage directory not exist
        if (!_rootDirExists) {
            fs.mkdirSync("wwwroot");
        } else {
            // add new sub Directory in storage directory
            var _dirName = path.join(__dirname, "../../wwwroot", subDirPath);
            // check sub directory name exist or not
            var _isDirExist = fs.existsSync(_dirName);
            // get the extension type of req file and merge with timestamp and get the name of file
            
            var _name = imageLink + "_" + req.files.file.name;
            // console.log(_name);
            _responsePath = path.join("../../wwwroot", "../../wwwroot/" + subDirPath, _name)
            // if directory not exist then create the join with _name

            if (!_isDirExist) {
                fs.mkdirSync(_dirName);
            }
            var _dirName = path.join(__dirname, "../../wwwroot", subDirPath);

            // if directory exist then join with _name
            var _filepath = path.join(_dirName, _name);
            console.log(_responsePath, _filepath);
            var _fileurl = req.protocol + "://" + req.headers.host + "/" + subDirPath + "/" + _name;
            console.log(_fileurl);

            const responseData = {
                _fileName: req.files.file.name,
                _fileUrl: _fileurl
            }

            //req.files.file.mv(_filepath);
            return new Promise((resolve, reject) => {
                req.files.file.mv(_filepath, (err) => {
                    if (err) {
                        resolve(false);
                    } else {
                        resolve(responseData);
                    }
                })
            });
        }
    } catch (err) {
        //res.status(500).send(err);
    }
}
module.exports.uploadMultipleFile = function (req, subDirPath) {
    try {
        var _rootDirExists = fs.existsSync("wwwroot");
        if (!_rootDirExists) {
            fs.mkdirSync("wwwroot");
        } else {
            var _dirName = path.join(__dirname, "../../wwwroot", subDirPath);
            var _isDirExist = fs.existsSync(_dirName);
            if (!_isDirExist) {
                fs.mkdirSync(_dirName);
            }
            var _dirName = path.join(__dirname, "../../wwwroot", subDirPath);

            let responseData = [];
            return new Promise((resolve, reject) => {
                _.forEach(_.keysIn(req.files.file), (key) => {
                    let _file = req.files.file[key];                    
                    console.log('file', file)
                    var _name = JSON.stringify(new Date().getTime()) + "_" + _file.name;
                    var _filepath = path.join(_dirName, _name);
                    var _fileurl = req.protocol + "://" + req.headers.host + "/" + subDirPath + "/" + _name;
                    //UPLOADING THE FILE 
                    req.files.file[key].mv(_filepath);

                    //push file details
                    responseData.push({
                        _fileName: _file.name,
                        _fileUrl: _fileurl
                    });
                });
                if(!responseData){
                    resolve(false);
                }else{
                    resolve(responseData); 
                }  
            });
        }
    } catch (err) {
        //res.status(500).send(err);
    }
}


// const imageFilter = function(req, file, cb) {
//     // Accept images only
//     if (!file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)) {
//         req.fileValidationError = 'Only image files are allowed!';
//         return cb(new Error('Only image files are allowed!'), false);
//     }
//     cb(null, true);
// };
// exports.imageFilter = imageFilter;