let jwt = require("jsonwebtoken");
const { promisify } = require("util");
const config = require("../app/app.config");
const apiResponse = require("../utils/apiResponse");
const UserModel = require("../models/user");


module.exports.jwtauthenticate = function (req, res, next) {
    // GEt authorication token from header
    let _token;
    if ( req.headers.authorization && req.headers.authorization.startsWith("Bearer")){
        _token = req.headers.authorization.split(" ")[1];
      }
    let _secret = config.JWT_SECRET;
    if (_token) { // if token exist then verify the token and authenticate 
        jwt.verify(_token, _secret, function (err, decoded) {
            if (err) {
                return apiResponse.tokenAuthResponse(res, "Failed to authenticate token.");
            } else {    
               //const decode = await promisify(jwt.verify)(token, process.env.JWT_SECRET);
                req.decoded = decoded;
                // use For testing 
                let decodedData = {
                    _id: req.decoded._id,
                    fullName: req.decoded.fullName,
                    firstName: req.decoded.firstName,
                    lastName: req.decoded.lastName,
                    email: req.decoded.email,
                    contactNo: req.decoded.contactNo,
                    roleName: req.decoded.roleName,
                    iat: new Date((req.decoded.iat) * 1000),
                    exp: new Date((req.decoded.exp) * 1000)
                };
                console.log('/-------- auth token decoded --------/\n', decodedData);

                let query = UserModel.findById(req.decoded._id);
                query.exec((err, successData) => {
                    if (err) {
                        // server error if any error occured at query time
                        return apiResponse.ErrorResponse(res, "Server Error.");
                    } else if (!successData) {
                        return apiResponse.tokenAuthResponse(res, "Invalid auth token.");
                    } else {
                        //console.log(successData)
                        if (successData.status == 'ACTIVE') { // process next() function if user active
                            next();
                        } else if (successData.status == 'INACTIVE' || 'DELETED' || 'BLOCKED') {
                            // Stop the process if user status is pending
                            return apiResponse.tokenAuthResponse(res, "Sorry ! Your  account is " + successData.status.toLowerCase());
                        } else {
                            return apiResponse.tokenAuthResponse(res, "Something went wrong.");
                            //console.log('Something went wrong'); if status not match between active or pending
                        }
                    }
                })
            }
        });
    } else { // if token not exist then return to client token not provided
        return apiResponse.tokenAuthResponse(res, "No token provided");
    }
}

exports.generateJWTToken =  function(userData) {
    //Prepare JWT token for authentication
    const jwtPayload = userData;
    const jwtData = {
        expiresIn: config.JWT_TIMEOUT_DURATION,
    };
    const secret = config.JWT_SECRET;
    //Generated JWT token with Payload and secret.
    const token = jwt.sign(jwtPayload, secret, jwtData);
    return  String(token);
};

exports.decodeJWTToken =  function(_token) {
    const _secret = config.JWT_SECRET;
    jwt.verify(_token, _secret, function (err, decoded) { 
        
     console.log('status',decoded);
     });
};

// module.exports = jwtauthenticate;

// function jwtauthenticate() {
//     const { jwt_secret } = config;
//     return expressJwt({ jwt_secret, algorithms: ['HS256'] });
// }