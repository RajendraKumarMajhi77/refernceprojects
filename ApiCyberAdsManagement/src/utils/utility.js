const jwt = require("jsonwebtoken");
const config = require("../app/app.config")

exports.randomNumber = function (length) {
    var text = "";
    var possible = "123456789";
    for (var i = 0; i < length; i++) {
        var sup = Math.floor(Math.random() * possible.length);
        text += i > 0 && sup == i ? "0" : possible.charAt(sup);
    }
    return Number(text);
};


exports.generateRandomString = function (length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}


exports.getLevelId = function (level) {
    var levelId = 0;
    if (level == 'Level 1') {
        levelId = 1;
    } else if (level == 'Level 2') {
        levelId = 2;
    } else if (level == 'Level 3') {
        levelId = 3;
    } else if (level == 'Level 4') {
        levelId = 4;
    } else if (level == 'Level 5') {
        levelId = 5;
    } else {
        levelId = 0;
    }
    return levelId;
};

exports.getPaymentStatus = function (status) {
    var status = "";
    if (status == 'success') {
        status = "SUCCESS";
    } else if (status == 'money settled') {
        status = "SUCCESS";
    } else if (status == 'settlement in process') {
        status = "SUCCESS";
    } else if (status == 'failure') {
        status = "FAILED";
    } else {
        status = "FAILED";
    }
    return status;
}

exports.getExpireDate = function (plan) {
    var ExpiredDate = Date;
    if (plan == 'Monthly') {
        ExpiredDate =  new Date( new Date().getFullYear(),new Date().getMonth() + 1, new Date().getDate() );
    } else if (plan == 'Quarterly') {
        ExpiredDate =  new Date( new Date().getFullYear(),new Date().getMonth() + 3, new Date().getDate() );
    } else if (plan == 'Yearly') {
        ExpiredDate =  new Date( new Date().getFullYear() + 1,new Date().getMonth(), new Date().getDate() );
    }
    return ExpiredDate;
}

exports.SeqOutputNumber =  function (_prefix,_length,_targetLength,isInc) {
    var number = 0;
    if(isInc == false){
        number = _length;
    }else{ number = _length + 1 ;}
    var targetLength = _targetLength;
    var RandomNumber = number + '';
    while (RandomNumber.length < targetLength) {
        RandomNumber = '0' + RandomNumber;
    }
    var output = (_prefix + RandomNumber).toString();
    return output;
}

