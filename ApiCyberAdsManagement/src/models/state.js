'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	stateSchema = new Schema({
        stateId:{ 
            type: Number
        },
		stateName: {
			type: String
		},
		countryId: {
			type: Number
        },
        isActive:{
            type: Boolean,
            default: true
        },
        isDeleted:{
            type: Boolean,
            default: false
        }
	}, {
		timestamps: true
	});
var state = mongoose.model('state', stateSchema, 'state');
module.exports = state;