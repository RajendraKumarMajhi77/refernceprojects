
'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    menuMasterSchema = new Schema({
        level: {
            type: String
        },
        tittle: {
            type: String
        },
        isGrouped: {
            type: Boolean,
            default: false
        },
        position: {
            type: String
        },
        icon: {
            type: String
        },
        url: {
            type: String
        },
        permissions: [
            {
                roleId: {
                    type: Schema.Types.ObjectId,
                    ref: 'role'
                }
            }
        ],
        children: [{
            level: {
                type: String,
                default: ''
            },
            tittle: {
                type: String
            },
            icon: {
                type: String
            },
            url: {
                type: String
            },
            isActive: {
                type: Boolean,
                default: true
            },
            isDeleted: {
                type: Boolean,
                default: false
            },lastModified: {
                type: Date,
                default: new Date()
            },
            permissions: [
                {
                    roleId: {
                        type: Schema.Types.ObjectId,
                        ref: 'role'
                    }
                }
            ]   
        }],
        isActive: {
            type: Boolean,
            default: true
        },
        isDeleted: {
            type: Boolean,
            default: false
        }
    }, {
        timestamps: true
    });
var menuMaster = mongoose.model('menuMaster', menuMasterSchema, 'menuMaster');
module.exports = menuMaster;