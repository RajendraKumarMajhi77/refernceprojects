'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    iconMasterSchema = new Schema({
        iconClass: {
            type: String
        },
        iconName: {
            type: String
        },
        iconCode: {
            type: Boolean,
            default: false
        }
    }, {
        timestamps: true
    });
var iconMaster = mongoose.model('iconMaster', iconMasterSchema, 'iconMaster');
module.exports = iconMaster;