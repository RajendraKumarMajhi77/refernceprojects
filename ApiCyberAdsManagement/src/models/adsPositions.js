'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	adsPositionsSchema = new Schema({
		adsPositions: {
			type: String,
			trim: true
		},
        isActive:{
            type: Boolean,
            default: true
        },
        isDeleted:{
            type: Boolean,
            default: false
        }
	}, {
		timestamps: true
	});
var adsPositions = mongoose.model('adsPositions', adsPositionsSchema, 'adsPositions');
module.exports = adsPositions;