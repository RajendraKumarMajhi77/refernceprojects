'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	citySchema = new Schema({
        cityId:{ 
            type: Number
        },
		cityName: {
			type: String
		},
		stateId: {
			type: Number
        },
        isActive:{
            type: Boolean,
            default: true
        },
        isDeleted:{
            type: Boolean,
            default: false
        }
	}, {
		timestamps: true
	});
var city = mongoose.model('city', citySchema, 'city');
module.exports = city;