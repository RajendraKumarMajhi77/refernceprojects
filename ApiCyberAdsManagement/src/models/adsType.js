'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	adsTypeSchema = new Schema({
        adsSectionId:{
            type : Schema.Types.ObjectId , ref:'adsSection'
        },
        adsPositionsId: {
			type : Schema.Types.ObjectId , ref:'adsPositions'
        },
		adsType: {
			type: String,
			trim: true
		},
		adsWidth: {
            type: String,
			default: '0'
		},
		adsHeight: {
            type: String,
			default: '0'
        },
        isActive:{
            type: Boolean,
            default: true
        },
        isDeleted:{
            type: Boolean,
            default: false
        },
        createdBy: {
			type : Schema.Types.ObjectId , ref:'user'
        },
        updatedBy:{
            type : Schema.Types.ObjectId , ref:'user'
        },
        deletedBy:{
            type : Schema.Types.ObjectId , ref:'user'
        },
        deletedOn: {
            type : Date
        }
	}, {
		timestamps: true
	});
var adsType = mongoose.model('adsType', adsTypeSchema, 'adsType');
module.exports = adsType;