'use strict';
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    adsLocationSchema = new Schema({
        pageLevel: {
            type: String,
            default: ''
        },
        pageName: {
            type: String,
            default: ''
        },
        pageUrl: {
            type: String,
            default: ''
        },
        parentId: {
            type: String,
            default: ''
        },
        sequence: [{
            index: {
                type: Number
            }
        }],
        isActive: {
            type: Boolean,
            default: true
        },
        isDeleted: {
            type: Boolean,
            default: false
        },
        createdBy: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        updatedBy: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        deletedBy: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        deletedOn: {
            type: Date
        }
    }, {
        timestamps: true
    });
var adsLocation = mongoose.model('adsLocation', adsLocationSchema, 'adsLocation');
module.exports = adsLocation;