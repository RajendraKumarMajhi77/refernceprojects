'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	countrySchema = new Schema({
        countryId:{ 
            type: Number
        },
		countryName: {
			type: String
		},
		sortName: {
			type: String
        },
        phoneCode: {
			type: String
        },
        isActive:{
            type: Boolean,
            default: true
        },
        isDeleted:{
            type: Boolean,
            default: false
        }
	}, {
		timestamps: true
	});
var country = mongoose.model('country', countrySchema, 'country');
module.exports = country;