'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	forgotPasswordSchema = new Schema({
        userId: {
			type : Schema.Types.ObjectId , ref:'user'
        },
		requestKey: {
			type: String
        },
        isVerified: {
            type: Boolean,
            default: false
        }
	}, {
		timestamps: true
	});
var forgotPassword = mongoose.model('forgotPassword', forgotPasswordSchema, 'forgotPassword');
module.exports = forgotPassword;