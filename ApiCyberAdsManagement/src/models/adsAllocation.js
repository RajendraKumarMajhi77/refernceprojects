'use strict';
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    adsAllocationSchema = new Schema({
        pageLevel: {
            type: String,
            default: ''
        },
        parentId: {
            type: Schema.Types.ObjectId
        },
        allocationDetails: [{
            adsTypeId: {
                type: Schema.Types.ObjectId,
                ref: 'adsType'
            }
        }],
        isActive: {
            type: Boolean,
            default: true
        },
        isDeleted: {
            type: Boolean,
            default: false
        },
        createdBy: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        updatedBy: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        deletedBy: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        deletedOn: {
            type: Date
        }
    }, {
        timestamps: true
    });
var adsAllocation = mongoose.model('adsAllocation', adsAllocationSchema, 'adsAllocation');
module.exports = adsAllocation;