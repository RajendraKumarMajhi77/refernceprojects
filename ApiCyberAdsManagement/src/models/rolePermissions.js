'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	rolePermissionsSchema = new Schema({
		roleId: {
            type : Schema.Types.ObjectId , ref:'role'
        },
		menuId: {
            type : Schema.Types.ObjectId , ref:'menu'
        },
		isGrouped: {            
			type : Boolean
		},
        children : [{
            menuId: {
                type : Schema.Types.ObjectId 
            }
        }]
	}, {
		timestamps: true
	});
var rolePermissions = mongoose.model('rolePermissions', rolePermissionsSchema, 'rolePermissions');
module.exports = rolePermissions;