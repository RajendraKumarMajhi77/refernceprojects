'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	userPasswordSchema = new Schema({
        userId: {
			type: Schema.Types.ObjectId,
			ref: 'user'
		},
		originalPassword: {
			type: String
		}
	}, {
		timestamps: true
	});

var userPassword = mongoose.model('userPassword', userPasswordSchema, 'userPassword');

module.exports = userPassword;