'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	domainSchema = new Schema({
		domainName: {
			type: String
		},
		url: {
			type: String
        },
        key: {
			type: String
        },
        isAllowed:{
            type: Boolean,
            default: false
        },
        isActive:{
            type: Boolean,
            default: true
        },
        isDeleted:{
            type: Boolean,
            default: false
        },
        createdBy: {
			type : Schema.Types.ObjectId , ref:'user'
        },
        updatedBy:{
            type : Schema.Types.ObjectId , ref:'user'
        },
        deletedBy:{
            type : Schema.Types.ObjectId , ref:'user'
        },
        deletedOn: {
            type : Date
        }
	}, {
		timestamps: true
	});
var domain = mongoose.model('domain', domainSchema, 'domain');
module.exports = domain;