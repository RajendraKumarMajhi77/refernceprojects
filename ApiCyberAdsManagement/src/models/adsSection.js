'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	adsSectionSchema = new Schema({
		adsSection: {
			type: String,
			trim: true
		},
        isActive:{
            type: Boolean,
            default: true
        },
        isDeleted:{
            type: Boolean,
            default: false
        }
	}, {
		timestamps: true
	});
var adsSection = mongoose.model('adsSection', adsSectionSchema, 'adsSection');
module.exports = adsSection;