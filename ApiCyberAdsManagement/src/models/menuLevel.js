'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    menuLevelSchema = new Schema({
        menuLevelId: {
            type: Number
        },
        menuLevel: {
            type: String
        }
    }, {
        timestamps: true
    });
var menuLevel = mongoose.model('menuLevel', menuLevelSchema, 'menuLevel');
module.exports = menuLevel;