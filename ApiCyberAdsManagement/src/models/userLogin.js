'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    usersLoginSchema = new Schema({
        userId: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        token: {
            type: String
        },
        tokenTimeOut: {
            type: Number
        },
        ipAdress: {
            type: String
        },
        ispIpAdress: {
            type: String
        },
        lat: {
            type: Number,
            default: 0
        },
        lon: {
            type: Number,
            default: 0
        },
        os: {
            type: String,
            default: ''
        },
        osVersion: {
            type: String
        },
        browser: {
            type: String
        },
        browserVersion: {
            type: String
        },
        userAgent: {
            type: String
        },
        deviceType: {
            type: String
        },
        city: {
            type: String
        },
        country: {
            type: String
        },
        timezone: {
            type: String
        },
        zip: {
            type: String
        },
        logoutDate: {
            type: Date
        },
        logoutMode: {
            type: String
        },
        otp: {
            type: String,
            default: '101010'
        }
    }, {
        timestamps: true
    });
var userlogin = mongoose.model('userlogin', usersLoginSchema, 'userlogin');

module.exports = userlogin;