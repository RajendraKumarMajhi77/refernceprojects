'use strict';
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    adsCenterSchema = new Schema({
        userId: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        adsLocationId: {
            type: Schema.Types.ObjectId,
            ref: 'adsLocation'
        },
        adsDetails: [{
            adsCode:{
                type: String,
                default: ''
            },
            adsTypeId: {
                type: Schema.Types.ObjectId,
                ref: 'adsType'
            },
            adsTittle: {
                type: String,
                default: ''
            },
            adsRefUrl:{
                type: String,
                default: ''
            },
            fileName: {
                type: String,
                default: ''
            },
            fileUrl: {
                type: String,
                default: ''
            },
            isActive: {
                type: Boolean,
                default: true
            },
            seq:{
                type: Number,
                default: 0
            },
            serchSeq:{
                type: Number,
                default: 0
            },
            serchSeqDetail:{
                type: Number,
                default: 0
            },
            clickDetails:[{
                country: {
                    type: String,
                    default: ''
                },
                countryCode: {
                    type: String,
                    default: ''
                },
                region: {
                    type: String,
                    default: ''
                },
                regionName: {
                    type: String,
                    default: ''
                },
                city: {
                    type: String,
                    default: ''
                },
                zip: {
                    type: String,
                    default: ''
                },
                lat: {
                    type: String,
                    default: ''
                },
                lon: {
                    type: String,
                    default: ''
                },
                timezone: {
                    type: String,
                    default: ''
                },
                isp: {
                    type: String,
                    default: ''
                },
                org: {
                    type: String,
                    default: ''
                },
                as: {
                    type: String,
                    default: ''
                },
                query:{
                    type: String,
                    default: ''
                },
                sourceUrl : {
                    type: String,
                    default: ''
                },
                destUrl : {
                    type: String,
                    default: ''
                },
                clickTime : {
                    type: Date,
                    default: Date.now()
                }
            }]
        }],
        expiredOn: {
            type: Date
        },
        publishedOn: {
            type: Date
        },
        status: {
			type: String,
			default: 'PENDING',
			uppercase: true
			// PROCESSED , PAID
		},
        isActive: {
            type: Boolean,
            default: true
        },
        isDeleted: {
            type: Boolean,
            default: false
        },
        updatedBy: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        deletedBy: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        deletedOn: {
            type: Date
        }
    }, {
        timestamps: true
    });
var adsCenter = mongoose.model('adsCenter', adsCenterSchema, 'adsCenter');
module.exports = adsCenter;