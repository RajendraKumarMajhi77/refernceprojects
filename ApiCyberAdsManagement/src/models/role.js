'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	roleSchema = new Schema({
		roleName: {
            type: String
        },
        roleShortName: {
            type: String
        },
        level: {
            type: String
        },
        levelId: {
            type: Number,
            default: 1
        },
        isActive:{
            type: Boolean,
            default: true
        },
        isDeleted:{
            type: Boolean,
            default: false
        },
        createdBy: {
			type : Schema.Types.ObjectId , ref:'user'
        },
        updatedBy:{
            type : Schema.Types.ObjectId , ref:'user'
        },
        deletedBy:{
            type : Schema.Types.ObjectId , ref:'user'
        },
        deletedOn: {
            type : Date
        }
	}, {
		timestamps: true
	});
var role = mongoose.model('role', roleSchema, 'role');
module.exports = role;