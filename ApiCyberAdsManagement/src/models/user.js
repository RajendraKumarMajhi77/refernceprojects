'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	usersSchema = new Schema({
		firstName: {
			type: String,
			default: ''
		},
		lastName: {
			type: String,
			default: ''
		},
		email: {
			type: String,
			default: ''
		},
		contactNo: {
			type: String,
			default: ''
		},
		alternateContactNo: {
			type: String,
			default: ''
		},
		userName: {
			type: String,
			default: ''
		},
		encyptedPassword: {
			type: String
		},
		resetPasswordCount:{
			type:Number,
			default: 0
		},
		roleId: {
			type: Schema.Types.ObjectId,
			ref: 'role'
		},
		status: {
			type: String,
			default: 'INACTIVE',
			uppercase: true
			// INACTIVE , ACTIVE ,DELETED, BLOCKED
		},
		WebUserId:{
			type:Number,
			default: 0
		},
		permanentAddress: {
            countryId: {
				type: Schema.Types.ObjectId,
				ref: 'country'
            },
            stateId: {
				type: Schema.Types.ObjectId,
				ref: 'state'
            },
            cityId: {
				type: Schema.Types.ObjectId,
				ref: 'city'
            },
			address: {				
				type: String,
				default: ''
			},
			zipCode:{
				type: String,
				default: ''
			}
        },
		source: {
			type: String,
			default: ''
		},
		hostUrl:{
			type: String,
			default: ''
		},
		entityDetails: {
			entityType: {
				type: String
            },
            instituteId: {
				type:Number
            },
            typeId: {
				type:Number
            },
            instituteName: {
				type: String
            },
			instituteCode:{				
				type: String
			},
			instituteEmail:{				
				type: String
			},
			instituteContactNo:{				
				type: String
			}
        },
		createdBy: {
			type: Schema.Types.ObjectId,
			ref: 'user'
		},
		updatedBy: {
			type: Schema.Types.ObjectId,
			ref: 'user'
		}
	}, {
		timestamps: true
	});

// Virtual for user's full name
usersSchema
	.virtual("fullName")
	.get(function () {
		return this.firstName + " " + this.lastName;
	});
var user = mongoose.model('user', usersSchema, 'user');

module.exports = user;