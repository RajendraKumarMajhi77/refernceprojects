const bcrypt = require("bcrypt");
const {
    await
} = require('asyncawait');
var mongoose = require('mongoose');
//const mailer = require("../utils/mailer");

const utility = require("../../utils/utility");
const apiResponse = require("../../utils/apiResponse");
let _jwt = require("../../middleWares/jwt");
const UserModel = require("../../models/user");
const UserLoginModel = require("../../models/userLogin");
const forgotPasswordModel = require("../../models/forgotPassword");
const userPasswordModel = require("../../models/userPassword");
const roleModel = require("../../models/role");
const menuMasterModel = require("../../models/menuMaster");
const countryModel = require("../../models/country");
const stateModel = require("../../models/state");
const cityModel = require("../../models/city");

exports.getCountryList = async function (req) {
    try {
        var condition = {
            isActive: true,
            isDeleted: false
        };
        return new Promise((resolve, reject) => {
            countryModel.find(condition, {
                countryId: 1,
                countryName: 1
            }, (err, success) => {
                if (!success) {
                    resolve(false);
                } else {
                    resolve(success);
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};

exports.getStateList = async function (req) {
    try {
        var condition = {
            "$and": [{
                "countryId": req.query.countryId
            }, {
                "isActive": true
            }, {
                "isDeleted": false
            }, ]
        }
        return new Promise((resolve, reject) => {
            stateModel.find(condition, {
                stateId: 1,
                stateName: 1
            }, (err, success) => {
                if (!success) {
                    resolve(false);
                } else {
                    resolve(success);
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};

exports.getCityList = async function (req) {
    try {
        var condition = {
            "$and": [{
                "stateId": req.query.stateId
            }, {
                "isActive": true
            }, {
                "isDeleted": false
            }, ]
        }
        return new Promise((resolve, reject) => {
            cityModel.find(condition, {
                cityId: 1,
                cityName: 1
            }, (err, success) => {
                if (!success) {
                    resolve(false);
                } else {
                    resolve(success);
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};

exports.CreateUser = async function (req, hashpsw, autopsw) {
    try {
        let reqObj = new UserModel({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            contactNo: req.body.contactNo,
            userName: req.body.email,
            hashPassword: hashpsw,
            originalPassword: autopsw,
            roleId: req.body.roleId,
            address: req.body.address,
            status: 'INACTIVE',
            createdBy: req.body.userId,
            source: req.body.source
        });
        return new Promise((resolve, reject) => {
            reqObj.save((err, success) => {
                if (!success) {
                    resolve(false);
                } else {
                    resolve(success);
                }
            });
        });
    } catch (e) {
        throw e;
    }

}

exports.FindUserExistOrNot = async function (email) {
    try {
        return new Promise((resolve, reject) => {
            UserModel.findOne({
                email: email
            }, {}, (err, success) => {
                if (!success) {
                    resolve(false);
                } else {
                    resolve(success)
                }
            });
        });
    } catch (e) {
        console.log('service error\n', e);
        throw e;
    }
};

exports.generateForgotPasswordRequestKey = async function (_id, email) {
    try {
        return new Promise((resolve, reject) => {
            let objData = new forgotPasswordModel({
                userId: _id,
                requestKey: email + ':' + utility.generateRandomString(35)
            });
            objData.save(function (err, success) {
                if (!success) {
                    resolve(false);
                } else {
                    resolve(success);
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};

exports.matchPassword = async function (reqPassword, fetchedPassword) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(reqPassword, fetchedPassword, function (err, passwordmatched) {
            if (passwordmatched) {
                resolve(true);
            } else {
                resolve(false);
            }
        });
    });
};
exports.createToken = async function (payload) {
    return new Promise((resolve, reject) => {
        roleModel.findOne({
            _id: payload.roleId
        }, {}, (err, roledata) => {
            if (roledata) {
                const JwtPayload = {
                    fullName: payload.fullName,
                    firstName: payload.firstName,
                    lastName: payload.lastName,
                    email: payload.email,
                    roleName: roledata.roleName,
                    contactNo: payload.contactNo,
                    _id: payload._id,
                };
                JwtPayload.token = _jwt.generateJWTToken(JwtPayload);
                resolve(JwtPayload);
            }
        });
    });
};

exports.saveuserlogin = async function (req, res) {
    try {
        let reqObjData = new UserLoginModel({
            userId: req.body.userId,
            token: req.body.token,
            tokenTimeOut: req.body.tokenTimeOut,
            ipAdress: req.body.ipAdress,
            ispIpAdress: req.body.ispIpAdress,
            lat: req.body.lat,
            lon: req.body.lon,
            os: req.body.os,
            osVersion: req.body.osVersion,
            browser: req.body.browser,
            browserVersion: req.body.browserVersion,
            userAgent: req.body.userAgent,
            deviceType: req.body.deviceType,
            city: req.body.city,
            country: req.body.country,
            timezone: req.body.timezone,
            zip: req.body.zip
        });
        return new Promise((resolve, reject) => {
            reqObjData.save((err, savedData) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(savedData);
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};

exports.logout = async function (req, res) {
    try {
        //update user login collection with logout details.
        // |_id is the IDA value stored in local storage . |
        var data = {
            logoutDate: new Date().getTime(),
            logoutMode: req.body.logoutMode
        }
        return new Promise((resolve, reject) => {
            let query = UserLoginModel.findByIdAndUpdate(req.body._id, data, {
                new: true
            });
            query.exec((err, success) => {
                if (err) {
                    reject(Error(err));
                } else if (success) {
                    // console.log(success);
                    resolve(success);
                } else {
                    reject(Error("Something went wrong."));
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};


exports.checkRequestKey = async function (_reqKey) {
    try {
        return new Promise((resolve, reject) => {
            forgotPasswordModel.findOne({
                requestKey: _reqKey
            }, {}, (err, success) => {
                if (!success) {
                    resolve(false);
                } else {
                    resolve(success);
                }
            });
        });
    } catch (e) {
        console.log('service error\n', e);
        throw e;
    }
};

exports.incUpdatePassword = async function (userId, STR_Hash) {
    try {
        return new Promise((resolve, reject) => {
            UserModel.findByIdAndUpdate({
                _id: userId
            }, {
                encyptedPassword: STR_Hash,
                $inc: {
                    resetPasswordCount: 1
                }
            }, (err, success) => {
                if (err || !success) {
                    resolve(false);
                } else {
                    resolve(true);
                }
            });
        });
    } catch (e) {
        console.log('service error\n', e);
        throw e;
    }
};

exports.updateOriginalPassword = async function (userId, STR_Random) {
    try {
        console.log('updateOrgPass', STR_Random);
        return new Promise((resolve, reject) => {
            userPasswordModel.findOneAndUpdate({
                userId: mongoose.Types.ObjectId(userId)
            }, {
                originalPassword: STR_Random
            }, (err, success) => {
                if (err || !success) {
                    resolve(false);
                } else {
                    resolve(true);
                }
            });
        });
    } catch (e) {
        console.log('service error\n', e);
        throw e;
    }
};


exports.findUserRoleDetails = async function (userId) {
    return new Promise((resolve, reject) => {
        const aggregate = UserModel.aggregate([{
            $match: {
                '_id': mongoose.Types.ObjectId(userId)
            }
        }, {
            $lookup: {
                from: 'role',
                localField: 'roleId',
                foreignField: '_id',
                as: 'roleCollection'
            }
        }, {
            $unwind: {
                path: '$roleCollection',
                preserveNullAndEmptyArrays: true
            }
        }, {
            $project: {
                'roleId': '$roleCollection._id',
                'roleName': '$roleCollection.roleName',
                'levelId': '$roleCollection.levelId'
            }
        }]);
        aggregate.exec((err, success) => {
            if (err) {
                throw (Error(err));
            } else if (!success) {
                resolve(false);
            } else {
                resolve(success)
            }
        });
    });
};
exports.getAssignedMenu = async function (roleId) {
    return new Promise((resolve, reject) => {
        const aggregate = menuMasterModel.aggregate([{
            $match: {
                "permissions.roleId": mongoose.Types.ObjectId(roleId)
            }
        }, {
            $match: {
                "children.permissions.roleId": mongoose.Types.ObjectId(roleId)
            }
        }, {
            $unwind: {
                path: '$children',
                preserveNullAndEmptyArrays: true
            }
        }, {
            $unwind: {
                path: '$children.permissions',
                preserveNullAndEmptyArrays: true
            }
        }, {
            $match: {
                "children.permissions.roleId": mongoose.Types.ObjectId(roleId)
            }
        }, {
            $group: {
                _id: "$_id",
                isGrouped: {
                    "$first": "$isGrouped"
                },
                level: {
                    "$first": "$level"
                },
                tittle: {
                    "$first": "$tittle"
                },
                icon: {
                    "$first": "$icon"
                },
                position: {
                    "$first": "$position"
                },
                url: {
                    "$first": "$url"
                },
                children: {
                    "$push": "$children"
                }
            }
        }, {
            $sort: {
                '_id': 1
            }
        }]);
        aggregate.exec((err, success) => {
            if (err) {
                throw (Error(err));
            } else if (!success) {
                resolve(false);
            } else {
                resolve(success)
            }
        });
    });
}

/* exports.signin = async function (req, res) {

    try {
        var condition = {
            email: req.body.email, // password: req.body.password,
            //status: 'ACTIVE'
        };
        return new Promise((resolve, reject) => {
            UserModel.findOne(condition, (err, success) => {
                if (err) {
                    reject(err)
                } else if (!success) {
                    resolve("!success")
                } else {
                    bcrypt.compare(req.body.password, success.password, function (err, passwordmatched) {
                        if (passwordmatched) {
                            let userData = {
                                _id: success._id,
                                fullName: success.fullName,
                                firstName: success.firstName,
                                lastName: success.lastName,
                                email: success.email,
                                contactNo: success.contactNo,
                                roleId: success.roleId
                            };
                            //get the JWT token for authentication in response _jwt is the middleware
                            userData.token = _jwt.generateJWTToken(userData);
                            resolve(userData);
                        } else {
                            resolve(false);
                        }
                    });
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
}; */