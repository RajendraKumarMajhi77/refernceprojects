let express = require("express");
var router = express.Router();
const {
    sanitizeBody,
    validationResult
} = require("express-validator");
let jwt = require("../../middleWares/jwt");
let Validate = require("../../middleWares/validation");
const apiResponse = require("../../utils/apiResponse");
const apiAuthController = require("./authentication.controller");
const {
    await
} = require("asyncawait");


/**
 * * Author : rAJENDRA -- Dt: 1st Jan 2021 
 * @description : to get routeURL.
 * 
 * @returns  {@route URL}
 */

async function getRouteUrl(req) {
    const fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
    return fullUrl;
}

/**
 * * Author : rAJENDRA -- Dt: 1st Jan 2021
 * @Api : auth/_test
 * @method : get 
 * @description : for test api
 * 
 * @returns  {@responseCode,@responseMessage}
 */
router.get("/_test", async (req, res) => {
    try {
        // validationResult(req).throw();
        var fullUrl = await getRouteUrl(req);
        res.send({
            responseCode: 200,
            responseMessage: "Hii @Rajendra test api is working fine ,Your Request URL [ " + fullUrl + " ]"

        });
    } catch (err) {
        //res.badRequest();
        res.send({
            responseCode: 422,
            responseMessage: "Some request missing"
        });
    }
}, );

/**
 * @Api : auth/signup
 * @method : post 
 * @description : for user registration
 *
 * @param {string}   firstName 
 * @param {string}   lastName
 * @param {string}   email
 * @param {string}   password
 * @param {number}   contactNo
 * @param {string}   roleId
 * @param {string}   createdBy
 * @param {string}   source
 * 
 * @returns {Object}
 */
router.post("/signup", Validate._signup, sanitizeBody("*").escape(), async (req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
        } else {
            next();
        }
    } catch (err) {
        //throw error in json response with status 500.
        return apiResponse.ErrorResponse(res, err);
    }
},apiAuthController.signup);

/**
 * @Api : auth/signin
 * @method : post 
 * @description : for user signin/login
 *
 * @param {string}   email
 * @param {string}   password
 * @returns {Object}
 */
router.post("/signin", Validate._signin, sanitizeBody("*").escape(), async (req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
        } else {
            next();
        }
    } catch (err) {
        //throw error in json response with status 500.
        return apiResponse.ErrorResponse(res, err);
    }
}, apiAuthController.signin);

/**
 * @Api : auth/saveuserlogin
 * @method : post 
 * @description : for save user login
 *
 * @param {string}   userId
 * @param {string}   token
 * @param {number}   tokenTimeOut
 * @param {string}   ipAdress
 * @param {string}   ispIpAdress
 * @param {string}   lat 
 * @param {string}   lon
 * @param {string}   os
 * @param {string}   osVersion
 * @param {string}   browser
 * @param {string}   browserVersion
 * @param {string}   userAgent
 * @param {string}   deviceType
 * @param {string}   city
 * @param {string}   country
 * @param {string}   timezone
 * @param {string}   zip
 *
 * @returns {Object}
 */
router.post("/saveuserlogin", sanitizeBody("*").escape(), async (req, res, next) => {
    try {
        next();
    } catch (err) {
        return apiResponse.ErrorResponse(res, err);
    }
}, jwt.jwtauthenticate, apiAuthController.saveuserlogin);


/**
 * @Api : auth/logout
 * @method : post 
 * @description : for logout user
 *
 * @param {string}   _id        [Localstorage value.  IDA is _id]
 * @param {string}   userId     [Localstorage value.  userId = IDI]
 * @param {string}   logoutMode  
 * Different logoutModes 1. unAuthorised  2.tokenTimeout 3.manualLogout
 * 
 * @returns {Object}  
 */
router.post("/logout", Validate._logout, sanitizeBody("*").escape(), async (req, res, next) => {
    try {
        // Extract the validation errors from a request.
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            // Display sanitized values/errors messages.
            return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
        } else {
            next();
        }
    } catch (err) {
        return apiResponse.ErrorResponse(res, err);
    }
}, apiAuthController.logout);

/**
 * @Api : auth/forgotpassword
 * @method : put 
 * @description : for request new password using forgotpassword screen
 *
 * @param {string}   _email  
 * @returns {Object}  
 */
router.put("/forgotpassword/:_email",Validate._forgotpassword, sanitizeBody("*").escape(),async (req, res, next) => {
    try {
        // Extract the validation errors from a request.
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            // Display sanitized values/errors messages.
            return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
        } else {
            next();
        }
    } catch (err) {
        return apiResponse.ErrorResponse(res, err);
    }
},apiAuthController.forgotPassword);  

/**
 * @Api : auth/resetpassword
 * @method : put 
 * @description : for request new password using resetpassword screen
 * 
 * @param {string}   _reqKey  
 * @returns {Object}  
 */
router.put("/resetpassword/:_reqKey",async (req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            let msg = 'Validation Error.';
            return apiResponse.validationErrorWithData(res, msg , errors.array());
        } else {
            next();
        }
    } catch (err) {
        return apiResponse.ErrorResponse(res, err);
    }
},apiAuthController.resetPassword); 

/**
 * @Api : auth/rolewisemenus
 * @method : put 
 * @description : for role wise menus list
 * 
 * @param {string}   _id 
 * @returns {Object}  
 */
router.get("/rolewisemenus",Validate._idValidation,sanitizeBody("*").escape(), async (req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            let msg = 'Validation Error.';
            return apiResponse.validationErrorWithData(res, msg , errors.array());
        } else {
            next();
        }
    } catch (err) {
        return apiResponse.ErrorResponse(res, err);
    }
},apiAuthController.roleWiseMenus); 



/**
 * @Api : auth/getcountrylist
 * @method : get
 * @description : for get country list
 *
 * @returns {Object}
 */
router.get("/getcountrylist",async (req, res, next) => {
    try {
        next();
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
},jwt.jwtauthenticate,apiAuthController.getCountryList);

/**
 * @Api : auth/getstatelist
 * @method : get
 * @description : for get state list
 *
 * @returns {Object}
 */
router.get("/getstatelist", Validate._getstatelist, sanitizeBody("*").escape(), async (req, res, next) => {
    try {
        const errors = validationResult(req);
        console.log(errors)
        if (!errors.isEmpty()) {
            return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
        } else {
            next();
        }
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
},jwt.jwtauthenticate, apiAuthController.getStateList);

/**
 * @Api : auth/getcitylist
 * @method : get
 * @description : for get state list
 *
 * @returns {Object}
 */
router.get("/getcitylist", Validate._getcitylist, sanitizeBody("*").escape(), async (req, res, next) => {
    try {
        const errors = validationResult(req);
        console.log(errors)
        if (!errors.isEmpty()) {
            return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
        } else {
            next();
        }
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
},jwt.jwtauthenticate, apiAuthController.getCityList);

module.exports = router;