const {
    await
} = require('asyncawait');
var mongoose = require('mongoose');
const utility = require("../../../utils/utility");
const apiResponse = require("../../../utils/apiResponse");
const helpers_fileupload = require("../../../middleWares/fileupload-helper");
const serv_adsCenter = require("./adsCenter.service");
const _ = require('lodash');

var listingAdsDetails = [];
exports.getPageCategoryList = async function (req, res, next) {
    try {
        var Data = await serv_adsCenter.getPageCategoryList();
        if (Data == false) {
            return apiResponse.validationErrorWithData(res, "No Data Found", []);
        } else {
            return apiResponse.successResponseWithData(res, "Data Found", Data);
        }
    } catch (e) {
        console.log("/--- error occured in getPageCategoryList | get -----/");
        next(e)
    }
};

exports.getDetailPage = async function (req, res, next) {
    try {
        if (req.query.adsLocationId == '0') {
            return apiResponse.validationErrorWithData(res, "Invalid Parameter", []);
        } else {
            var Data = await serv_adsCenter.getDetailPage(req.query.adsLocationId);
            //console.log(Data)
            if (Data == false) {
                return apiResponse.validationErrorWithData(res, "No Data Found", []);
            } else {
                return apiResponse.successResponseWithData(res, "Data Found", Data);
            }
        }
    } catch (e) {
        console.log("/--- error occured in getPageCategoryList | get -----/");
        next(e)
    }
};
exports.getLocationWiseAdsType = async function (req, res, next) {
    try {
        if (req.params.adsLocationId == "0") {
            return apiResponse.validationErrorWithData(res, "Invalid Parameter", []);
        } else {
            console.log(req.params)
            var Data = await serv_adsCenter.getLocationWiseAdsType(req.params.adsLocationId, req.params.adsPageLevel);
            var _adsDetails = [];
            if (Data != false) {
                for (var i = 0; i < Data.length; i++) {
                    if (req.params.adsCenterId != "0") {
                        _adsDetails = await serv_adsCenter.getAdsDetails(req.params.adsCenterId, Data[i].adsTypeId);
                        Data[i].adsDetails = _adsDetails;
                        if (_adsDetails.length > 0) {
                            Data[i].isShowPayNowBtn = true;
                        } else {
                            Data[i].isShowPayNowBtn = false;
                        }
                    } else {
                        Data[i].adsDetails = [];
                        Data[i].isShowPayNowBtn = false;
                    }
                }
            }

            if (Data != false) {
                for (var i = 0; i < Data.length; i++) {
                    // listing adstypeid 
                    if (Data[i].adsTypeId == '6064399f0d649827a052835a') {
                        if (Data[i].adsDetails.length == 0) {
                            var getSeq = await serv_adsCenter.getSeq(Data[i].locationId)
                            if (getSeq == false) {
                                for (var j = 0; j <= 12; j++) {
                                    // console.log('j index',j)
                                    if (j == 3) {
                                        // console.log('Ads')
                                        Data[i].adsDetails.push({
                                            'Heading': 'Ads Section',
                                            'Description': 'Sample Text'
                                        });
                                    } else if (j == 7) {
                                        // console.log('Ads')
                                        Data[i].adsDetails.push({
                                            'Heading': 'Ads Section',
                                            'Description': 'Sample Text'
                                        });
                                    } else if (j == 11) {
                                        // console.log('Ads')
                                        Data[i].adsDetails.push({
                                            'Heading': 'Ads Section',
                                            'Description': 'Sample Text'
                                        });
                                    } else {
                                        //console.log('Listing')
                                        Data[i].adsDetails.push({
                                            'Heading': 'Listing Section',
                                            'Description': 'Sample Text'
                                        });
                                    }
                                }
                            } else {
                                for (var j = 0; j <= 12; j++) {
                                    if (_.filter(getSeq, {
                                            index: j
                                        }).length > 0) {
                                        Data[i].adsDetails.push({
                                            'Heading': 'Ads Section',
                                            'Description': 'Sample Text'
                                        });
                                    } else {
                                        Data[i].adsDetails.push({
                                            'Heading': 'Listing Section',
                                            'Description': 'Sample Text'
                                        });
                                    }
                                }
                            }
                        } else {
                            var tempAdsSection = Data[i].adsDetails;
                            Data[i].adsDetails = [];
                            for (var j = 0; j <= 12; j++) {
                                var check = await serv_adsCenter.checkSequenceExistOrNot(Data[i].locationId, j);
                                if (check == true) { //3-7-11
                                    var particularSecAds =  _.filter(tempAdsSection, {seq: j});
                                    console.log(particularSecAds);
                                        if (particularSecAds.length > 0) {
                                            Data[i].adsDetails.push({
                                                'Heading': 'Ads Section',
                                                'Description': 'file exists',
                                                '_id': particularSecAds[0]._id ,
                                                'adsLocationId':particularSecAds[0].adsLocationId, 
                                                'adsDetailsId': particularSecAds[0].adsDetailsId ,
                                                'adsTypeId':particularSecAds[0].adsTypeId ,
                                                'adsTittle':particularSecAds[0].adsTittle ,
                                                'adsRefUrl': particularSecAds[0].adsRefUrl ,
                                                'fileName': particularSecAds[0].fileName ,
                                                'fileUrl': particularSecAds[0].fileUrl ,
                                                'seq': particularSecAds[0].seq 
                                            });
                                        } else {
                                            Data[i].adsDetails.push({
                                                'Heading': 'Ads Section',
                                                'Description': 'Sample Text',
                                                 '_id': '',
                                                 'adsLocationId':'',
                                                 'adsDetailsId': '',
                                                 'adsTypeId':'',
                                                 'adsTittle':'',
                                                 'adsRefUrl': '',
                                                 'fileName': '',
                                                 'fileUrl': '',
                                                 'seq': 0
                                            });
                                        }
                                } else {
                                    Data[i].adsDetails.push({
                                        'Heading': 'Listing Section',
                                        'Description': 'Sample Text'
                                    });

                                }
                            }
                        }
                    }
                }
            }
            //console.log(JSON.stringify(Data));
            if (Data == false) {
                return apiResponse.validationErrorWithData(res, "No Data Found", []);
            } else {
                return apiResponse.successResponseWithData(res, "Data Found", Data);
            }
        }
    } catch (e) {
        console.log("/--- error occured in getLocationWiseAdsType | get -----/");
        // "./node_modules/@angular/material/prebuilt-themes/indigo-pink.css",
        next(e)
    }
};

exports.UploadAds = async function (req, res, next) {
    //console.log(req.body)
    try {
        let url = req.body.url;
        let userId = req.body.userId;
        let adsCenterId = req.body.adsCenterId;
        let adsLocationId = req.body.adsLocationId;
        let adsLocationDetailPageId = req.body.adsLocationDetailPageId;
        let adsTypeId = req.body.adsTypeId;
        let adsTittle = req.body.adsTittle;
        let listingAdsSeqNo = req.body.listingAdsSeqNo;
        let locationId = adsLocationDetailPageId == '0' ? adsLocationId : adsLocationDetailPageId;

        const subDirPath = "UEAds";
        if (!req.files) {
            return apiResponse.successResponseWithData(res, "No File Uploaded", []);
        } else {
            var createInstanse;
            if (adsCenterId == '0') {
                createInstanse = await serv_adsCenter.addInstance(userId, locationId);
                adsCenterId = createInstanse._id;
            }
            // Imgae link generated according to different id's 
            const imageLink = adsCenterId + "_" + locationId + "_" + adsTypeId + "_iat" + JSON.stringify(new Date().getTime());
            // upload single image  parameters req, subdirectory [where to store], imagelink
            var uploadAdsImage = await helpers_fileupload.uploadSingleFile(req, subDirPath, imageLink);
            if (uploadAdsImage != false) {
                var saveAdsDetails = await serv_adsCenter.saveAdsDetails(req, adsCenterId, uploadAdsImage);
                if (saveAdsDetails != false) {
                    // Code to update the adsCode start here 
                    var length = saveAdsDetails.adsDetails.length - 1;
                    var findCount = await serv_adsCenter.getCount();
                    var updateAdsCode = await serv_adsCenter.updateAdsCode(adsCenterId,
                        saveAdsDetails.adsDetails[length]._id ,findCount); 
                    // Code to update the adsCode ends here
                    let msg = "File uploaded successfully.";
                    return apiResponse.successResponseWithData(res, msg, saveAdsDetails);
                } else {
                    let msg = "Something went wrong.";
                    return apiResponse.validationErrorWithData(res, msg, []);
                }
            } else {
                let msg = "Something went wrong.";
                return apiResponse.validationErrorWithData(res, msg, []);
            }
        }
    } catch (e) {
        next(e)
    }
};

exports.UpdateAdsSeq = async function (req, res, next) {
    try {
        var changeSeq = await serv_adsCenter.updateAdslocationSeq(req);
        if(req.body.itemSeqFormat.length > 0){
            var changeItemSeq = await serv_adsCenter.updateAdsDetailsSeq(req.body.itemSeqFormat)
        }
        //console.log('Records',req.body.itemSeqFormat);
        //var changeItemSeq = aw
        if (changeSeq == true) {
            let msg = "Ads Listing Sequence Updated Successfully.";
            return apiResponse.successResponseWithData(res, msg, []);
        } else {
            let msg = "Something went wrong.";
            return apiResponse.validationErrorWithData(res, msg, []);
        }
    } catch (e) {
        next(e)
    }
};


exports.ProceedTopayNow = async function (req, res, next) {
    try {
        let _id = req.body.adsCenterId;
        let userId = req.body.userId;
        var proceedToPay = await serv_adsCenter.proceedtoPay(_id,userId);
        if (proceedToPay == true) {
            let msg = "Thank You For Advertising With UE.";
            return apiResponse.successResponseWithData(res, msg, []);
        } else {
            let msg = "Something went wrong.";
            return apiResponse.validationErrorWithData(res, msg, []);
        }
    } catch (e) {
        next(e)
    }
};
