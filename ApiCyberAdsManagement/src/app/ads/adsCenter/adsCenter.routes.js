let express = require("express");
var router = express.Router();
const {
    await
} = require("asyncawait");
const {
    sanitizeBody,
    validationResult
} = require("express-validator");
let jwt = require("../../../middleWares/jwt")
let Validate = require("../../../middleWares/validation");
const apiResponse = require("../../../utils/apiResponse");
const apiAdsCenterCtrl = require("./adsCenter.ctrl");


/**
 * @Api : v1/ads-adscenter/getpagecategory 
 * @method : get
 * @description : for get ads location
 *
 * @returns {Object}
 */
router.get("/getpagecategory", async (req, res, next) => {
    try {
        next();
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
}, apiAdsCenterCtrl.getPageCategoryList);

/**
 * @Api : v1/ads-adscenter/getdetailpage 
 * @method : get
 * @description : for get ads location
 *
 * @returns {Object}
 */
router.get("/getdetailpage", async (req, res, next) => {
    try {
        next();
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
}, apiAdsCenterCtrl.getDetailPage);



/**
 * @Api : v1/ads-adscenter/get-locationswise-adstype
 * @method : get
 * @description : for get ads location
 *
 * @returns {Object}
 */
router.get("/get-locationwise-adstype/:adsLocationId/:adsLocationDetailPageId/:adsPageLevel/:adsCenterId", async (req, res, next) => {
    try {
        next();
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
}, apiAdsCenterCtrl.getLocationWiseAdsType);

/**
 * @Api : v1/ads-adscenter/upload-ads
 * @method : post  formData
 * @description : for upload-ads
 *
 * @param {string}   adsLocationId
 * @param {string}   adsLocationDetailPageId
 * @param {string}   adsTypeId
 * @param {string}   adsTittle 
 * @param {string}   url 
 * @param {file}     file 
 *   
 * @returns {Object}
 */
router.post("/upload-ads", async (req, res, next) => {
    try {
        next();
    } catch (err) {
        return apiResponse.ErrorResponse(res, err);
    }
}, apiAdsCenterCtrl.UploadAds);


/**
 * @Api : v1/ads-adscenter/update-adsseq
 * @method : post  
 * @description : for update-adsseq
 *
 * @param {string}   adsLocationId
 * @param {string}   userId
 * @param {Array}    SeqFormat
 * @param {Array}    itemSeqFormat
 *   
 * @returns {Object}
 */
router.post("/update-adsseq", async (req, res, next) => {
    try {
        next();
    } catch (err) {
        return apiResponse.ErrorResponse(res, err);
    }
}, apiAdsCenterCtrl.UpdateAdsSeq);

/**
 * @Api : v1/ads-adscenter/proceed-topaynow
 * @method : post  
 * @description : for update-adsseq
 *
 * @param {string}   userId
 * @param {string}   adsCenterId
 *   
 * @returns {Object}
 */
router.post("/proceed-topaynow", async (req, res, next) => {
    try {
        next();
    } catch (err) {
        return apiResponse.ErrorResponse(res, err);
    }
}, apiAdsCenterCtrl.ProceedTopayNow);

module.exports = router;

