const {
  await
} = require('asyncawait');
var mongoose = require('mongoose');
const adsLocationCollection = require("../../../models/adsLocation");
const adsAllocationCollection = require("../../../models/adsAllocation");
const adsCenterCollection = require("../../../models/adsCenter");
const utility = require("../../../utils/utility");

exports.getPageCategoryList = async function () {
  try {
    return new Promise((resolve, reject) => {
      const aggregate = adsLocationCollection.aggregate([{
        '$match': {
          'isActive': true,
          'isDeleted': false,
          'pageLevel': {
            $ne: 'Level-2'
          }
        }
      }, {
        '$project': {
          '_id': 1,
          'pageLevel': 1,
          'pageName': 1,
          'pageUrl': 1,
          'parentId': 1,
          'updatedAt': 1
        }
      }]);
      aggregate.exec((err, success) => {
        if (err) {
          throw (Error(err));
        } else if (!success) {
          resolve(false);
        } else {
          resolve(success)
        }
      });
    });
  } catch (e) {
    console.log(e);
    throw e;
  }
};
exports.getDetailPage = async function (parentId) {
  try {
    return new Promise((resolve, reject) => {
      const aggregate = adsLocationCollection.aggregate([{
        '$match': {
          'isActive': true,
          'isDeleted': false,
          'pageLevel': {
            $eq: 'Level-2'
          },
          'parentId': parentId
        }
      }, {
        '$project': {
          '_id': 1,
          'pageLevel': 1,
          'pageName': 1,
          'pageUrl': 1,
          'parentId': 1,
          'updatedAt': 1
        }
      }]);
      aggregate.exec((err, success) => {
        if (err) {
          throw (Error(err));
        } else if (!success) {
          resolve(false);
        } else {
          resolve(success)
        }
      });
    });
  } catch (e) {
    console.log(e);
    throw e;
  }
};
exports.getLocationWiseAdsType = async function (adsLocationId, adsPageLevel) {
  try {
    return new Promise((resolve, reject) => {
      const aggregate = adsAllocationCollection.aggregate([{
        '$match': {
          'isActive': true,
          'isDeleted': false,
          'pageLevel': adsPageLevel,
          'parentId': mongoose.Types.ObjectId(adsLocationId)
        }
      }, {
        '$unwind': {
          'path': '$allocationDetails',
          'includeArrayIndex': 'index',
          'preserveNullAndEmptyArrays': true
        }
      }, {
        '$lookup': {
          'from': 'adsLocation',
          'localField': 'parentId',
          'foreignField': '_id',
          'as': 'adsLocationCol'
        }
      }, {
        '$unwind': {
          'path': '$adsLocationCol',
          'preserveNullAndEmptyArrays': true
        }
      }, {
        '$lookup': {
          'from': 'adsType',
          'localField': 'allocationDetails.adsTypeId',
          'foreignField': '_id',
          'as': 'adsTypeCol'
        }
      }, {
        '$unwind': {
          'path': '$adsTypeCol',
          'preserveNullAndEmptyArrays': true
        }
      }, {
        '$lookup': {
          'from': 'adsSection',
          'localField': 'adsTypeCol.adsSectionId',
          'foreignField': '_id',
          'as': 'adsSectionCol'
        }
      }, {
        '$unwind': {
          'path': '$adsSectionCol',
          'preserveNullAndEmptyArrays': true
        }
      }, {
        '$lookup': {
          'from': 'adsPositions',
          'localField': 'adsTypeCol.adsPositionsId',
          'foreignField': '_id',
          'as': 'adsPositionsCol'
        }
      }, {
        '$unwind': {
          'path': '$adsPositionsCol',
          'preserveNullAndEmptyArrays': true
        }
      }, {
        '$project': {
          '_id': 1,
          'index': 1,
          'pageLevel': 1,
          'locationId': '$adsLocationCol._id',
          'pageName': '$adsLocationCol.pageName',
          'pageUrl': '$adsLocationCol.pageUrl',
          'adsTypeId': '$adsTypeCol._id',
          'adsType': '$adsTypeCol.adsType',
          'adsWidth': '$adsTypeCol.adsWidth',
          'adsHeight': '$adsTypeCol.adsHeight',
          'adsSectionId': '$adsSectionCol._id',
          'adsSection': '$adsSectionCol.adsSection',
          'adsPositionsId': '$adsPositionsCol._id',
          'adsPositions': '$adsPositionsCol.adsPositions'
        }
      }]);
      aggregate.exec((err, success) => {
        if (err) {
          throw (Error(err));
        } else if (!success) {
          resolve(false);
        } else {
          resolve(success)
        }
      });
    });
  } catch (e) {
    console.log(e);
    throw e;
  }
};

exports.addInstance = async function (userId, adsLocationId) {
  try {
    let reqObj = new adsCenterCollection({
      userId: userId,
      adsLocationId: adsLocationId
    });
    return new Promise((resolve, reject) => {
      reqObj.save((err, success) => {
        if (err) {
          reject(err);
        }
        if (!success) {
          resolve(false);
        } else {
          resolve(success);
        }
      });
    });
  } catch {
    console.log(e);
    throw e;
  }
};
exports.saveAdsDetails = async function (req, adsCenterId, uploadResponse) {
  try {
    return new Promise((resolve, reject) => {
      adsCenterCollection.findByIdAndUpdate(adsCenterId, {
          $push: {
            "adsDetails": {
              adsTypeId: req.body.adsTypeId,
              adsTittle: req.body.adsTittle,
              adsRefUrl: req.body.url,
              fileName: uploadResponse._fileName,
              fileUrl: uploadResponse._fileUrl,
              seq: parseInt(req.body.listingAdsSeqNo)
            }
          }
        }, {
          safe: true,
          upsert: true,
          new: true
        },
        function (err, success) {
          if (err) {
            resolve(false);
          }
          resolve(success);


        }
      );
    });
  } catch (e) {
    console.log('service error\n', e);
    throw e;
  }
};

exports.getAdsDetails = async function (adsCenterId, adsTypeId) {
  try {
    return new Promise((resolve, reject) => {
      const aggregate = adsCenterCollection.aggregate([{
        '$unwind': {
          'path': '$adsDetails',
          'preserveNullAndEmptyArrays': true
        }
      }, {
        '$match': {
          'isActive': true,
          'isDeleted': false,
          '_id': mongoose.Types.ObjectId(adsCenterId)
        }
      }, {
        '$project': {
          '_id': 1,
          'adsLocationId': 1,
          'adsDetailsId': '$adsDetails._id',
          'adsTypeId': '$adsDetails.adsTypeId',
          'adsTittle': '$adsDetails.adsTittle',
          'adsRefUrl': '$adsDetails.adsRefUrl',
          'fileName': '$adsDetails.fileName',
          'fileUrl': '$adsDetails.fileUrl',
          'seq': '$adsDetails.seq'
        }
      }, {
        '$match': {
          'adsTypeId': mongoose.Types.ObjectId(adsTypeId)
        }
      }]);
      aggregate.exec((err, success) => {
        if (err) {
          throw (Error(err));
        } else if (!success) {
          resolve(false);
        } else {
          // console.log('iNSIDE DATA',success)
          resolve(success)
        }
      });
    });

  } catch (e) {
    console.log('service error\n', e);
    throw e;
  }
};


exports.checkSequenceExistOrNot = async function (_id, j) {
  return new Promise((resolve, reject) => {
    const aggregate = adsLocationCollection.aggregate([{
      '$match': {
        '_id': mongoose.Types.ObjectId(_id),
        'isActive': true,
        'isDeleted': false
      }
    }, {
      '$unwind': {
        'path': '$sequence',
        'preserveNullAndEmptyArrays': true
      }
    }, {
      '$project': {
        '_id': 1,
        'seq_id': '$sequence._id',
        'index': '$sequence.index'
      }
    }, {
      '$match': {
        'index': j
      }
    }]);
    aggregate.exec((err, success) => {
      if (err || !success) {
        resolve(false);
      } else if (success.length == 0) {
        resolve(false);
      } else if (success.length > 0) {
        resolve(true)
      }
    });
  });
};

exports.getSeq = async function (_id) {
  return new Promise((resolve, reject) => {
    const aggregate = adsLocationCollection.aggregate([{
      '$match': {
        '_id': mongoose.Types.ObjectId(_id),
        'isActive': true,
        'isDeleted': false
      }
    }, {
      '$project': {
        '_id': 1,
        'sequence': 1
      }
    }]);
    aggregate.exec((err, success) => {
      if (err || !success) {
        resolve(false);
      } else if (success[0].sequence.length == 0) {
        resolve(false);
      } else if (success[0].sequence.length > 0) {
        resolve(success[0].sequence)
      }
    });
  });
};

exports.updateAdslocationSeq = async function (req) {
  try {
    return new Promise((resolve, reject) => {
      adsLocationCollection.findByIdAndUpdate(req.body.adsLocationId, {
        $set: {
          "sequence": []
        }
      }, function (err, success) {
        if (err) {
          resolve(false);
        }
        adsLocationCollection.findByIdAndUpdate(req.body.adsLocationId, {
            $push: {
              "sequence": req.body.SeqFormat
            }
          }, {
            safe: true,
            upsert: true,
            new: true
          },
          function (err, success) {
            if (err) {
              resolve(false);
            }
            resolve(true);
          }
        );
      });


    });
  } catch (e) {
    console.log('service error\n', e);
    throw e;
  }
};

exports.updateAdsDetailsSeq = async function (itemList) {
  try {
    return new Promise((resolve, reject) => {
      for (var i = 0; i < itemList.length; i++) {
        console.log('index:', i, '_id:   ', itemList[i]._id, 'adsDetailsId:   ', itemList[i].adsDetailsId, 'seq: ', itemList[i].seq);
        adsCenterCollection.updateOne({
          "_id": mongoose.Types.ObjectId(itemList[i]._id),
          "adsDetails._id": mongoose.Types.ObjectId(itemList[i].adsDetailsId)
        }, {
          $set: {
            "adsDetails.$.seq": itemList[i].index
          }
        }, (err, success) => {
          if (err) {
            throw err
          }
          if (!success) {
            resolve(false);
          } else {
            resolve(true);
          }
        });
      }
    });
  } catch (e) {
    console.log('service error\n', e);
    throw e;
  }
};


exports.proceedtoPay = async function (_id, userId) {
  try {
    return new Promise((resolve, reject) => {
      adsCenterCollection.updateOne({
        "_id": mongoose.Types.ObjectId(_id),
        "userId": mongoose.Types.ObjectId(userId)
      }, {
        $set: {
          "status": 'PUBLISHED',
          "publishedOn": Date.now(),
          "expiredOn": utility.getExpireDate('Monthly')

        }
      }, (err, success) => {
        if (err) {
          throw err
        }
        if (!success) {
          resolve(false);
        } else {
          resolve(true);
        }
      });
    });
  } catch (e) {
    console.log('service error\n', e);
    throw e;
  }
};


exports.getCount = async function () {
  try {
    return new Promise((resolve, reject) => {
      const aggregate = adsCenterCollection.aggregate(      [
        {
          '$project': {
            '_id': 0, 
            'adsDetails': 1
          }
        }, {
          '$unwind': {
            'path': '$adsDetails', 
            'preserveNullAndEmptyArrays': true
          }
        }, {
          '$group': {
            '_id': null, 
            'count': {
              '$sum': 1
            }
          }
        }
      ]);
      aggregate.exec((err, success) => {
        if (err || !success) {
          resolve(false);
        } else {
          resolve(success[0].count)
        }
      });
    });
  } catch (e) {
    console.log('service error\n', e);
    throw e;
  }
};

exports.updateAdsCode = async function (adsCenterId,adsDetails_id ,findCount) {
  try {
  // var value = 
    return new Promise((resolve, reject) => {
  adsCenterCollection.updateOne({
          "_id": mongoose.Types.ObjectId(adsCenterId),
          "adsDetails._id": mongoose.Types.ObjectId(adsDetails_id)
        }, {
          $set: {
            "adsDetails.$.adsCode":utility.SeqOutputNumber('#AD-',findCount,10,false)
          }
        }, (err, success) => {
          if (err) {
            throw err
          }
          if (!success) {
            resolve(false);
          } else {
            resolve(true);
          }
        });
    });
  } catch (e) {
    console.log('service error\n', e);
    throw e;
  }
};


