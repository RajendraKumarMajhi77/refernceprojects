const {
  await
} = require('asyncawait');
var mongoose = require('mongoose');
const adsLocationCollection = require("../../../models/adsLocation");
const adsAllocationCollection = require("../../../models/adsAllocation");
const adsCenterCollection = require("../../../models/adsCenter");
const utility = require("../../../utils/utility");
const Crypto = require("../../../middleWares/crypto");


exports.getAdsLocation = async function (_PATH) {
  try {
    return new Promise((resolve, reject) => {
      const aggregate = adsLocationCollection.aggregate([{
        '$match': {
          'isActive': true,
          'isDeleted': false
        }
      }, {
        '$project': {
          '_id': 1,
          'pageLevel': 1,
          'pageName': 1,
          'pageUrl': 1,
          'parentId': 1
        }
      }]);
      aggregate.exec((err, success) => {
        if (err) {
          throw (Error(err));
        } else if (!success) {
          resolve(false);
        } else {
          for (var i = 0; i < success.length; i++) {
            if (new URL(success[i].pageUrl).pathname == _PATH) {
              resolve(success[i])
            }
          }
        }
      });
    });
  } catch (e) {
    console.log(e);
    throw e;
  }
};
exports.findAdsType = async function (level, _id) {
  try {
    return new Promise((resolve, reject) => {
      const aggregate = adsAllocationCollection.aggregate([{
        '$unwind': {
          'path': '$allocationDetails',
          'preserveNullAndEmptyArrays': true
        }
      }, {
        '$match': {
          'isActive': true,
          'isDeleted': false,
          'pageLevel': level,
          'parentId': mongoose.Types.ObjectId(_id)
        }
      }, {
        '$project': {
          '_id': 1,
          'pageLevel': 1,
          'parentId': 1,
          'adsTypeId': '$allocationDetails.adsTypeId'
        }
      }]);
      aggregate.exec((err, success) => {
        if (err) {
          throw (Error(err));
        } else if (!success) {
          resolve(false);
        } else {
          resolve(success);
        }
      });
    });
  } catch (e) {
    console.log(e);
    throw e;
  }

}
exports.findLevelZeroAds = async function (_id, adsTypeId, limit) {
  try {
    return new Promise((resolve, reject) => {
      const aggregate = adsCenterCollection.aggregate([{
        '$match': {
          'isActive': true,
          'isDeleted': false,
          'status': 'PUBLISHED',
          'adsLocationId': mongoose.Types.ObjectId(_id)
        }
      }, {
        '$unwind': {
          'path': '$adsDetails',
          'preserveNullAndEmptyArrays': true
        }
      }, {
        '$project': {
          '_id': 1,
          'adsLocationId': 1,
          'adsDetailsid': '$adsDetails._id',
          'adsTypeId': '$adsDetails.adsTypeId',
          'expiredOn': 1,
          'adsTittle': '$adsDetails.adsTittle',
          'adsRefUrl': '$adsDetails.adsRefUrl',
          'fileUrl': '$adsDetails.fileUrl',
          'createdAt': 1,
          'updatedAt': 1,
          'serchSeq': '$adsDetails.serchSeq'
        }
      }, {
        '$match': {
          'expiredOn': {
            '$gte': new Date()
          },
          'adsTypeId': mongoose.Types.ObjectId(adsTypeId)
        }
      }, {
        '$sort': {
          'createdAt': 1
        }
      }, {
        '$sort': {
          'serchSeq': 1
        }
      }, {
        '$limit': limit
      }]);
      aggregate.exec((err, success) => {
        if (err) {
          throw (Error(err));
        } else if (!success) {
          resolve(false);
        } else {
          resolve(success)
        }
      });
    });
  } catch (e) {
    console.log(e);
    throw e;
  }
};
exports.findLevelOneAds = async function (_id, adsTypeId, limit) {
  try {
    return new Promise((resolve, reject) => {
      const aggregate = adsLocationCollection.aggregate([{
        '$match': {
          'isActive': true,
          'isDeleted': false,
          'pageLevel': 'Level-1',
          'parentId': '',
          '_id': mongoose.Types.ObjectId(_id)
        }
      }, {
        '$unionWith': {
          'coll': 'adsLocation',
          'pipeline': [{
            '$match': {
              'isActive': true,
              'isDeleted': false,
              'pageLevel': 'Level-2',
              'parentId': _id.toString()
            }
          }]
        }
      }, {
        '$project': {
          '_id': 1,
          'pageLevel': 1,
          'pageName': 1,
          'pageUrl': 1
        }
      }, {
        '$lookup': {
          'from': 'adsCenter',
          'localField': '_id',
          'foreignField': 'adsLocationId',
          'as': 'adsCenterCol'
        }
      }, {
        '$unwind': {
          'path': '$adsCenterCol',
          'preserveNullAndEmptyArrays': true
        }
      }, {
        '$match': {
          'adsCenterCol.isActive': true,
          'adsCenterCol.isDeleted': false,
          'adsCenterCol.status': 'PUBLISHED'
        }
      }, {
        '$unwind': {
          'path': '$adsCenterCol.adsDetails',
          'preserveNullAndEmptyArrays': true
        }
      }, {
        '$project': {
          '_id': 1,
          'adsCenterCol': 1
        }
      }, {
        '$project': {
          '_id': '$adsCenterCol._id',
          'adsLocationId': '$adsCenterCol.adsLocationId',
          'adsDetailsid': '$adsCenterCol.adsDetails._id',
          'adsTypeId': '$adsCenterCol.adsDetails.adsTypeId',
          'expiredOn': '$adsCenterCol.expiredOn',
          'adsTittle': '$adsCenterCol.adsDetails.adsTittle',
          'adsRefUrl': '$adsCenterCol.adsDetails.adsRefUrl',
          'fileUrl': '$adsCenterCol.adsDetails.fileUrl',
          'createdAt': '$adsCenterCol.createdAt',
          'updatedAt': '$adsCenterCol.updatedAt',
          'serchSeq': '$adsCenterCol.adsDetails.serchSeq'
        }
      }, {
        '$match': {
          'expiredOn': {
            '$gte': new Date()
          },
          'adsTypeId': mongoose.Types.ObjectId(adsTypeId)
        }
      }, {
        '$sort': {
          'updatedAt': -1
        }
      }, {
        '$sort': {
          'serchSeq': 1
        }
      }, {
        '$limit': limit
      }]);
      aggregate.exec((err, success) => {
        if (err) {
          throw (Error(err));
        } else if (!success) {
          resolve(false);
        } else {
          resolve(success)
        }
      });
    });
  } catch (e) {
    console.log(e);
    throw e;
  }
};
exports.findLevelTwoAds = async function (_id, adsTypeId, limit) {
  try {
    return new Promise((resolve, reject) => {
      const aggregate = adsCenterCollection.aggregate([{
        '$match': {
          'isActive': true,
          'isDeleted': false,
          'status': 'PUBLISHED',
          'adsLocationId': mongoose.Types.ObjectId(_id)
        }
      }, {
        '$unwind': {
          'path': '$adsDetails',
          'preserveNullAndEmptyArrays': true
        }
      }, {
        '$project': {
          '_id': 1,
          'adsLocationId': 1,
          'adsDetailsid': '$adsDetails._id',
          'adsTypeId': '$adsDetails.adsTypeId',
          'expiredOn': 1,
          'adsTittle': '$adsDetails.adsTittle',
          'adsRefUrl': '$adsDetails.adsRefUrl',
          'fileUrl': '$adsDetails.fileUrl',
          'createdAt': 1,
          'updatedAt': 1,
          'serchSeq': '$adsDetails.serchSeq'
        }
      }, {
        '$match': {
          'expiredOn': {
            '$gte': new Date('Sat, 03 Apr 2021 11:04:28 GMT')
          },
          'adsTypeId': mongoose.Types.ObjectId(adsTypeId)
        }
      }, {
        '$sort': {
          'createdAt': 1
        }
      }, {
        '$sort': {
          'serchSeq': 1
        }
      }, {
        '$limit': limit
      }]);
      aggregate.exec((err, success) => {
        if (err) {
          throw (Error(err));
        } else if (!success) {
          resolve(false);
        } else {
          resolve(success)
        }
      });
    });
  } catch (e) {
    console.log(e);
    throw e;
  }
};
exports.incSearchSeq = async function (Data) {
  try {
    // console.log('Data Fetched :: >>>>>>\n',Data)
    return new Promise((resolve, reject) => {
      for (let i of Data) {
        //console.log('Data Fetched Instance :: >>>>>>\n',i)
        adsCenterCollection.updateOne({
          "_id": i._id,
          "adsLocationId": i.adsLocationId,
          "adsDetails._id": i.adsDetailsid
        }, {
          $inc: {
            'adsDetails.$.serchSeq': 1
          }
        }, (err, success) => {
          if (err) {
            throw err
          }
          if (!success) {
            resolve(false);
          } else {
            resolve(success);
          }
        });
      }
    });
  } catch (e) {
    console.log('service error\n', e);
    throw e;
  }
};
exports.incSearchSeqDetail = async function (Data) {
  try {
    return new Promise((resolve, reject) => {
      for (let i of Data) {
        adsCenterCollection.updateOne({
          "_id": i._id,
          "adsLocationId": i.adsLocationId,
          "adsDetails._id": i.adsDetailsid
        }, {
          $inc: {
            'adsDetails.$.serchSeqDetail': 1
          }
        }, (err, success) => {
          if (err) {
            throw err
          }
          if (!success) {
            resolve(false);
          } else {
            resolve(success);
          }
        });
      }
    });
  } catch (e) {
    console.log('service error\n', e);
    throw e;
  }
};


exports.findSeqList = async function (_id) {
  let seq = [];
  return new Promise((resolve, reject) => {
    const aggregate = adsLocationCollection.aggregate([{
      '$match': {
        '_id': mongoose.Types.ObjectId(_id),
        'isActive': true,
        'isDeleted': false
      }
    }, {
      '$project': {
        '_id': 1,
        'sequence': 1
      }
    }]);
    aggregate.exec((err, success) => {
      if (err || !success) {
        resolve(false);
      } else if (success[0].sequence.length == 0) {
        resolve(false);
      } else if (success[0].sequence.length > 0) {
        for (let i of success[0].sequence) {
          seq.push({
            index: i.index
          });
        }
        resolve(seq)
      }
    });
  });
};

exports.saveClicksAnalytics = async function (data) {
  debugger;
  try {
    return new Promise((resolve, reject) => {
      adsCenterCollection.findOneAndUpdate({
          "_id": mongoose.Types.ObjectId(data.adsCenterId),
          "adsDetails._id" : mongoose.Types.ObjectId(data.adsDetailsId)
        }, {
          $push: {
            "adsDetails.$.clickDetails": {
              country: data.locationData.country,
              countryCode: data.locationData.countryCode,
              region: data.locationData.region,
              regionName: data.locationData.regionName,
              city: data.locationData.city,
              zip: data.locationData.zip,
              lat: data.locationData.lat,
              lon: data.locationData.lon,
              timezone: data.locationData.timezone,
              isp: data.locationData.isp,
              org: data.locationData.org,
              as: data.locationData.as,
              query: data.locationData.query,
              sourceUrl: data.sourceUrl,
              destUrl: data.destUrl
            }
          }
        },
        function (err, success) {
          if (err) {
            resolve(false);
          }
          resolve(true);
        }
      );
    });
  } catch (e) {
    console.log(e);
    throw e;
  }
};