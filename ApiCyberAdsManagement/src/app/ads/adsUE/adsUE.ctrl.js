const {
    await
} = require('asyncawait');
var mongoose = require('mongoose');
const utility = require("../../../utils/utility");
const apiResponse = require("../../../utils/apiResponse");
const serv_adsUE = require("./adsUE.service");
const _ = require('lodash');

exports.getAds = async function (req, res, next) {
    try {
        var _PATH = new URL(req.params.source).pathname;
        //console.log(_PATH);
        var _findAdsLocation = await serv_adsUE.getAdsLocation(_PATH);
        //console.log(_findAdsLocation)
        let finalObjectOfList = {};
        if (_findAdsLocation != false) {
            // console.log(_findAdsLocation)
            if (_findAdsLocation.pageLevel == 'Level-0') {
                var findAdsType = await serv_adsUE.findAdsType(_findAdsLocation.pageLevel, _findAdsLocation._id);
                // console.log(findAdsType);
                finalObjectOfList.pageLevel = _findAdsLocation.pageLevel;
                if (findAdsType != false) {
                    for (var i = 0; i < findAdsType.length; i++) {
                        //console.log(findAdsType[i].adsTypeId);
                        if (findAdsType[i].adsTypeId == '605db959675ca07294ae38f6') {
                            var _firstSec = await serv_adsUE.findLevelZeroAds(_findAdsLocation._id, findAdsType[i].adsTypeId, 1);

                            if (Object.keys(_firstSec).length > 0) {
                                finalObjectOfList.firstSec = _firstSec;
                                var updateSearchSeq = await serv_adsUE.incSearchSeq(_firstSec);
                            } else {
                                finalObjectOfList.firstSec = [];
                            }
                        } else if (findAdsType[i].adsTypeId == '605dbac33638333504edd705') {
                            var _secondSec = await serv_adsUE.findLevelZeroAds(_findAdsLocation._id, findAdsType[i].adsTypeId, 2);
                            if (Object.keys(_secondSec).length > 0) {
                                finalObjectOfList.secondSec = _secondSec;
                                var updateSearchSeq = await serv_adsUE.incSearchSeq(_secondSec);
                            } else {
                                finalObjectOfList.secondSec = [];
                            }
                        }
                    }
                }
            }
            if (_findAdsLocation.pageLevel == 'Level-1') {
                var findAdsType = await serv_adsUE.findAdsType(_findAdsLocation.pageLevel, _findAdsLocation._id);
                // console.log(findAdsType);
                finalObjectOfList.pageLevel = _findAdsLocation.pageLevel;
                if (findAdsType != false) {
                    for (var i = 0; i < findAdsType.length; i++) {
                        // console.log(findAdsType[i].adsTypeId);
                        //606439810d649827a0528359 -- upeer banner ads
                        //6064399f0d649827a052835a -- listing ads 
                        if (findAdsType[i].adsTypeId == '606439810d649827a0528359') {
                            var _firstSec = await serv_adsUE.findLevelOneAds(_findAdsLocation._id, findAdsType[i].adsTypeId, 1);
                            if (Object.keys(_firstSec).length > 0) {
                                finalObjectOfList.firstSec = _firstSec;
                                var updateSearchSeq = await serv_adsUE.incSearchSeq(_firstSec);
                            } else {
                                finalObjectOfList.firstSec = [];
                            }
                        } else if (findAdsType[i].adsTypeId == '6064399f0d649827a052835a') {
                            var _secondSecSeq = await serv_adsUE.findSeqList(_findAdsLocation._id);
                            //console.log(_secondSecSeq);
                            if (Object.keys(_secondSecSeq).length > 0) {
                                finalObjectOfList.secondSecSeq = _secondSecSeq;
                            } else {
                                finalObjectOfList.secondSec = [];
                            }
                            var _secondSec = await serv_adsUE.findLevelOneAds(_findAdsLocation._id, findAdsType[i].adsTypeId, 3);
                            if (Object.keys(_secondSec).length > 0) {
                                finalObjectOfList.secondSec = _secondSec;
                                var updateSearchSeq = await serv_adsUE.incSearchSeq(_secondSec);
                            } else {
                                finalObjectOfList.secondSec = [];
                            }
                        }
                    }
                }
            }
            if (_findAdsLocation.pageLevel == 'Level-2') {
                //As Details page so ads location id replaced with parentId
                var findAdsType = await serv_adsUE.findAdsType(_findAdsLocation.pageLevel, _findAdsLocation.parentId);
                //console.log(findAdsType);
                finalObjectOfList.pageLevel = _findAdsLocation.pageLevel;
                if (findAdsType != false) {
                    for (var i = 0; i < findAdsType.length; i++) {
                        //console.log(findAdsType[i].adsTypeId);
                        // 606439810d649827a0528359 -- upeer banner ads  
                        if (findAdsType[i].adsTypeId == '606439810d649827a0528359') {
                            var _firstSec = await serv_adsUE.findLevelTwoAds(_findAdsLocation._id, findAdsType[i].adsTypeId, 1);
                            if (Object.keys(_firstSec).length > 0) {
                                finalObjectOfList.firstSec = _firstSec;
                                var updateSearchSeq = await serv_adsUE.incSearchSeqDetail(_firstSec);
                            } else {
                                finalObjectOfList.firstSec = [];
                            }
                        }
                    }
                }
            }
            // console.log(JSON.stringify(finalObjectOfList));
            if (finalObjectOfList.length == 0) {
                return apiResponse.validationErrorWithData(res, "No Data Found", []);
            } else {
                return apiResponse.successResponseWithData(res, "Data Found", finalObjectOfList);
            }
        }
    } catch (e) {
        next(e);
    }
};

exports.clickAds = async function (req, res, next) {
    try {
        debugger;
        var saveClicksAnalytics = await serv_adsUE.saveClicksAnalytics(req.body);
        if (saveClicksAnalytics == true) {
            return apiResponse.successResponseWithData(res, "Record Stored", []);
        } else {
            return apiResponse.validationErrorWithData(res, "Record Not Stored", []);
        }
    } catch (e) {
        next(e);
    }
};