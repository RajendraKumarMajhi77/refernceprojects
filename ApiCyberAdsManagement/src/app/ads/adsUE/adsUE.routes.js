let express = require("express");
var router = express.Router();
const {
    await
} = require("asyncawait");
const {
    sanitizeBody,
    validationResult
} = require("express-validator");
let jwt = require("../../../middleWares/jwt")
let Validate = require("../../../middleWares/validation");
const apiResponse = require("../../../utils/apiResponse");
const apiAdsUECtrl = require("./adsUE.ctrl");

/**
 * @Api : v1/UE
 * @method : get
 * @description : for get ads
 *
 * @returns {Object}
 */
router.get("/ads/:source", async (req, res, next) => {
    try {
        next();
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
}, apiAdsUECtrl.getAds);

/**
 * @Api : v1/UE
 * @method : post
 * @description : for post 
 *
 * @returns {Object}
 */
 router.post("/ads-clicked", async (req, res, next) => {
    try {
       next();
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
}, apiAdsUECtrl.clickAds);


module.exports = router;