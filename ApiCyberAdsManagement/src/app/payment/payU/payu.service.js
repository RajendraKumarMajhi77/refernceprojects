const {
    await
} = require('asyncawait');
const utility = require("../../../utils/utility");
const jsSHA = require("jssha");
//const payments_AdsSubscriptionModel = require("../../../models/payments_AdsSubscription")

exports.validateHash = async function (req, PayU_SALT) {
    try {

        return new Promise((resolve, reject) => {
            var key = req.body.key;
            var txnid = req.body.txnid;
            var amount = req.body.amount;
            var productinfo = req.body.productinfo;
            var firstname = req.body.firstname;
            var email = req.body.email;
            var udf5 = req.body.udf5;
            var mihpayid = req.body.mihpayid;
            var status = req.body.status;
            var responseHash = req.body.hash;

            let salt = PayU_SALT;

            let hashSequence = "key|txnid|amount|productinfo|firstname|email|phone|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10"
            hashSequence = hashSequence.split('|');

            let hashString = key + '|' + txnid + '|' + amount + '|' + productinfo + '|' + firstname + '|' + email + '|||||' + udf5 + '|||||';
            let hashArray = hashString.split('|');
            let reverseHashArray = hashArray.reverse();
            let reverseHashString = salt + '|' + status + '|' + reverseHashArray.join('|');

            let sha = new jsSHA('SHA-512', "TEXT");
            sha.update(reverseHashString);
            //Getting hashed value from sha module
            let calHash = sha.getHash("HEX");

            if (calHash == responseHash) {
                resolve(true);
            } else {
                resolve(false);
            }
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};

// exports.updatePackageSubscription = async function (req, checkHash) {
//     try {

//         /**
//          * Following status are termed as success payments in payumoney or payubiz
//          * 1. money settled
//          * 2. settlement in process
//          * 3. success
//          */
        
//         return new Promise((resolve, reject) => {
//             payments_AdsSubscriptionModel.findOneAndUpdate({
//                     "txnId": req.body.txnid
//                 }, {
//                     $set: {
//                         paidAmount: req.body.amount,
//                         gatewayRefNo: req.body.mihpayid,
//                         gatewayStatus: utility.getPaymentStatus(req.body.status),
//                         errorMessage: req.body.error_Message,
//                         gatewayStatusUpdateOn: Date.now(),
//                         paymentMode: req.body.mode,
//                         isHashMatched: checkHash,
//                         JSONDATA: String(JSON.stringify(req.body)) 
//                     }
//                 }, {
//                     safe: true,
//                     upsert: true,
//                     new: true
//                 },
//                 function (err, success) {
//                     if (err) {
//                         resolve(false);
//                     }
//                     resolve(true);
//                 }
//             );
//         });
//     } catch (e) {
//         console.log(e);
//         throw e;
//     }
// };