let express = require("express");
var router = express.Router();
const {
    sanitizeBody,
    validationResult
} = require("express-validator");
const {
    await
} = require("asyncawait");
let jwt = require("../../../middleWares/jwt");
let Validate = require("../../../middleWares/validation");
const apiResponse = require("../../../utils/apiResponse");
const apiPayUCtrl = require("./payu.ctrl")


/**
 * @Api : payment_gateway/payumoney/
 * @method : post 
 * @description : for clientpackagesubscription
 * 
 * @param {string}   txnId
 * @param {string}   fullName
 * @param {string}   email
 * @param {string}   contactNo
 * @param {string}   productinfo
 * @param {string}   userId
 * @param {string}   payableAmount
 * 
 * 
 * @returns {Object}
 */
 router.post("/clientpackagesubscription",async (req, res, next) => {
    try {
        next();
    } catch (err) {
        //throw error in json response with status 500.
        return apiResponse.ErrorResponse(res, err);
    }
},jwt.jwtauthenticate,apiPayUCtrl._payNow);

/**
 * @Api : payment_gateway/payumoney/gatewayresponse
 * @method : post 
 * @description : for gatewayresponse
 *  
 * @returns {Object}
 */
 router.post("/gatewayresponse",async (req, res, next) => {
    try { 
        next();
    } catch (err) {
        //throw error in json response with status 500.
        return apiResponse.ErrorResponse(res, err);
    }
},apiPayUCtrl.gateWayResponse);

module.exports = router;