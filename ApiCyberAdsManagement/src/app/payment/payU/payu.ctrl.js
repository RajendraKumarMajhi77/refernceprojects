const {
    await
} = require('asyncawait');
//We are using request for making an HTTP/HTTPS call to payumoney server
const request = require('request');
const jsSHA = require("jssha");
const utility = require("../../../utils/utility");
const apiResponse = require("../../../utils/apiResponse");
const serv_payU = require("./payu.service");
const Crypto = require("../../../middleWares/crypto");
const config = require("../../app.config")
const {
    v1: uuidv1,
    v4: uuidv4,
} = require('uuid');
const postForm_PayU = require("../../../mailerHtmls/postform_payumoney");


exports._payNow = async function (req, res, next) {
    try {
        //Here save all the details in pay object 
        const pay = {
            txnid: Crypto.decrypt(req.body.txnId),
            firstname: Crypto.decrypt(req.body.fullName),
            email: Crypto.decrypt(req.body.email),
            phone: Crypto.decrypt(req.body.contactNo),
            productinfo: Crypto.decrypt(req.body.productinfo),
            amount: Crypto.decrypt(req.body.payableAmount),
        }
        let hashSequence = "key|txnid|amount|productinfo|firstname|email|phone|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10"
        hashSequence = hashSequence.split('|');

        const hashString = config.PayU_MERCHANT_KEY +
            '|' + pay.txnid +
            '|' + pay.amount +
            '|' + pay.productinfo +
            '|' + pay.firstname +
            '|' + pay.email +
            '|' + '||||||||||' +
            config.PayU_SALT;
        const sha = new jsSHA('SHA-512', "TEXT");
        sha.update(hashString);
        //Getting hashed value from sha module
        const hash = sha.getHash("HEX");

        //We have to additionally pass merchant key to API
        //so remember to include it.
        pay.udf1 = "";
        pay.udf2 = "";
        pay.udf3 = "";
        pay.udf4 = "";
        pay.udf5 = "";
        pay.udf6 = "";
        pay.udf7 = "";
        pay.udf8 = "";
        pay.udf9 = "";
        pay.udf10 = "";
        pay.action = config.PayU_PAYU_BASE_URL
        pay.key = config.PayU_MERCHANT_KEY;
        pay.service_provider = config.PayU_SERVICE_PROVIDER;
        pay.surl = 'http://localhost:1677/api/payment_gateway/payumoney/gatewayresponse';
        //req.protocol + '://' + req.get('host') + 'api/payment_gateway/payumoney/gatewayresponse';
        pay.furl = 'http://localhost:1677/api/payment_gateway/payumoney/gatewayresponse';
        pay.hash = hash;

        var postFormData = postForm_PayU.PayU(pay);

        if (postFormData.length > 0) {
            console.log('======== PostForm Generated ==========', postFormData)
            return apiResponse.successResponseWithData(res, postFormData, []);
        }
    } catch (e) {
        next(e)
    }
};

exports.gateWayResponse = async function (req, res, next) {
    try {
        //console.log('Requst',req.body);
        const checkHash = await serv_payU.validateHash(req,config.PayU_SALT);
        //console.log('Check Hash',checkHash);

        if(req.body.productinfo == 'Package Subscription'){

            const updateResponse = await serv_payU.updatePackageSubscription(req,checkHash);
            //console.log('Check Hash',updateResponse);

            const return_url = config.client+ '/auth/gatewayhandler?txnid='+req.body.txnid+'&gid='+
            req.body.mihpayid+'&status='+ req.body.status;

            //console.log('return_url',return_url);
            res.redirect(return_url);
        }
    } catch (e) {
        console.log(e);
        next(e);
    }
};

async function getRouteUrl(req) {
    const fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
    return fullUrl;
}

//Making an HTTP/HTTPS call with request
// request.post({
//     headers: {
//         'Accept': 'application/json',
//         'Content-Type': 'application/json'
//     },
//     url: config.PayU_PAYU_BASE_URL, //Testing url
//     form: pay

// }, function (error, httpRes, body) {
//     if (error)
//     console.log('404 ==============\n', error)
//          res.send({
//              status: false,
//              message: error.toString()
//          });
//         console.log('httpRes.statusCode \n',httpRes.statusCode)
//     if (httpRes.statusCode === 200) {

//         console.log('200 ==============\n',body)
//         res.send(body);
//     } else if (httpRes.statusCode >= 300 &&
//         httpRes.statusCode <= 400) {
//             console.log('last ==============\n',httpRes.headers.location.toString())
//         res.redirect(httpRes.headers.location.toString());
//     }
// })