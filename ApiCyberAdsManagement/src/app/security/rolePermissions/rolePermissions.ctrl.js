const {
    await
} = require('asyncawait');
const config = require("../../app.config")
const utility = require("../../../utils/utility");
const apiResponse = require("../../../utils/apiResponse");
const serv_rolePermissions = require("./rolePermissions.service");

exports.getMappedParentMenus = async function (req, res, next) {
    try {
        var userRoleResponse = await serv_rolePermissions.findUserRoleDetails(req.query.userId);
        if (userRoleResponse != false) {
            let roleId = userRoleResponse[0].roleId,
                levelId = userRoleResponse[0].levelId,
                roleName = userRoleResponse[0].roleName;
            var aggregateMenuList = await serv_rolePermissions.roleWiseParentMenuList(roleId, roleName);
            var aggregateRoleList = await serv_rolePermissions.getLevelWiseRoles(req.query.userId);
        }
        //console.log('Parent Menu List ==================>>>>>>>>>>>>\n',JSON.stringify(aggregateMenuList));
        //console.log('Role Menu List  ==================>>>>>>>>>>>>\n',JSON.stringify(aggregateRoleList));
        let finalObjectOfList = [];
        var checkPermissions = false;
        for (var i = 0; i < aggregateMenuList.length; i++) {
            let finalObj = {};
            let roles = [];
            finalObj = {
                _id: aggregateMenuList[i]._id,
                tittle: aggregateMenuList[i].tittle,
                position: aggregateMenuList[i].position,
                icon: aggregateMenuList[i].icon,
                url: aggregateMenuList[i].url,
                isGrouped: aggregateMenuList[i].isGrouped,
                isEditable: false,
                roles: roles
            }
            for (var j = 0; j < aggregateRoleList.length; j++) {
                checkPermissions = await serv_rolePermissions.checkRoleExistsInParent(aggregateMenuList[i]._id, aggregateRoleList[j]._id);
                let permissions = {};
                permissions = {
                    roleId: aggregateRoleList[j]._id,
                    roleName: aggregateRoleList[j].roleName,
                    IsChecked: checkPermissions
                }
                if (Object.keys(permissions).length > 0) {
                    roles.push(permissions);
                }
            }
            if (Object.keys(finalObj).length > 0) {
                finalObjectOfList.push(finalObj);
            }
        }
        if (finalObjectOfList.length == 0) {
            return apiResponse.successResponseWithData(res, "No Data Found", []);
        } else {
            //console.log('Mapped Parent Menu List=====================>>>>>>>>>>>>>\n', JSON.stringify(finalObjectOfList));
            return apiResponse.successResponseWithData(res, "Data Found", finalObjectOfList);
        }
    } catch (e) {
        next(e);
    }
};

exports.getMappedChildMenus = async function (req, res, next) {
    try {
        const menuId = req.query._id;
        var userRoleResponse = await serv_rolePermissions.findUserRoleDetails(req.query.userId);
        if (userRoleResponse != false) {
            let roleId = userRoleResponse[0].roleId,
                levelId = userRoleResponse[0].levelId,
                roleName = userRoleResponse[0].roleName;
            var aggregateChildMenuList = await serv_rolePermissions.roleWiseChildMenuList(roleId, roleName, req.query._id);
            var aggregateRoleList = await serv_rolePermissions.getLevelWiseRoles(req.query.userId);
        }
      // console.log('ChildMenu=======================\n\n',JSON.stringify(aggregateChildMenuList),'\n\n');
      // console.log('RoleList=======================\n\n',JSON.stringify(aggregateRoleList),'\n\n');
        let finalObjectOfList = [];
        var checkPermissions = false;
        for (var i = 0; i < aggregateChildMenuList.length; i++) {
            let finalObj = {};
            let roles = [];
            finalObj = {
                _id: aggregateChildMenuList[i]._id,
                tittle: aggregateChildMenuList[i].tittle,
                icon: aggregateChildMenuList[i].icon,
                url: aggregateChildMenuList[i].url,
                isEditable: false,
                roles: roles
            }
            for (var j = 0; j < aggregateRoleList.length; j++) {
                var  checkPermissions = await serv_rolePermissions.checkRoleExistsInChildren(
                menuId,aggregateChildMenuList[i]._id, aggregateRoleList[j]._id);
                let permissions = {};
                permissions = {
                    roleId: aggregateRoleList[j]._id,
                    roleName: aggregateRoleList[j].roleName,
                    IsChecked: checkPermissions
                }
                if (Object.keys(permissions).length > 0) {
                    roles.push(permissions);
                }
            }
            if (Object.keys(finalObj).length > 0) {
                finalObjectOfList.push(finalObj);
            }
        }
        if (finalObjectOfList.length == 0) {
            return apiResponse.successResponseWithData(res, "No Data Found", []);
        } else {
           // console.log(JSON.stringify(finalObjectOfList),'=======================\n\n');
            return apiResponse.successResponseWithData(res, "Data Found", finalObjectOfList);
        }
    } catch (e) {
        next(e);
    }
};

exports.updateParentMenuRolePermissions = async function (req, res, next) {
    try {
        var roles = req.body.roles;
        var withoutIsChecked = req.body.withoutIsChecked;
        var isRemoved = await serv_rolePermissions.removeParentPermissions(req.body._id, withoutIsChecked);
        if (isRemoved) {
            if (roles.length > 0) {
                var isUpdated = await serv_rolePermissions.updateParentPermissions(req.body._id, roles);
                if (isUpdated) {
                    return apiResponse.successResponseWithData(res, 'Permissions Updated successfully.', []);
                } else {
                    return apiResponse.successResponseWithData(res, 'Something Went Wrong !', []);
                }
            } else {
                return apiResponse.successResponseWithData(res, 'Permissions Updated successfully.', []);
            }
        } else {
            return apiResponse.successResponseWithData(res, 'Something Went Wrong !', []);
        }
    } catch (e) {
        next(e);
    }
};


exports.updateChildMenuRolePermissions = async function (req, res, next) {
    try {
        var roles = req.body.roles;
        var withoutIsChecked = req.body.withoutIsChecked;
        var isRemoved = await serv_rolePermissions.removeChildPermissions(req.body._id,req.body.childId, withoutIsChecked);
        console.log(isRemoved);
        if (isRemoved) {
            if (roles.length > 0) {
                var isUpdated = await serv_rolePermissions.updateChildPermissions(req.body._id,req.body.childId, roles);
                if (isUpdated) {
                    return apiResponse.successResponseWithData(res, 'Permissions Updated successfully.', []);
                } else {
                    return apiResponse.successResponseWithData(res, 'Something Went Wrong !', []);
                }
            } else {
                return apiResponse.successResponseWithData(res, 'Permissions Updated successfully.', []);
            }
        } else {
            return apiResponse.successResponseWithData(res, 'Something Went Wrong !', []);
        }
    } catch (e) {
        next(e);
    }
};

// aggregateMenuList.forEach(async (el1) => {
//     let finalObj = {};
//     let roles = [];
//     finalObj = {
//         _id: el1._id,
//         tittle: el1.tittle,
//         position: el1.position,
//         icon: el1.icon,
//         url: el1.url,
//         isGrouped: el1.isGrouped,
//         roles: roles
//     }
//     aggregateRoleList.forEach(async (el2) => {
//         checkPermissions = await serv_rolePermissions.checkRoleExists(el1._id, el2._id);
//         let permissions = {};
//         permissions = {
//             roleId: el2._id,
//             roleName: el2.roleName,
//             IsChecked: checkPermissions
//         }
//         if (Object.keys(permissions).length > 0) {
//             roles.push(permissions);
//         }
//     })
//     if (Object.keys(finalObj).length > 0) {
//         finalObjectOfList.push(finalObj);
//     }
// })