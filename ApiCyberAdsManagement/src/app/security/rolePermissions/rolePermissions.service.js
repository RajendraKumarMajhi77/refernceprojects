const {
    await
} = require('asyncawait');
var mongoose = require('mongoose');
const utility = require("../../../utils/utility");
const apiResponse = require("../../../utils/apiResponse");
const menuMasterModel = require("../../../models/menuMaster")
const userModel = require("../../../models/user")
const roleModel = require("../../../models/role")


exports.findUserRoleDetails = async function (userId) {
    return new Promise((resolve, reject) => {
        const aggregate = userModel.aggregate([{
            $match: {
                '_id': mongoose.Types.ObjectId(userId)
            }
        }, {
            $lookup: {
                from: 'role',
                localField: 'roleId',
                foreignField: '_id',
                as: 'roleCollection'
            }
        }, {
            $unwind: {
                path: '$roleCollection',
                preserveNullAndEmptyArrays: true
            }
        }, {
            $project: {
                'roleId': '$roleCollection._id',
                'roleName': '$roleCollection.roleName',
                'levelId': '$roleCollection.levelId'
            }
        }]);
        aggregate.exec((err, success) => {
            if (err) {
                throw (Error(err));
            } else if (!success) {
                resolve(false);
            } else {
                resolve(success)
            }
        });
    });
};
exports.getLevelWiseRoles = async function (userId) {
    return new Promise((resolve, reject) => {
        const aggregate = userModel.aggregate([{
                $match: {
                    '_id': mongoose.Types.ObjectId(userId)
                }
            },
            {
                $lookup: {
                    from: 'role',
                    localField: 'roleId',
                    foreignField: '_id',
                    as: 'roleCollection'
                }
            },
            {
                $unwind: {
                    path: '$roleCollection',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $project: {
                    'levelId': '$roleCollection.levelId'
                }
            }
        ]);
        aggregate.exec((err, success) => {
            if (err) {
                throw (Error(err));
            } else {
                if (success[0].levelId == 1) {

                    roleModel.find({
                        "isActive": true,
                        "isDeleted": false
                    }, {}, (err, success) => {
                        if (success.length == 0) {
                            resolve(false);
                        } else if (success.length > 0) {
                            resolve(success)
                        }
                    }).sort({
                        "levelId": 1
                    });
                } else {
                    roleModel.find({
                        "isActive": true,
                        "isDeleted": false,
                        "levelId": {
                            "$gt": success[0].levelId
                        }
                    }, {}, (err, success) => {
                        if (success.length == 0) {
                            resolve(false);
                        } else if (success.length > 0) {
                            resolve(success)
                        }
                    }).sort({
                        "levelId": 1
                    });
                }
            }
        });
    });
};

exports.roleWiseParentMenuList = async function (roleId, roleName) {
    return new Promise((resolve, reject) => {
        if (roleName == 'Super Admin') {
            menuMasterModel.find({}, {}, (err, success) => {
                if (success.length == 0) {
                    resolve(false);
                } else if (success.length > 0) {
                    resolve(success)
                }
            }).sort({
                '_id': -1
            });
        } else {
            menuMasterModel.find({
                "permissions": {
                    $elemMatch: {
                        roleId: {
                            $eq: mongoose.Types.ObjectId(roleId)
                        }
                    }
                }
            }, {}, (err, success) => {
                if (success.length == 0) {
                    resolve(false);
                } else if (success.length > 0) {
                    resolve(success)
                }
            }).sort({
                '_id': -1
            });
        }
    });
};
exports.checkRoleExistsInParent = async function (mid, rid) {
    return new Promise((resolve, reject) => {
        menuMasterModel.find({
            _id: mongoose.Types.ObjectId(mid),
            "permissions": {
                $elemMatch: {
                    roleId: {
                        $eq: mongoose.Types.ObjectId(rid)
                    }
                }
            }
        }, {}, (err, success) => {
            if (success.length == 0) {
                resolve(false);
            } else if (success.length > 0) {
                resolve(true)
            }
        });
    });
};
exports.removeParentPermissions = async function (_id, withoutIsChecked) {
    try {
        return new Promise((resolve, reject) => {
            for (var i = 0; i < withoutIsChecked.length; i++) {
                menuMasterModel.findByIdAndUpdate(_id, {
                        $pull: {
                            "permissions": {
                                roleId: withoutIsChecked[i].roleId,
                            }
                        }
                    }, {
                        safe: true,
                        upsert: true,
                        new: true
                    },
                    function (err, success) {
                        if (err) {
                            resolve(false);
                        }
                        resolve(success);
                    }
                );
            }

        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};
exports.updateParentPermissions = async function (_id, roles) {
    try {
        return new Promise((resolve, reject) => {
            for (var i = 0; i < roles.length; i++) {
                menuMasterModel.findByIdAndUpdate(_id, {
                        $push: {
                            "permissions": {
                                roleId: roles[i].roleId,
                            }
                        }
                    }, {
                        safe: true,
                        upsert: true,
                        new: true
                    },
                    function (err, success) {
                        if (err) {
                            resolve(false);
                        }
                        resolve(success);
                    }
                );
            }
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};


exports.roleWiseChildMenuList = async function (roleId, roleName, menuId) {
    if (roleName == 'Super Admin') {
        return new Promise((resolve, reject) => {
            const aggregate = menuMasterModel.aggregate([{
                    $match: {
                        _id: mongoose.Types.ObjectId(menuId)
                    }
                },
                {
                    $unwind: {
                        path: '$children',
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $project: {
                        _id: '$children._id',
                        level: '$children.level',
                        tittle: '$children.tittle',
                        icon: '$children.icon',
                        url: '$children.url',
                        permissions: '$children.permissions'
                    }
                }
            ]);
            aggregate.exec((err, success) => {
                if (err) {
                    throw (Error(err));
                } else if (!success) {
                    resolve(false);
                } else {
                    resolve(success)
                }
            });
        });
    } else {
        return new Promise((resolve, reject) => {
            const aggregate = menuMasterModel.aggregate([{
                    $match: {
                        _id: mongoose.Types.ObjectId(menuId)
                    }
                },
                {
                    $unwind: {
                        path: '$children',
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $match: {
                        'children.permissions.roleId': mongoose.Types.ObjectId(roleId)
                    }
                },
                {
                    $project: {
                        _id: '$children._id',
                        level: '$children.level',
                        tittle: '$children.tittle',
                        icon: '$children.icon',
                        url: '$children.url',
                        permissions: '$children.permissions'
                    }
                }
            ]);
            aggregate.exec((err, success) => {
                if (err) {
                    throw (Error(err));
                } else if (!success) {
                    resolve(false);
                } else {
                    resolve(success)
                }
            });
        });
    }
};
exports.checkRoleExistsInChildren = async function (menuId, childrenId, roleId) {
    return new Promise((resolve, reject) => {
        const aggregate = menuMasterModel.aggregate([{
            $match: {
                _id: mongoose.Types.ObjectId(menuId)
            }
        }, {
            $unwind: {
                path: '$children',
                preserveNullAndEmptyArrays: true
            }

        }, {
            $match: {
                "children._id": mongoose.Types.ObjectId(childrenId)
            }
        }, {
            $unwind: {
                path: '$children.permissions',
                preserveNullAndEmptyArrays: true
            }
        }, {
            $match: {
                'children.permissions.roleId': mongoose.Types.ObjectId(roleId)
            }
        }, {
            $group: {
                _id: "$_id"

            }
        }]);
        aggregate.exec((err, success) => {
            if (err) {
                throw (Error(err));
            } else {
                if (success.length == 0) {
                    resolve(false);
                } else {
                    resolve(true);
                }
            }
        });

    });
};

exports.removeChildPermissions = async function (_id, childId, withoutIsChecked) {
    try {
        return new Promise((resolve, reject) => {
            for (var i = 0; i < withoutIsChecked.length; i++) {
                menuMasterModel.findOneAndUpdate({
                    "_id": _id,
                    "children._id": childId
                }, {
                    $pull: {
                        "children.$.permissions": {
                            roleId: withoutIsChecked[i].roleId,
                        }
                    }
                }, (err, success) => {
                    if (err) {
                        throw err
                    }
                    if (!success) {
                        resolve(false);
                    } else {
                        resolve(success);
                    }
                });
            }
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};
exports.updateChildPermissions = async function (_id, childId, roles) {
    try {
        return new Promise((resolve, reject) => {
            //console.log('With Checked \n',roles,_id,childId)
            for (var i = 0; i < roles.length; i++) {
                // console.log('Print >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>',roles[i]);
                menuMasterModel.findOneAndUpdate({
                        "_id": _id,
                        "children._id": childId
                    }, {
                        $push: {
                            "children.$.permissions": {
                                roleId: roles[i].roleId
                            }
                        }
                    },
                    function (err, success) {
                        if (err) {
                            resolve(false);
                        }
                        resolve(success);
                    }
                );
            }
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};

// exports.checkRoleExistsInChildrenV2 = async function (mid, cid, rid) {
//     //console.log(mid ,'>>>>>>>',cid,'>>>>>>>>', rid)
//     return new Promise((resolve, reject) => {
//         menuMasterModel.find({
//             '_id': mongoose.Types.ObjectId(mid),
//             'children._id': mongoose.Types.ObjectId(cid),
//             'children.permissions': {
//                 $elemMatch: {
//                     roleId: {
//                         $eq: mongoose.Types.ObjectId(rid)
//                     }
//                 }
//             }
//         }, {}, (err, success) => {
//             if (success.length == 0) {
//                 resolve(false);
//             } else if (success.length > 0) {
//                 resolve(true)
//             }
//         });
//     });
// };
//     menuMasterModel.updateMany({
//         _id: _id
//     }, {
//         $push: {
//             permissions: {
//                 $each: roles,
//                 // $sort: { score: -1 },
//                 // $slice: 3
//             }
//         }
//     }, options, (err, success) => {
//         if (!success || err) {
//             resolve(false);
//         } else if (success) {
//             resolve(true)
//         }
//     });


// menuMasterModel.findByIdAndUpdate(_id, {
//     $pull: {
//         permissions: {
//             $in: withoutIsChecked
//         }
//     }
// }, options, (err, success) => {
//     if (!success || err) {
//         resolve(false);
//     } else {
//         resolve(true)
//     }
// });