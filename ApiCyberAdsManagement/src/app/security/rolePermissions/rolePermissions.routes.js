let express = require("express");
var router = express.Router();
const {
    sanitizeBody,
    validationResult
} = require("express-validator");
const {
    await
} = require("asyncawait");
let jwt = require("../../../middleWares/jwt")
let Validate = require("../../../middleWares/validation");
const apiResponse = require("../../../utils/apiResponse");
const apiRolePermissionsCtrl = require("./rolePermissions.ctrl");

/**
 * 
 * * Author : rAJENDRA -- Dt: 12th feb 2021
 * * Api: security/rolepermissions/getmappedmenus
 * @method : get
 * @description : for get parent menu list
 *  
 * @returns {Object}
 */
router.get("/getmappedparentmenus", async (req, res, next) => {
    try {
        next();
    } catch (err) {
        //throw error in json response with status 500.
        return apiResponse.ErrorResponse(res, err);
    }
},jwt.jwtauthenticate,apiRolePermissionsCtrl.getMappedParentMenus);

/**
 * 
 * * Author : rAJENDRA -- Dt: 12th feb 2021
 * * Api: security/rolepermissions/getmappedmenus
 * @method : get
 * @description : for get parent menu list
 *  
 * @returns {Object}
 */
router.get("/getmappedchildmenus", async (req, res, next) => {
    try {
        next();
    } catch (err) {
        //throw error in json response with status 500.
        return apiResponse.ErrorResponse(res, err);
    }
},jwt.jwtauthenticate,apiRolePermissionsCtrl.getMappedChildMenus);


/**
 * * Author : rAJENDRA -- Dt: 19th Feb 2021
 * * Api: security/rolepermissions/updaterolepermissions
 * @method : put 
 * @description : for save/update role permissions
 *
 * @param {String}  type 
 * @param {String}  _id 
 * @param {Array}  permissions
 * 
 * @returns {Object}
 */
router.post("/updateparentmenu",Validate._updateRP, async (req, res, next) => {
    try { 
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
        } else {
            next();
        }
    } catch (err) {
        //throw error in json response with status 500.
        return apiResponse.ErrorResponse(res, err);
    }
},jwt.jwtauthenticate,apiRolePermissionsCtrl.updateParentMenuRolePermissions);

/**
 * * Author : rAJENDRA -- Dt: 19th Feb 2021
 * * Api: security/rolepermissions/updaterolepermissions
 * @method : put 
 * @description : for save/update role permissions
 *
 * @param {String}  type 
 * @param {String}  _id 
 * @param {Array}  permissions
 * 
 * @returns {Object}
 */
router.post("/updatechildmenu",Validate._updateRP, async (req, res, next) => {
    try { 
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
        } else {
            next();
        }
    } catch (err) {
        //throw error in json response with status 500.
        return apiResponse.ErrorResponse(res, err);
    }
},jwt.jwtauthenticate,apiRolePermissionsCtrl.updateChildMenuRolePermissions)


module.exports = router;