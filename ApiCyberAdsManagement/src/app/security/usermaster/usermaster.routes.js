let express = require("express");
var router = express.Router();
const {
    sanitizeBody,
    validationResult
} = require("express-validator");
const {
    await
} = require("asyncawait");
let jwt = require("../../../middleWares/jwt")
let Validate = require("../../../middleWares/validation");
const apiResponse = require("../../../utils/apiResponse");
const apiUserMasterCtrl = require("./usermaster.controller");

/**
 * * Author : rAJENDRA -- Dt: 22st Jan 2021
 * * Api: security/usermaster/userregistration
 * @method : post 
 * @description : for user registration
 *
 * @param {string}   _id [optional / in update case required] 
 * @param {string}   firstName 
 * @param {string}   lastName
 * @param {string}   email
 * @param {number}   contactNo
 * @param {string}   roleId
 * @param {string}   status  generatedFrom  hostUrl  userId
 * @param {string}   createdBy
 * @param {string}   source
 * 
 * @returns {Object}
 */
router.post("/userregistration", Validate._userregistration, sanitizeBody("*").escape(), async (req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
        } else {
            next();
        }
    } catch (err) {
        //throw error in json response with status 500.
        return apiResponse.ErrorResponse(res, err);
    }
}, apiUserMasterCtrl.userRegistration);


/**
 * * Author : rAJENDRA -- Dt: 22st Jan 2021
 * * Api: security/usermaster/entityregistration
 * @method : post 
 * @description : for user registration
 *
 * @param {number}   WebUserId 
 * @param {number}   instituteId 
 * @param {number}   typeId  
 * @param {string}   firstName 
 * @param {string}   lastName
 * @param {string}   email
 * @param {string}   contactNo
 * @param {string}   alternateContactNo
 * @param {string}   entityType
 * @param {string}   instituteName
 * @param {string}   instituteCode
 * @param {string}   instituteEmail
 * @param {string}   instituteContactNo
 * @param {string}   hostUrl
 * @param {string}   source
 * 
 * @returns {Object}
 */
router.post("/entityregistration", async (req, res, next) => {
    try {   
         next();
    } catch (err) {
        //throw error in json response with status 500.
        return apiResponse.ErrorResponse(res, err);
    }
}, apiUserMasterCtrl.entityRegistration);


/**
 * * Author : rAJENDRA -- Dt: 12th feb 2021
 * * Api: security/usermaster/getuserlist
 * @method : get
 * @description : for get user list
 * 
 * 
 * @returns {Object}
 */
router.get("/getuserlist", async (req, res, next) => {
    try {
        next();
    } catch (err) {
        //throw error in json response with status 500.
        return apiResponse.ErrorResponse(res, err);
    }
}, apiUserMasterCtrl.getUserList);

/**
 * * Author : rAJENDRA -- Dt: 12th feb 2021
 * * Api: security/usermaster/edituserlist
 * @method : get
 * @description : for get user list
 * 
 * @param {String} _id 
 * 
 * @returns {Object}
 */
router.get("/edituserlist", async (req, res, next) => {
    try {
        next();
    } catch (err) {
        //throw error in json response with status 500.
        return apiResponse.ErrorResponse(res, err);
    }
}, apiUserMasterCtrl.editUserList);

module.exports = router;