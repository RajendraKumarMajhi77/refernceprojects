const bcrypt = require("bcrypt");
const {
    await
} = require('asyncawait');
var mongoose = require('mongoose');
const utility = require("../../../utils/utility");
const apiResponse = require("../../../utils/apiResponse");
let _jwt = require("../../../middleWares/jwt");
const userModel = require("../../../models/user");
const userPasswordModel = require("../../../models/userPassword");


exports.checkEmail = async function (Opeartion, email, _id) {
    try {
        return new Promise((resolve, reject) => {
            if (Opeartion) {
                var condition = {
                    "$and": [{
                        "email": email
                    }, {
                        "status": {
                            "$ne": "DELETED"
                        }
                    }]
                }
            } else {
                var condition = {
                    "$and": [{
                        "email": email
                    }, {
                        "status": {
                            "$ne": "DELETED"
                        }
                    }, {
                        "_id": {
                            "$ne": _id
                        }
                    }]
                }
            }
            userModel.find(condition, {}, (err, success) => {
                if (success.length == 0) {
                    resolve(false);
                } else if (success.length > 0) {
                    resolve(true)
                }
            });
        });
    } catch (e) {
        console.log('service error\n', e);
        throw e;
    }
};
exports.storeOriginalPassword = async function (_id,STR_Random){
    try {
        let reqObj = new userPasswordModel({
            userId: _id,
            originalPassword: STR_Random
        });
        reqObj.save(); 
          
    } catch (e) {
        console.log('service error\n', e);
        throw e;
    }
};
exports.RegisterUser = async function (req,_status,STR_Hash) {
    try {
        console.log(req.body)
        let reqObj = new userModel({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            contactNo: req.body.contactNo,
            userName: req.body.email,
            encyptedPassword: STR_Hash,
            roleId: req.body.roleId,
            status: _status,
            source: req.body.source,
            hostUrl: req.body.hostUrl,
            createdBy: req.body.userId
        });
        return new Promise((resolve, reject) => {
            reqObj.save((err, success) => {
                if (err) {
                    reject(err);
                }
                if (!success) {
                    resolve(false);
                } else {
                    resolve(success);
                }
            });
        });
    } catch (e) {
        console.log('service error\n', e);
        throw e;
    }
};

exports.UpdateUser = async function (data){
    console.log(data);
    return new Promise((resolve, reject) => {
        var reqObj = {
            firstName: data.firstName,
            lastName: data.lastName,
            email: data.email,
            contactNo: data.contactNo,
            roleId: data.roleId,
            updatedBy: data.userId
        }
        userModel.findByIdAndUpdate(data._id, reqObj, { new: true}, (err, success) => {
            if (err) {
                reject(Error(err));
            }
            resolve(success)
        });
    });

};
exports.getUserLevel = async function (userId) {
    console.log(userId);
    return new Promise((resolve, reject) => {
        const aggregate = userModel.aggregate([{
                $match: {
                    '_id': mongoose.Types.ObjectId(userId)
                }
            },
            {
                $lookup: {
                    from: 'role',
                    localField: 'roleId',
                    foreignField: '_id',
                    as: 'roleCollection'
                }
            },
            {
                $unwind: {
                    path: '$roleCollection',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $project: {
                    'levelId': '$roleCollection.levelId'
                }
            }
        ]);
        aggregate.exec((err, success) => {
            if (err) {
                throw (Error(err));
            } else {
                resolve(success[0].levelId);      
            }
        });
    });
};

exports.UserList = async function (userlevel) {
    try {
        return new Promise((resolve, reject) => {
            const aggregate = userModel.aggregate([{
                    "$match": {
                        "status": {
                            "$ne": "DELETED"
                        }
                    }
                }, {
                    $lookup: {
                        from: 'role',
                        localField: 'roleId',
                        foreignField: '_id',
                        as: 'roleCollection'
                    }
                },
                {
                    $unwind: {
                        path: '$roleCollection',
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    "$match": {
                        "roleCollection.levelId": {
                            "$gte":userlevel
                        }
                    }
                },      
                {
                    $project: {
                        '_id': 1,
                        'firstName': 1,
                        'lastName': 1,
                        'email': 1,
                        'contactNo': 1,
                        'alternateContactNo': 1,
                        'userName': 1,
                        'password': 1,
                        'bcryptPassword': 1,
                        'originalPassword': 1,
                        'resetPasswordCount': 1,
                        'status': 1,
                        'permanentAddress': 1,
                        'source': 1,
                        'hostUrl': 1,
                        'roleName': '$roleCollection.roleName',
                        'level': '$roleCollection.level',
                    }
                },

            ]);
            aggregate.exec((err, success) => {
                if (err) {
                    throw (Error(err));
                } else if (!success) {
                    resolve(false);
                } else {
                    resolve(success);
                }
            });
        });
    } catch (e) {
        console.log('service error\n', e);
        throw e;
    }
};
exports.editUserList = async function (req) {
    try {
        return new Promise((resolve, reject) => {
            userModel.find({
                _id: req.query._id
            },{}).then((data) => {
                if (data.length > 0) {
                    resolve (data);
                } else {
                    resolve (false);
                }
            });
        });
    }catch (e) {
        console.log(e);
        throw e;
    }
}


/****************************************  entity registration ****************************/


exports.checkEntityEmail = async function (email) {
    try {
        var condition = {
            "$and": [{
                "email": email
            }, {
                "status": {
                    "$ne": "DELETED"
                }
            }]
        }
        return new Promise((resolve, reject) => {
                userModel.find(condition, {}, (err, success) => {
                    if (success.length == 0) {
                        resolve(false);
                    } else if (success.length > 0) {
                        resolve(true)
                    }
                });
            });
    } catch (e) {
        console.log('service error\n', e);
        throw e;
    }
};

exports.RegisterEntityUser = async function (req,_status,STR_Hash) {
    try {
        console.log(req.body)
        const _entityDetails = {
            instituteId:req.body.instituteId,
            typeId:req.body.typeId,
            entityType :req.body.entityType,
            instituteName: req.body.instituteName,
            instituteCode: req.body.instituteCode,
            instituteEmail: req.body.instituteEmail,
            instituteContactNo:req.body.instituteContactNo
        }
        console.log(_entityDetails)
        let reqObj = new userModel({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            contactNo: req.body.contactNo,
            alternateContactNo: req.body.alternateContactNo,
            userName: req.body.email,
            encyptedPassword: STR_Hash,
            roleId: mongoose.Types.ObjectId('6030eac56b8d6a4e70a9cc5d'), // RoleId Entity
            status: _status,
            source: req.body.source,
            hostUrl: req.body.hostUrl,
            entityDetails : _entityDetails,
            WebUserId: Number(req.body.WebUserId),
        });
        console.log(reqObj);
        return new Promise((resolve, reject) => {
            reqObj.save((err, success) => {
                if (err) {
                    reject(err);
                }
                if (!success) {
                    resolve(false);
                } else {
                    resolve(success);
                }
            });
        });
    } catch (e) {
        console.log('service error\n', e);
        throw e;
    }
};