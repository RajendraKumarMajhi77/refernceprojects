var mongoose = require('mongoose');
const bcrypt = require("bcrypt");
const {
    await
} = require('asyncawait');
const emailService = require("../../../middleWares/mailer");
const config = require("../../app.config")
const utility = require("../../../utils/utility");
const apiResponse = require("../../../utils/apiResponse");
const Service_userMaster = require("./usermaster.service");


exports.userRegistration = async function (req, res, next) {
    try {
        let _status = '';
        let Op = req.body._id == '' ? true : false;
        let checkEmail = await Service_userMaster.checkEmail(Op, req.body.email, req.body._id);
        if (checkEmail == false) {
            if (Op == true) {
                // for create new user
                if (req.body.source == 'AdsAdminPanel') {
                    _status = 'ACTIVE';
                }
                let STR_Random = utility.generateRandomString(10);
                let STR_Hash = await bcrypt.hash(STR_Random, 10);
                let RegdUser = await Service_userMaster.RegisterUser(req, _status, STR_Hash);
                if (RegdUser != false) {
                    // for storing the original password for safe side.
                    await Service_userMaster.storeOriginalPassword(RegdUser._id, STR_Random);

                    if (_status == 'ACTIVE' && RegdUser.resetPasswordCount == 0) {
                        // Html email body
                        let html = "<p> Hello " + RegdUser.firstName + " " + RegdUser.lastName + " , \n " +
                            "Admin Panel Login Details:\n" +
                            "UserName : " + RegdUser.userName +
                            "Password : " + STR_Random + "</p>";
                        // Send confirmation email

                        const mailOptions = {
                            from: config.mailDisplayName + ' <' + config.mailFrom + '>',
                            to: RegdUser.email,
                            subject: 'Account Activation.',
                            html: html // mailerOtpHtml.mailerhtml(success)
                        }
                        // emailService.send(mailOptions);
                    }
                    let msg = 'User Registered Successfully.';
                    return apiResponse.successResponseWithData(res, msg, RegdUser);
                } else {
                    let msg = 'Something Went Wrong !';
                    return apiResponse.successResponseWithData(res, msg, []);
                }
            } else {
                // for updating user Details
                var updateUser = await Service_userMaster.UpdateUser(req.body);
                let msg = 'User Details Updated Successfully.';
                return apiResponse.successResponseWithData(res, msg, updateUser);

            }
        } else {
            let msg = 'Email Already In Use.';
            return apiResponse.validationErrorWithData(res, msg, []);
        }
    } catch (e) {
        next(e);
    }
};

exports.getUserList = async function (req, res, next) {
    try {
        var _userLevel = await Service_userMaster.getUserLevel(req.query.userId);
        var userList = await Service_userMaster.UserList(_userLevel);
        if (userList == false) {
            return apiResponse.successResponseWithData(res, "No Data Found", []);
        } else {

            return apiResponse.successResponseWithData(res, "Data Found", userList);
        }
    } catch (e) {
        next(e);
    }

}

exports.editUserList = async function (req, res, next) {
    try {
        var data = await Service_userMaster.editUserList(req);
        if (data == false) {
            return apiResponse.successResponseWithData(res, "No Data Found", []);
        } else {
            console.log(data);
            return apiResponse.successResponseWithData(res, "Data Found", data);
        }
    } catch (e) {
        next(e)
    }
}


exports.entityRegistration = async function (req, res, next) {
    try {
        let _status = '';
        let checkEmail = await Service_userMaster.checkEntityEmail(req.body.email);
        if (checkEmail == false) {
            //console.log(checkEmail, '\ndata',req.body);
            // for create new user
            if (req.body.source == 'UEAdminPanel') {
                _status = 'ACTIVE';
            }
            let STR_Random = utility.generateRandomString(10);
            let STR_Hash = await bcrypt.hash(STR_Random, 10);
            let RegdUser = await Service_userMaster.RegisterEntityUser(req, _status, STR_Hash);
            if (RegdUser != false) {
                // for storing the original password for safe side.
                await Service_userMaster.storeOriginalPassword(RegdUser._id, STR_Random);

                if (_status == 'ACTIVE' && RegdUser.resetPasswordCount == 0) {
                    // Html email body
                    let html = "<p> Hello " + RegdUser.firstName + " " + RegdUser.lastName + " , \n " +
                        "Admin Panel Login Details:\n" +
                        "UserName : " + RegdUser.userName +
                        "Password : " + STR_Random + "</p>";
                    // Send confirmation email

                    const mailOptions = {
                        from: config.mailDisplayName + ' <' + config.mailFrom + '>',
                        to: RegdUser.email,
                        subject: 'Account Activation.',
                        html: html // mailerOtpHtml.mailerhtml(success)
                    }
                    //emailService.send(mailOptions);
                }
                let msg = 'User Registered Successfully.';
                return apiResponse.successResponseWithData(res, msg, RegdUser);
            } else {
                let msg = 'Something Went Wrong !';
                return apiResponse.successResponseWithData(res, msg, []);
            }
        } else {
            let msg = 'Email Already In Use.';
            return apiResponse.validationErrorWithData(res, msg, []);
        }
    } catch (e) {
        next(e);
    }
};