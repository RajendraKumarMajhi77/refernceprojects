const {
    await
} = require('asyncawait');
const utility = require("../../../utils/utility");
const apiResponse = require("../../../utils/apiResponse");
const serv_menuMaster = require('./menuMaster.service')


exports.addMenu = async function (req, res, next) {
    try {
        var isExistsMainMenu = await serv_menuMaster.addmenu(req);
        if (isExistsMainMenu == true) {
            return apiResponse.validationErrorWithData(res, req.body.tittle + " Menu Already Exists.", []);
        } else {
            var responseData = isExistsMainMenu;
            var msg = req.body._id == '' ? 'Menu Added Successfully' : 'Menu Updated Successfully';
            return apiResponse.successResponseWithData(res, msg, responseData);
        }
    } catch (e) {
        console.log("/--- error occured in user saveuserlogin.| post  -----/");
        next(e);
    }
};

exports.addSubMenu = async function (req,res,next){
    try {
        let info = req.body; 
        let result = await serv_menuMaster.findSubMenu(info);
        if (!result) {
            return apiResponse.successResponseWithData(res, "Data Found", []);
        } else {
            var msg = req.body._id != '' && req.body._idchild == ''? 'Sub Menu Added Successfully' : 'Sub Menu Updated Successfully';
            console.log(result);
            return apiResponse.successResponseWithData(res, msg, result);
        }
    } catch (e) {
        console.log("/--- error occured in user saveuserlogin.| post  -----/");
        next(e);
    }

};

exports.getMenuList = async function (req, res, next) {
    try {
        var data = await serv_menuMaster.getMenuList(req);
        if (data == false) {
            return apiResponse.successResponseWithData(res, "No Data Found", []);
        } else {
            return apiResponse.successResponseWithData(res, "Data Found", data);
        }
    } catch (e) {
        console.log("/--- error occured in getMenuList | get -----/");
        next(e)
    }
};
