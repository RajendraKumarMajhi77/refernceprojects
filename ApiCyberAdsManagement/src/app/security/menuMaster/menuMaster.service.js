const {
    await
} = require('asyncawait');
const utility = require("../../../utils/utility");
const iconModel = require('../../../models/iconMaster');
const menuMasterModel = require("../../../models/menuMaster");


exports.addmenu = async function (req) {
    try {
        if (req.body._id == '') {
            // TODO: add main menu ..
            var condition = {
                "$and": [{
                    "tittle": req.body.tittle
                }, {
                    "level": req.body.level
                }, {
                    "isActive": true
                }, {
                    "isDeleted": false
                }, ]
            }
            return new Promise((resolve, reject) => {
                const query = menuMasterModel.findOne(condition);
                query.exec((err, success) => {
                    if (err) {
                        reject(Error(err));
                    } else if (success) {
                        console.log('\n>>>>>>>>>>>>>>>>> Data Exists >>>>>>>>>>>>>>>>\n', success, '\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n');
                        resolve(true);
                    } else {
                        let children = [];
                        let permissions = [];
                        let objData = new menuMasterModel({
                            level: req.body.level,
                            tittle: req.body.tittle,
                            isGrouped: req.body.isGrouped,
                            icon: req.body.icon,
                            position: req.body.position,
                            url: req.body.url,
                            permissions: permissions,
                            children: children
                        });
                        objData.save(function (err, success) {
                            if (err) {
                                reject(err);
                            } else if (!success) {
                                resolve(true);
                            }
                            resolve(success);
                        });
                    }
                });
            });
        } else {
            // TODO: update menu ..
            var condition = {
                "$and": [{
                    "tittle": req.body.tittle
                }, {
                    "level": req.body.level
                }, {
                    "isActive": true
                }, {
                    "isDeleted": false
                }, {
                    "_id": {
                        "$ne": req.body._id
                    }
                }]
            }
            return new Promise((resolve, reject) => {
                const query = menuMasterModel.findOne(condition);
                query.exec((err, success) => {
                    if (err) {
                        reject(Error(err));
                    } else if (success) {
                        console.log('\n>>>>>>>>>>>>>>>>> Data Exists >>>>>>>>>>>>>>>>\n', success, '\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n');
                        resolve(true);
                    } else {
                        var data = {
                            level: req.body.level,
                            tittle: req.body.tittle,
                            isGrouped: req.body.isGrouped,
                            icon: req.body.icon,
                            position: req.body.position,
                            url: req.body.url
                        }
                        menuMasterModel.findByIdAndUpdate(req.body._id, data, {
                            new: true
                        }, (err, success) => {
                            if (err) {
                                reject(Error(err));
                            }
                            resolve(success)
                        });
                    }
                });
            });
        }
    } catch (e) {
        console.log(e);
        throw e;
    }
};
exports.findSubMenu = async function (info) {
    try {
        console.log(info);
        if (info._id != '' && info._idchild == '') {
            return new Promise((resolve, reject) => {
                menuMasterModel.findByIdAndUpdate(info._id, {
                        $push: {
                            "children": {
                                level: info.level,
                                tittle: info.tittle,
                                icon: info.icon,
                                url: info.url
                            }
                        }
                    }, {
                        safe: true,
                        upsert: true,
                        new: true
                    },
                    function (err, success) {
                        if (err) {
                            resolve(false);
                        }
                        resolve(success);
                    }
                );
            });
        }
        else{
            return new Promise((resolve, reject) => {
                menuMasterModel.updateOne({"_id": info._id,"children._id": info._idchild}, 
                {$set: {
                    "children.$.level": info.level,
                    "children.$.tittle": info.tittle,
                    "children.$.icon": info.icon,
                    "children.$.url": info.url
                }}, (err, success) => {
                    if(err){throw err}
                    if (!success) {
                        resolve(false);
                    } else {
                        resolve(success);
                    }
                });
            });
        }
    } catch (e) {
        console.log(e);
        throw e;
    }
};

exports.getMenuList = async function (req) {
    try {
        var condition = {
            isActive: true,
            isDeleted: false
        };
        return new Promise((resolve, reject) => {
            menuMasterModel.find({}, {}, (err, success) => {
                if (!success) {
                    resolve(false);
                } else {
                    resolve(success);
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};
