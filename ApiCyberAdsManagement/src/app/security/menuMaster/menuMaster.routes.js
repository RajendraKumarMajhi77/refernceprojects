let express = require("express");
var router = express.Router();
const {
    sanitizeBody,
    validationResult
} = require("express-validator");
const {
    await
} = require("asyncawait");
let jwt = require("../../../middleWares/jwt")
let Validate = require("../../../middleWares/validation");
const apiResponse = require("../../../utils/apiResponse");
const apiMenuMasterCtrl = require("./menuMaster.ctrl")



/**
 * * Author : rAJENDRA -- Dt: 1st feb 2021
 * * Api : security/addmenu
 * @method : post 
 * @description : for add menu
 *
 * 
 * @returns {Object}
 */
router.post("/addmenu", async (req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
        } else {
            next();
        }
    } catch (err) {
        return apiResponse.ErrorResponse(res, err);
    }
}, apiMenuMasterCtrl.addMenu);

/**
 * * Author : rAJENDRA -- Dt: 1st feb 2021
 * * Api : security/addmenu
 * @method : post 
 * @description : for add menu
 *
 * 
 * @returns {Object}
 */
router.post("/addsubmenu", async (req, res, next) => {
    try {
        next();
        // const errors = validationResult(req);
        // if (!errors.isEmpty()) {
        //     return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
        // } else {
            
        // }
    } catch (err) {
        return apiResponse.ErrorResponse(res, err);
    }
}, apiMenuMasterCtrl.addSubMenu);

/**
 * * Author : rAJENDRA -- Dt: 1st feb 2021
 * * Api : security/getmenulist
 * @method : get
 * @description : for get role list
 *
 * @returns {Object}
 */
router.get("/getmenulist", async (req, res, next) => {
    try {      
         next();
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
},apiMenuMasterCtrl.getMenuList); 


module.exports =  router;