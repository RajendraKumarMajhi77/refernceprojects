const bcrypt = require("bcrypt");
const {
    await
} = require('asyncawait');
var mongoose = require('mongoose');
const utility = require("../../../utils/utility");
const roleModel = require("../../../models/role");


exports.findRole = async function (req) {
    try {
        if (req.body._id == '') {
            // TODO: add Role ..
            var condition = {
                "$and": [{
                    "roleName": req.body.roleName
                }, {
                    "level": req.body.level
                }, {
                    "isActive": true
                }, {
                    "isDeleted": false
                }, ]
            }
            return new Promise((resolve, reject) => {
                let query = roleModel.findOne(condition);
                query.exec((err, success) => {
                    if (err) {
                        reject(Error(err));
                    } else if (success) {
                        console.log('\n>>>>>>>>>>>>>>>>> Data Exists >>>>>>>>>>>>>>>>\n', success, '\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n');
                        resolve(true);
                    } else {
                        let objData = new roleModel({
                            roleName: req.body.roleName,
                            roleShortName: req.body.roleShortName,
                            level: req.body.level,
                            levelId: utility.getLevelId(req.body.level),
                            createdBy: req.body.userId
                        });
                        objData.save(function (err, success) {
                            if (err) {
                                reject(Error(err));
                            }
                            resolve(success)
                        });
                    }
                });
            });
        } else {
            // TODO: update role ..
            var condition = {
                "$and": [{
                    "roleName": req.body.roleName
                }, {
                    "level": req.body.level

                }, {
                    "_id": {
                        "$ne": req.body._id
                    }
                }]
            }
            return new Promise((resolve, reject) => {
                let query = roleModel.findOne(condition);
                query.exec((err, success) => {
                    if (err) {
                        reject(Error(err));
                    } else if (success) {
                        console.log('\n>>>>>>>>>>>>>>>>> Data Exists >>>>>>>>>>>>>>>>\n', success, '\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n');
                        resolve(true);
                    } else {
                        var data = {
                            roleName: req.body.roleName,
                            roleShortName: req.body.roleShortName,
                            level: req.body.level,
                            levelId: utility.getLevelId(req.body.level),
                            updatedBy: req.body.userId
                        }
                        roleModel.findByIdAndUpdate(req.body._id, data, {
                            new: true
                        }, (err, success) => {
                            if (err) {
                                reject(Error(err));
                            }
                            resolve(success)
                        });
                    }
                });
            });
        }
    } catch (e) {
        console.log(e);
        throw e;
    }
};

exports.getRoleList = async function (req) {
    try {
        var condition = {
            isActive: true,
            isDeleted: false
        };
        return new Promise((resolve, reject) => {
            roleModel.find(condition, {}, (err, success) => {
                if (!success) {
                    resolve(false);
                } else {
                    resolve(success);
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};

exports.editRoleList = async function (req) {
    try {
        return new Promise((resolve, reject) => {
            roleModel.find({
                _id: req.query._id
            }, " _id roleName roleShortName level isActive isDeleted").then((data) => {
                if (data.length > 0) {
                    resolve(data);
                } else {
                    resolve(false);
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};

exports.updateRoleListStatus = async function (req) {
    try {
        var data = {};
        if (req.body.statusType == 'ActiveinActive') {
            data = {
                isActive: req.body.status == false ? true : false,
                updatedBy: req.body.userId
            }
        } else if (req.body.statusType == 'Delete') {
            data = {
                isActive: false,
                isDeleted: true,
                deletedBy: req.body.userId,
                deletedOn: new Date()
            }
        }
        return new Promise((resolve, reject) => {
            roleModel.findByIdAndUpdate(req.body._id, data, (err, success) => {
                if (!success) {
                    resolve(false);
                } else {
                    resolve(data);
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};