let express = require("express");
var router = express.Router();
const {
    sanitizeBody,
    validationResult
} = require("express-validator");
const {
    await
} = require("asyncawait");
let Validate = require("../../../middleWares/validation");
const apiResponse = require("../../../utils/apiResponse");
const apiRoleMasterCtrl = require("./roleMaster.ctrl");


/**
 * * Author : rAJENDRA -- Dt: 1st feb 2021
 * @Api : role/addrole
 * @method : post 
 * @description : for add/update role
 *
 * @param {string}   roleName 
 * @param {string}   roleShortName
 * @param {string}   level 
 * @param {string}   userId
 * @param {string}   _id  
 * 
 * @returns {Object}
 */
router.post("/addrole", Validate._addrole, sanitizeBody("*").escape(),async (req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
        } else {
            next();
        }
    } catch (err) {
        return apiResponse.ErrorResponse(res, err);
    }
},apiRoleMasterCtrl.addrole);

/**
 * * Author : rAJENDRA -- Dt: 1st feb 2021
 * @Api : role/getrolelist
 * @method : get
 * @description : for get role list
 *
 * @returns {Object}
 */
router.get("/getrolelist", async (req, res, next) => {
    try {      
         next();
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
},apiRoleMasterCtrl.getRoleList);
  
/**
 * * Author : rAJENDRA -- Dt: 1st feb 2021
 * @Api : role/editrolelist
 * @method : get
 * @description : for edit role list
 * 
 * @param {string}  _id
 * 
 * @returns {Object}
 */
router.get("/editrolelist", async (req, res, next) => {
    try { 
         next();
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
},apiRoleMasterCtrl.editRoleList);


/**
 * * Author : rAJENDRA -- Dt: 1st feb 2021
 * @Api : role/updateroleliststatus
 * @method : post 
 * @description : for update role list status
 *
 * @param {string}   statusType 
 * @param {boolean}  status 
 * @param {string}   userId
 * @param {string}   _id  
 * 
 * @returns {Object}
 */
router.post("/updateroleliststatus", Validate._updateroleliststatus, sanitizeBody("*").escape(),async (req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
        } else {
            next();
        }
    } catch (err) {
        return apiResponse.ErrorResponse(res, err);
    }
},apiRoleMasterCtrl.updateRoleListStatus);






module.exports = router;