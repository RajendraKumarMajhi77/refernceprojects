const {
    await
} = require('asyncawait');
const utility = require("../../../utils/utility");
const apiResponse = require("../../../utils/apiResponse");
const serv_roleMaster = require("./roleMaster.service");

exports.addrole = async function (req, res, next) {
    try {
        var roleData = await serv_roleMaster.findRole(req);
        if (roleData == true) {
            return apiResponse.validationErrorWithData(res, req.body.roleName + " Role Already Exists.", []);
        } else {
            var responseData = roleData;
            var msg = req.body._id == '' ? 'Role Added Successfully' : 'Role Updated Successfully';
            return apiResponse.successResponseWithData(res, msg, responseData);
        }
    } catch (e) {
        console.log("/--- error occured in addrole.| post  -----/");
        next(e)
    }
};

exports.getRoleList = async function (req, res, next) {
    try {
        var data = await serv_roleMaster.getRoleList(req);
        if (data == false) {
            return apiResponse.successResponseWithData(res, "No Data Found", []);
        } else {
            return apiResponse.successResponseWithData(res, "Data Found", data);
        }
    } catch (e) {
        console.log("/--- error occured in getRoleList | get -----/");
        next(e)
    }
};

exports.editRoleList = async function (req, res, next) {
    try {
        var data = await serv_roleMaster.editRoleList(req);
        if (data == false) {
            return apiResponse.successResponseWithData(res, "No Data Found", []);
        } else {
            return apiResponse.successResponseWithData(res, "Data Found", data);
        }
    } catch (e) {
        console.log("/--- error occured in editRoleList | get  -----/");
        next(e)
    }
};

exports.updateRoleListStatus = async function (req, res, next) {
    try {
        var updateStatus = await serv_roleMaster.updateRoleListStatus(req);
        var responseData = updateStatus;
        if(updateStatus == false) {
            return apiResponse.validationErrorWithData(res, "Data not found.", []);
        }else {
            var msg = req.body.statusType == 'Delete' ? 'Role Deleted Successfully' : 'Status Updated Successfully';
            return apiResponse.successResponseWithData(res, msg, responseData);
        }  
    } catch (e) {
        console.log("/--- error occured in updateRoleListStatus.| post  -----/");
        next(e)
    }
};