'use strict';

const dotenv = require('dotenv');
const assert = require('assert');

dotenv.config();

// #Express environment variable.
const {
    node_env,
    port,
    secretKey,
    key,
    server,
    url,
    client
} = process.env;
// #Express jwt authentication service.
const {
    JWT_SECRET,
    JWT_TIMEOUT_DURATION,
    JWT_ApiRequest_TIMEOUT_DURATION
} = process.env;

// #Node Mailer UE Configuration
const {
    ue_EMAIL_SMTP_HOST,
    ue_EMAIL_SMTP_PORT,
    ue_EMAIL_DisplayName,
    ue_EMAIL_SMTP_USERNAME,
    ue_EMAIL_SMTP_PASSWORD,
    ue_EMAIL_SMTP_SECURE
} = process.env;

// #PayuMoney Test kit
const {
    test_SALT,
    test_MERCHANT_KEY,
    test_PAYU_BASE_URL,
    test_SERVICE_PROVIDER,
    live_SALT,
    live_MERCHANT_KEY,
    live_PAYU_BASE_URL,
    live_SERVICE_PROVIDER,
} = process.env;

module.exports = {
    ENV: node_env,
    port: port,
    secretKey: secretKey,
    key: key,
    server: server,
    url: url,
    client:client,
    
    JWT_SECRET: JWT_SECRET,
    JWT_TIMEOUT_DURATION: JWT_TIMEOUT_DURATION,
    JWT_ApiRequest_TIMEOUT_DURATION: JWT_ApiRequest_TIMEOUT_DURATION,

    maillHost: ue_EMAIL_SMTP_HOST,
    mailFrom: ue_EMAIL_SMTP_USERNAME,
    mailDisplayName: ue_EMAIL_DisplayName,
    mailPortNo: ue_EMAIL_SMTP_PORT,
    mailPassword: ue_EMAIL_SMTP_PASSWORD,
    mailSecure: ue_EMAIL_SMTP_SECURE,

    PayU_SALT: test_SALT,
    PayU_MERCHANT_KEY: test_MERCHANT_KEY,
    PayU_PAYU_BASE_URL: test_PAYU_BASE_URL,
    PayU_SERVICE_PROVIDER: test_SERVICE_PROVIDER
}