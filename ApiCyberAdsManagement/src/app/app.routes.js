var express = require("express");
var app = express();

//=============================================================================================================
//                                 ||   Authentication Pages routes ||                                       ||
//=============================================================================================================
var routes_authentication = require("./authentication/authentication.routes");
app.use("/auth/", routes_authentication);

//=============================================================================================================
//                                 ||   Dashboard Page routes ||                                       ||
//=============================================================================================================
var routes_dashboard = require("./dashboard/dashboard.routes");
app.use("/v1/dashboard/", routes_dashboard);

//=============================================================================================================
//                                 ||    All security Pages routes    ||                                       ||
//=============================================================================================================
var routes_rolemaster = require("./security/roleMaster/roleMaster.routes");
var routes_usermaster = require("./security/usermaster/usermaster.routes");
var routes_menumaster = require("./security/menuMaster/menuMaster.routes");
var routes_rolepermissiomns = require("./security/rolePermissions/rolePermissions.routes");
app.use("/security/rolemaster",routes_rolemaster);
app.use("/security/usermaster",routes_usermaster);
app.use("/security/menumaster",routes_menumaster);
app.use("/security/rolepermissions",routes_rolepermissiomns);

//=============================================================================================================
//                                 ||    All master Pages routes    ||                                       ||
//=============================================================================================================
var routes_adstype = require("./masters/adsType/adsType.routes");
app.use("/v1/master-adstype",routes_adstype);
var routes_adslocation = require("./masters/adsLocation/adsLocation.routes");
app.use("/v1/master-adslocation",routes_adslocation);
var routes_adsallocation = require("./masters/adsAllocation/adsAllocation.routes");
app.use("/v1/master-adsallocation",routes_adsallocation);

//=============================================================================================================
//                                 ||    Ads Pages routes    ||                                       ||
//=============================================================================================================
var routes_adscenter = require("./ads/adsCenter/adsCenter.routes");
app.use("/v1/ads-adscenter",routes_adscenter);


//=============================================================================================================
//                                 ||    Analytics Page routes||                                       ||
//=============================================================================================================
var routes_adsCampaignList= require("./analytics/adsCampaignList/adsCampaignList.routes");
app.use("/v1/analytics-adscampaign",routes_adsCampaignList);


//=============================================================================================================
//                                 ||    All Payment Gateway routes   -- PayU Money ||                                       ||
//=============================================================================================================
var routes_payuMoney= require("./payment/payU/payu.routes");
app.use("/payment_gateway/payumoney",routes_payuMoney);

//=============================================================================================================
//                                 ||    sCRIPT  routes   -- UE ADS ||                                       ||
//=============================================================================================================
var routes_adsUE= require("./ads/adsUE/adsUE.routes");
app.use("/v1/UE",routes_adsUE);



module.exports = app;