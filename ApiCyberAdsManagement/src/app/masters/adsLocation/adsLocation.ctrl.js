const {
    await
} = require('asyncawait');
var mongoose = require('mongoose');
const utility = require("../../../utils/utility");
const apiResponse = require("../../../utils/apiResponse");
const serv_adsLocation = require("./adsLocation.service");

exports.addAdsLocation = async function (req, res, next) {
    try {
        console.log('RequestBody>>>>>>>>>>>>>>\n', req.body);
        var isExists = await serv_adsLocation.findAdsLocation(req);
        console.log(isExists)
        if (isExists == true) {
             let msg = req.body.pageName + ' already exists with the following configuration.'
             return apiResponse.validationErrorWithData(res, msg, []);
         } else {
             // Opeartaion Save
            if (req.body._id == "") {
                var addRequestData = await serv_adsLocation.addAdsLocation(req);
               if (addRequestData == false) {
                    let msg = 'Something Went Wrong!.'
                    return apiResponse.validationErrorWithData(res, msg, []);
                } else {
                    let msg = 'Ads Location Added Successfully';
                    // console.log('Save>>>>\n',addRequestData);
                    return apiResponse.successResponseWithData(res, msg, addRequestData);
                }
             } // Operation Updated 
            else {
                var updateRequestData = await serv_adsLocation.updateAdsLocation(req);
                if (updateRequestData == false) {
                    let msg = 'Something Went Wrong!.'
                    return apiResponse.validationErrorWithData(res, msg, []);
                } else {
                    let msg = 'Ads Location Updated Successfully';
                    //console.log('Update>>>>\n',updateRequestData);
                    return apiResponse.successResponseWithData(res, msg, updateRequestData);
                }
            }
         }
    } catch (e) {
        console.log("/--- error occured in addAdsLocation | post  -----/");
        next(e)
    }
};

exports.getAdsLocationList = async function (req, res, next) {
    try {
        var Data = await serv_adsLocation.getAdsLocationList(req);
        if(Data != false){
            for(var i = 0; i <Data.length; i++){
                if(Data[i].parentId != ''){
                    Data[i].parentMenu = await serv_adsLocation.
                    getMappedParentMenu(mongoose.Types.ObjectId(Data[i].parentId));
                }
                else{
                    Data[i].parentMenu = '--'
                }
            } 
        }
        console.log(Data)
        if (Data == false) {
            return apiResponse.validationErrorWithData(res, "No Data Found", []);
        } else {
            return apiResponse.successResponseWithData(res, "Data Found", Data);
        }
    } catch (e) {
        console.log("/--- error occured in getAdsLocationList | get -----/");
        next(e)
    }
};

exports.editAdsLocation = async function (req, res, next) {
    try {
        //console.log(req)
        var Data = await serv_adsLocation.editAdsLocation(req.params._id);
        // console.log('Edit Data',Data);
        if (Data == false) {
            return apiResponse.validationErrorWithData(res, "No Data Found", []);
        } else {
            return apiResponse.successResponseWithData(res, "Data Found", Data);
        }
    } catch (e) {
        console.log("/--- error occured in editAdsLocation | get -----/");
        next(e)
    }
};

exports.getLevel1PageList = async function (req, res, next) {
    try {
        //console.log(req)
        var Data = await serv_adsLocation.getLevel1PageList();
        // console.log('Edit Data',Data);
        if (Data == false) {
            return apiResponse.validationErrorWithData(res, "No Data Found", []);
        } else {
            return apiResponse.successResponseWithData(res, "Data Found", Data);
        }
    } catch (e) {
        console.log("/--- error occured in getLevel1PageList | get -----/");
        next(e)
    }
};