let express = require("express");
var router = express.Router();
const {
    await
} = require("asyncawait");
const {
    sanitizeBody,
    validationResult
} = require("express-validator");
let jwt = require("../../../middleWares/jwt")
let Validate = require("../../../middleWares/validation");
const apiResponse = require("../../../utils/apiResponse");
const apiAdsLocationCtrl = require("./adsLocation.ctrl");

/**
 * @Api : v1/master-adslocation/saveadslocation
 * @method : post 
 * @description : for add/update adslocation
 * 
 * @param {string}      _id  [adsLocation collection pk]
 * @param {string}      pageLevel
 * @param {string}      pageName 
 * @param {string}      pageUrl  
 * @param {number}      parentId
 * @param {string}      userId 
 * 
 * @returns {Object}
 */
 router.post("/saveadslocation", async (req, res, next) => {
    try {
       // const errors = validationResult(req);
        //console.log(errors);
        //if (!errors.isEmpty()) {
        //    return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
        //} else {        
            next();
        //}
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
},apiAdsLocationCtrl.addAdsLocation);

/**
 * @Api : v1/master-adslocation/getadslocation
 * @method : get
 * @description : for get ads location
 *
 * @returns {Object}
 */
 router.get("/getadslocation", async (req, res, next) => {
    try {
        next();
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
}, apiAdsLocationCtrl.getAdsLocationList);

/**
 * @Api : v1/master-adslocation/getadslocation
 * @method : get
 * @description : for get adslocation
 *
 * @returns {Object}
 */
 router.get("/getadslocation/:_id", async (req, res, next) => {
    try {
        next();
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
}, apiAdsLocationCtrl.editAdsLocation);

/**
 * @Api : v1/master-adslocation/getlevel1pagelist
 * @method : get
 * @description : for get level1 pagelist
 *
 * @returns {Object}
 */
 router.get("/getlevel1pagelist", async (req, res, next) => {
    try {
        next();
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
}, apiAdsLocationCtrl.getLevel1PageList);


module.exports = router;