const {
    await
} = require('asyncawait');
var mongoose = require('mongoose');
const adsLocationCollection = require("../../../models/adsLocation");

exports.findAdsLocation = async function (req) {
    try {
        var condition = {
            "$and": [{
                "isActive": true
            }, {
                "isDeleted": false
            }, {
                "pageLevel": req.body.pageLevel
            }, {
                "pageName": req.body.pageName
            }, {
                "pageUrl": req.body.pageUrl
            }, {
                "parentId": req.body.parentId
            }]
        }

        // console.log('condition',condition);
        if (req.body._id != '') {
            condition.$and.push({
                "_id": {
                    "$ne": mongoose.Types.ObjectId(req.body._id)
                }
            })
        }
        //  console.log('<><><><><><><><><><>',condition )
        return new Promise((resolve, reject) => {
            adsLocationCollection.find(condition, {}, (err, success) => {
                // console.log(err,success);
                if (err || success.length == 0) {
                    resolve(false);
                } else {
                    resolve(true);
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};

exports.addAdsLocation = async function (req) {
    try {
        let reqObj = new adsLocationCollection({
            pageLevel: req.body.pageLevel,
            pageName: req.body.pageName,
            pageUrl: req.body.pageUrl,
            createdBy: req.body.userId,
            parentId: req.body.parentId
        });

        console.log(req, '>>>>\n', reqObj);
        return new Promise((resolve, reject) => {
            reqObj.save((err, success) => {
                if (err) {
                    reject(err);
                }
                if (!success) {
                    resolve(false);
                } else {
                    resolve(success);
                }
            });
        });
    } catch (e) {
        console.log('service error\n', e);
        throw e;
    }
};

exports.updateAdsLocation = async function (req) {
    try {
        return new Promise((resolve, reject) => {
            adsLocationCollection.findByIdAndUpdate({
                _id: mongoose.Types.ObjectId(req.body._id)
            }, {
                pageLevel: req.body.pageLevel,
                pageName: req.body.pageName,
                pageUrl: req.body.pageUrl,
                createdBy: req.body.userId,
                parentId: req.body.parentId,
                updatedBy: req.body.userId
            }, (err, success) => {
                if (err || !success) {
                    resolve(false);
                } else {
                    resolve(success);
                }
            });
        });
    } catch (e) {
        console.log('service error\n', e);
        throw e;
    }
};

exports.getAdsLocationList = async function () {
    try {
        return new Promise((resolve, reject) => {
            const aggregate = adsLocationCollection.aggregate([{
                '$match': {
                    'isActive': true,
                    'isDeleted': false
                }
            }, {
                '$project': {
                    '_id': 1,
                    'pageLevel': 1,
                    'pageName': 1,
                    'pageUrl': 1,
                    'parentId': 1,
                    'updatedAt': 1
                }
            }]);
            aggregate.exec((err, success) => {
                if (err) {
                    throw (Error(err));
                } else if (!success) {
                    resolve(false);
                } else {
                    resolve(success)
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};

exports.editAdsLocation = async function (_id) {
    try {
        return new Promise((resolve, reject) => {
            const aggregate = adsLocationCollection.aggregate([{
                '$match': {
                    'isActive': true,
                    'isDeleted': false,
                    '_id': mongoose.Types.ObjectId(_id)
                }
            }, {
                '$project': {
                    '_id': 1,
                    'pageLevel': 1,
                    'pageName': 1,
                    'pageUrl': 1,
                    'parentId': 1,
                    'updatedAt': 1
                }
            }]);
            aggregate.exec((err, success) => {
                if (err) {
                    throw (Error(err));
                } else if (!success) {
                    resolve(false);
                } else {
                    resolve(success)
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};

exports.getLevel1PageList = async function () {
    try {
        return new Promise((resolve, reject) => {
            const aggregate = adsLocationCollection.aggregate([{
                '$match': {
                    'isActive': true,
                    'isDeleted': false,
                    'pageLevel': 'Level-1'
                }
            }, {
                '$project': {
                    '_id': 1,
                    'pageLevel': 1,
                    'pageName': 1,
                    'pageUrl': 1,
                    'parentId': 1,
                    'updatedAt': 1
                }
            }]);
            aggregate.exec((err, success) => {
                if (err) {
                    throw (Error(err));
                } else if (!success) {
                    resolve(false);
                } else {
                    resolve(success)
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};


exports.getMappedParentMenu = async function (_id) {
    try {
        return new Promise((resolve, reject) => {
            const aggregate = adsLocationCollection.aggregate([{
                '$match': {
                    'isActive': true,
                    'isDeleted': false,
                    'pageLevel': 'Level-1',
                    '_id': _id
                }
            }, {
                '$project': {
                    '_id': 1,
                    'pageLevel': 1,
                    'pageName': 1,
                    'pageUrl': 1,
                    'parentId': 1,
                    'updatedAt': 1
                }
            }]);
            aggregate.exec((err, success) => {
                if (err) {
                    throw (Error(err));
                } else if (!success) {
                    resolve(false);
                } else {
                    resolve(success[0].pageName)
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};