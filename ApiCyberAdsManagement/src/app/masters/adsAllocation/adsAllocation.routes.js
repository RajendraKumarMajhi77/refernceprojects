let express = require("express");
var router = express.Router();
const {
    await
} = require("asyncawait");
const {
    sanitizeBody,
    validationResult
} = require("express-validator");
let jwt = require("../../../middleWares/jwt")
let Validate = require("../../../middleWares/validation");
const apiResponse = require("../../../utils/apiResponse");
const apiAdsAllocationCtrl = require("./adsAllocation.ctrl");

/**
 * @Api : v1/master-adsallocation/getlevelwisepages
 * @method : get
 * @description : for get level wisepages
 *
 * @returns {Object}
 */
    router.get("/getlevelwisepages/:level", async (req, res, next) => {
        try {
            next();
        } catch (err) {
            console.log(err);
            return apiResponse.ErrorResponse(res, err);
        }
    }, apiAdsAllocationCtrl.getlevelwisepages);

/**
 * @Api : v1/master-adsallocation/getassignedadstype
 * @method : get
 * @description : for getassignedadstype
 *
 * @returns {Object}
 */
     router.get("/getassignedadstype", async (req, res, next) => {
        try {
            next();
        } catch (err) {
            //console.log(err);
            return apiResponse.ErrorResponse(res, err);
        }
    }, apiAdsAllocationCtrl.getAssignedAdsType);


/**
 * @Api : v1/master-adsallocation/saveadsallocation
 * @method : post 
 * @description : for add/update saveadsallocation
 * 
 * @param {string}      pageLevel  
 * @param {string}      parentId
 * @param {string}      userId 
 * 
 * @returns {Object}
 */
 router.post("/saveadsallocation", async (req, res, next) => {
    try {
       // const errors = validationResult(req);
        //console.log(errors);
        //if (!errors.isEmpty()) {
        //    return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
        //} else {        
            next();
        //}
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
},apiAdsAllocationCtrl.addAdsAllocation);





module.exports = router;