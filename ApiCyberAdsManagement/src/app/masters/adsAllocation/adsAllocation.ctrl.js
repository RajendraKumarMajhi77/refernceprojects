const {
    await
} = require('asyncawait');
var mongoose = require('mongoose');
const utility = require("../../../utils/utility");
const apiResponse = require("../../../utils/apiResponse");
const serv_adsAllocation = require("./adsAllocation.service");

exports.getlevelwisepages = async function (req, res, next) {
    try {
       // console.log(req)
       var Data = await serv_adsAllocation.getlevelwisepages(req.params.level);
        // console.log('Edit Data',Data);
        if (Data == false) {
           return apiResponse.validationErrorWithData(res, "No Data Found", []);
        } else {
           return apiResponse.successResponseWithData(res, "Data Found", Data);
        }
    } catch (e) {
        console.log("/--- error occured in editAdsLocation | get -----/");
        next(e)
    }
};

exports.getAssignedAdsType = async function(req,res,next){
try{
    console.log(req.query);
    var checkPermissions = false;
    var Data = await serv_adsAllocation.findAdsType();
    for(var i=0;i<Data.length; i++){
        checkPermissions = await serv_adsAllocation.checkAdsTypeAssignedOrNot(req.query.level,req.query.adsLocation,Data[i]._id);
        //console.log('permission',checkPermissions);
        Data[i].IsChecked = checkPermissions;
    }
     // console.log('adsType Data>>> \n',Data);
    if (Data == false) {
        return apiResponse.validationErrorWithData(res, "No Data Found", []);
     } else {
        return apiResponse.successResponseWithData(res, "Data Found", Data);
     }
}catch(e){
    console.log("/--- error occured in getAssignedAdsType | get -----/");
    next(e);
}
};

exports.addAdsAllocation = async function (req, res, next) {
    try{
        var checkIsExists = await serv_adsAllocation.findAdsAllocation(req)
        var adsAllocationId='';
        if(checkIsExists == false){
            var addRequestData =  await serv_adsAllocation.addAllocation(req);
            if (addRequestData == false) {
                let msg = 'Something Went Wrong!.'
                return apiResponse.validationErrorWithData(res, msg, []);
            } else {
                let msg = 'Ads Allocation Added Successfully';
                // console.log('Save>>>>\n',addRequestData);
                return apiResponse.successResponseWithData(res, msg, addRequestData);
            }
        }else{
            adsAllocationId = checkIsExists[0]._id;
            var removeData = await serv_adsAllocation.removeAllocation(adsAllocationId);
            if(removeData == false){
                let msg = 'Something Went Wrong!..'
                return apiResponse.validationErrorWithData(res, msg, []);
            }else{
                var addRequestData =  await serv_adsAllocation.addAllocation(req);
                if (addRequestData == false) {
                    let msg = 'Something Went Wrong!.'
                    return apiResponse.validationErrorWithData(res, msg, []);
                } else {
                    let msg = 'Ads Allocation Updated Successfully';
                    // console.log('Save>>>>\n',addRequestData);
                    return apiResponse.successResponseWithData(res, msg, addRequestData);
                }
            }
        }

    }catch(e){
        console.log("/--- error occured in addAdsAllocation | get -----/");
        next(e)
    }

};