const {
    await
} = require('asyncawait');
var mongoose = require('mongoose');
const adsLocationCollection = require("../../../models/adsLocation");
const adsTypeCollection = require("../../../models/adsType");
const adsAllocationCollection = require("../../../models/adsAllocation");

exports.getlevelwisepages = async function (level) {
    try {
        if (level != 'Level-0') {
            level = 'Level-1'
        }
        return new Promise((resolve, reject) => {
            const aggregate = adsLocationCollection.aggregate([{
                '$match': {
                    'isActive': true,
                    'isDeleted': false,
                    'pageLevel': level
                }
            }, {
                '$project': {
                    '_id': 1,
                    'pageLevel': 1,
                    'pageName': 1,
                    'pageUrl': 1,
                    'parentId': 1,
                    'updatedAt': 1
                }
            }]);
            aggregate.exec((err, success) => {
                if (err) {
                    throw (Error(err));
                } else if (!success) {
                    resolve(false);
                } else {
                    resolve(success)
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};

exports.findAdsType = async function () {
    try {
        return new Promise((resolve, reject) => {
            const aggregate = adsTypeCollection.aggregate([{
                '$lookup': {
                    'from': 'adsSection',
                    'localField': 'adsSectionId',
                    'foreignField': '_id',
                    'as': 'adsSectionCol'
                }
            }, {
                '$unwind': {
                    'path': '$adsSectionCol',
                    'preserveNullAndEmptyArrays': true
                }
            }, {
                '$lookup': {
                    'from': 'adsPositions',
                    'localField': 'adsPositionsId',
                    'foreignField': '_id',
                    'as': 'adsPositionsCol'
                }
            }, {
                '$unwind': {
                    'path': '$adsPositionsCol',
                    'preserveNullAndEmptyArrays': true
                }
            }, {
                '$project': {
                    '_id': 1,
                    'adsSectionId': 1,
                    'adsSection': '$adsSectionCol.adsSection',
                    'adsPositionsId': 1,
                    'adsPositions': '$adsPositionsCol.adsPositions',
                    'adsType': 1,
                    'adsWidth': 1,
                    'adsHeight': 1,
                    'isActive': 1,
                    'isDeleted': 1,
                    'updatedAt': 1
                }
            }, {
                '$match': {
                    'isActive': true,
                    'isDeleted': false
                }
            }, {
                '$sort': {
                    '_id': 1
                }
            }]);
            aggregate.exec((err, success) => {
                if (err) {
                    throw (Error(err));
                } else if (!success) {
                    resolve(false);
                } else {
                    resolve(success)
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};
exports.checkAdsTypeAssignedOrNot = async function (level, adsLocation,_id) {
    return new Promise((resolve, reject) => {
        adsAllocationCollection.find({
            pageLevel: level, parentId: mongoose.Types.ObjectId(adsLocation),
            "allocationDetails": {
                $elemMatch: {
                    adsTypeId: {
                        $eq: mongoose.Types.ObjectId(_id)
                    }
                }
            }
        }, {}, (err, success) => {
            if (success.length == 0) {
                resolve(false);
            } else if (success.length > 0) {
                resolve(true)
            }
        });
    });
};
exports.findAdsAllocation = async function (req) {
    try {
        var condition = {
            "$and": [{
                "pageLevel": req.body.pageLevel
            }, {
                "parentId": mongoose.Types.ObjectId(req.body.parentId)
            }, {
                "isActive": true
            }, {
                "isDeleted": false
            }, ]
        }
        return new Promise((resolve, reject) => {
            adsAllocationCollection.find(condition, {}, (err, success) => {
                // console.log(err,success);
                if (err || success.length == 0) {
                    resolve(false);
                } else {
                    resolve(success);
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};

exports.removeAllocation = async function (adsAllocationId) {
    try {
        return new Promise((resolve, reject) => {
            adsAllocationCollection.findByIdAndDelete(adsAllocationId, (err, success) => {
                if (err) {
                    reject(Error(err));
                } else if (!success) {
                    resolve(false)
                } else {
                    resolve(true)
                }
            });
        });
    } catch (e) {
        console.log(e);
        next(e);
    }
};


exports.addAllocation = async function (req) {
    try {
        console.log(req.body)
        let _AllocationDetails = [];
        if (req.body.AllocationDetails.length > 0) {
            for (var i = 0; i < req.body.AllocationDetails.length; i++) {
                _AllocationDetails.push({adsTypeId:req.body.AllocationDetails[i]._id});
            }
        } else {
            _AllocationDetails = [];
        }
        let reqObj = new adsAllocationCollection({
            pageLevel: req.body.pageLevel,
            parentId: mongoose.Types.ObjectId(req.body.parentId),
            allocationDetails: _AllocationDetails
        });

         console.log('>>>>\n', reqObj);
        return new Promise((resolve, reject) => {
            reqObj.save((err, success) => {
                if (err) {
                    reject(err);
                }
                if (!success) {
                    resolve(false);
                } else {
                    resolve(success);
                }
            });
        });
    } catch (e) {
        console.log('service error\n', e);
        throw e;
    }
};