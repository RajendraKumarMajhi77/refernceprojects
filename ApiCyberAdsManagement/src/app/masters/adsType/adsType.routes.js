let express = require("express");
var router = express.Router();
const {
    sanitizeBody,
    validationResult
} = require("express-validator");
let jwt = require("../../../middleWares/jwt")
let Validate = require("../../../middleWares/validation");
const apiResponse = require("../../../utils/apiResponse");
const apiAdsTypeCtrl = require("./adsType.ctrl")
const {
    await
} = require("asyncawait");

/**
 * @Api : v1/master-adstype/getadssection
 * @method : get
 * @description : for get ads section
 *
 * @returns {Object}
 */
 router.get("/getadssection", async (req, res, next) => {
    try {
        next();
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
}, apiAdsTypeCtrl.getAdsSectionList);


/**
 * @Api : v1/master-adstype/getadspositions
 * @method : get
 * @description : for get ads positions
 *
 * @returns {Object}
 */
 router.get("/getadspositions", async (req, res, next) => {
    try {
        next();
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
}, apiAdsTypeCtrl.getAdsPositionsList);

/**
 * @Api : v1/master-adstype/saveadstype
 * @method : post 
 * @description : for add/update adsType
 *
 * @param {string}      _id  [adsType collection pk]
 * @param {string}      adsSectionId
 * @param {string}      adsPositionId 
 * @param {string}      adsType  
 * @param {number}      adsWidth
 * @param {number}      adsHeight
 * @param {string}      userId 
 * 
 * @returns {Object}
 */
 router.post("/saveadstype",Validate._adstypemaster, sanitizeBody("*").escape(), async (req, res, next) => {
    try {
        const errors = validationResult(req);
        console.log(errors);
        if (!errors.isEmpty()) {
            return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
        } else {        
            next();
        }
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
},apiAdsTypeCtrl.addAdsType);

/**
 * @Api : v1/master-adstype/getadstype
 * @method : get
 * @description : for get ads positions
 *
 * @returns {Object}
 */
 router.get("/getadstype", async (req, res, next) => {
    try {
        next();
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
}, apiAdsTypeCtrl.getAdsTypeList);

/**
 * @Api : v1/master-adstype/getadstype
 * @method : get
 * @description : for get ads positions
 *
 * @returns {Object}
 */
 router.get("/getadstype/:_id", async (req, res, next) => {
    try {
        next();
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
}, apiAdsTypeCtrl.editAdsType);


/**
 * @Api : v1/master-adstype/deleteadstype
 * @method : put 
 * @description : for delete ads type
 *
 * @param {string}   userId 
 * @param {string}   _id  
 * 
 * @returns {Object}  
 */
 router.put("/deleteadstype/:userId/:_id",sanitizeBody("*").escape(),async (req, res, next) => {
    try {
        next();
    } catch (err) {
        return apiResponse.ErrorResponse(res, err);
    }
},apiAdsTypeCtrl.deleteAdsType); 


module.exports = router;