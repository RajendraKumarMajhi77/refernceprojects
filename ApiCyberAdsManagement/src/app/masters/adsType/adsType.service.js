const {
    await
} = require('asyncawait');
var mongoose = require('mongoose');
const adsSectionCollection = require("../../../models/adsSection");
const adsPositionsCollection = require("../../../models/adsPositions");
const adsTypeCollection = require("../../../models/adsType");


exports.getAdsSectionList = async function () {
    try {
        var condition = {
            isActive: true,
            isDeleted: false
        };
        return new Promise((resolve, reject) => {
            adsSectionCollection.find(condition, {}, (err, success) => {
                if (!success) {
                    resolve(false);
                } else {
                    resolve(success);
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};

exports.getAdsPositionsList = async function () {
    try {
        var condition = {
            isActive: true,
            isDeleted: false
        };
        return new Promise((resolve, reject) => {
            adsPositionsCollection.find(condition, {}, (err, success) => {
                if (!success) {
                    resolve(false);
                } else {
                    resolve(success);
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};

exports.findAdsType = async function (req) {
    try {
        var condition = {
            "$and": [{
                "isActive": true
            }, {
                "isDeleted": false
            }, {
                "adsType": req.body.adsType
            }, {
                "adsWidth": req.body.adsWidth
            }, {
                "adsHeight": req.body.adsHeight
            }, {
                "adsSectionId": req.body.adsSectionId
            }, {
                "adsPositionsId": req.body.adsPositionsId
            }]
        }

        // console.log('condition',condition);
        if (req.body._id != '') {
            condition.$and.push({
                "_id": {
                    "$ne": mongoose.Types.ObjectId(req.body._id)
                }
            })
        }
        //  console.log('<><><><><><><><><><>',condition )
        return new Promise((resolve, reject) => {
            adsTypeCollection.find(condition, {}, (err, success) => {
                // console.log(err,success);
                if (err || success.length == 0) {
                    resolve(false);
                } else {
                    resolve(true);
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};

exports.addAdsType = async function (req) {
    try {
        let reqObj = new adsTypeCollection({
            adsSectionId: req.body.adsSectionId,
            adsPositionsId: req.body.adsPositionsId,
            adsType: req.body.adsType,
            adsWidth: req.body.adsWidth,
            adsHeight: req.body.adsHeight,
            createdBy: req.body.userId
        });
        return new Promise((resolve, reject) => {
            reqObj.save((err, success) => {
                if (err) {
                    reject(err);
                }
                if (!success) {
                    resolve(false);
                } else {
                    resolve(success);
                }
            });
        });
    } catch (e) {
        console.log('service error\n', e);
        throw e;
    }
};

exports.updateAdsType = async function (req) {
    try {
        return new Promise((resolve, reject) => {
            adsTypeCollection.findByIdAndUpdate({
                _id: mongoose.Types.ObjectId(req.body._id)
            }, {
                adsSectionId: req.body.adsSectionId,
                adsPositionsId: req.body.adsPositionsId,
                adsType: req.body.adsType,
                adsWidth: req.body.adsWidth,
                adsHeight: req.body.adsHeight,
                updatedBy: req.body.userId
            }, (err, success) => {
                if (err || !success) {
                    resolve(false);
                } else {
                    resolve(success);
                }
            });
        });
    } catch (e) {
        console.log('service error\n', e);
        throw e;
    }
};
exports.getAdsTypeList = async function () {
    try {
        return new Promise((resolve, reject) => {
            const aggregate = adsTypeCollection.aggregate([{
                '$lookup': {
                    'from': 'adsSection',
                    'localField': 'adsSectionId',
                    'foreignField': '_id',
                    'as': 'adsSectionCol'
                }
            }, {
                '$unwind': {
                    'path': '$adsSectionCol',
                    'preserveNullAndEmptyArrays': true
                }
            }, {
                '$lookup': {
                    'from': 'adsPositions',
                    'localField': 'adsPositionsId',
                    'foreignField': '_id',
                    'as': 'adsPositionsCol'
                }
            }, {
                '$unwind': {
                    'path': '$adsPositionsCol',
                    'preserveNullAndEmptyArrays': true
                }
            }, {
                '$project': {
                    '_id': 1,
                    'adsSectionId': 1,
                    'adsSection': '$adsSectionCol.adsSection',
                    'adsPositionsId': 1,
                    'adsPositions': '$adsPositionsCol.adsPositions',
                    'adsType': 1,
                    'adsWidth': 1,
                    'adsHeight': 1,
                    'isActive': 1,
                    'isDeleted': 1, 
                    'updatedAt': 1
                }
            }, {
                '$match': {
                    'isActive': true,
                    'isDeleted': false
                }
            }, {
                '$sort': {
                    '_id': 1
                }
            }]);
            aggregate.exec((err, success) => {
                if (err) {
                    throw (Error(err));
                } else if (!success) {
                    resolve(false);
                } else {
                    resolve(success)
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};

exports.editAdsType = async function (_id) {
    try {
        return new Promise((resolve, reject) => {
            const aggregate = adsTypeCollection.aggregate([{
                '$lookup': {
                    'from': 'adsSection',
                    'localField': 'adsSectionId',
                    'foreignField': '_id',
                    'as': 'adsSectionCol'
                }
            }, {
                '$unwind': {
                    'path': '$adsSectionCol',
                    'preserveNullAndEmptyArrays': true
                }
            }, {
                '$lookup': {
                    'from': 'adsPositions',
                    'localField': 'adsPositionsId',
                    'foreignField': '_id',
                    'as': 'adsPositionsCol'
                }
            }, {
                '$unwind': {
                    'path': '$adsPositionsCol',
                    'preserveNullAndEmptyArrays': true
                }
            }, {
                '$project': {
                    '_id': 1,
                    'adsSectionId': 1,
                    'adsSection': '$adsSectionCol.adsSection',
                    'adsPositionsId': 1,
                    'adsPositions': '$adsPositionsCol.adsPositions',
                    'adsType': 1,
                    'adsWidth': 1,
                    'adsHeight': 1,
                    'isActive': 1,
                    'isDeleted': 1, 
                    'updatedAt': 1
                }
            }, {
                '$match': {
                    'isActive': true,
                    'isDeleted': false,
                    '_id': mongoose.Types.ObjectId(_id)
                }
            }, {
                '$sort': {
                    '_id': 1
                }
            }]);
            aggregate.exec((err, success) => {
                if (err) {
                    throw (Error(err));
                } else if (!success) {
                    resolve(false);
                } else {
                    resolve(success)
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};

exports.deleteAdsType = async function (userId, _id) {
    try {
        data = {
            isActive: false,
            isDeleted: true,
            deletedBy: userId,
            deletedOn: new Date()
        }
        return new Promise((resolve, reject) => {
            adsTypeCollection.findByIdAndUpdate(_id, data, (err, success) => {
                if (!success) {
                    resolve(false);
                } else {
                    resolve(data);
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }

};