const {
    await
} = require('asyncawait');
const utility = require("../../../utils/utility");
const apiResponse = require("../../../utils/apiResponse");
const serv_adsType = require("./adsType.service");

exports.getAdsSectionList = async function (req, res, next) {
    try {
        var responseData = await serv_adsType.getAdsSectionList(req);
        if (responseData == false) {
            return apiResponse.successResponseWithData(res, "No Data Found", []);
        } else {
            return apiResponse.successResponseWithData(res, "Data Found", responseData);
        }
    } catch (e) {
        console.log("/--- error occured in getAdsSectionList | get -----/");
        next(e)
    }
};

exports.getAdsPositionsList = async function (req, res, next) {
    try {
        var responseData = await serv_adsType.getAdsPositionsList(req);
        if (responseData == false) {
            return apiResponse.successResponseWithData(res, "No Data Found", []);
        } else {
            return apiResponse.successResponseWithData(res, "Data Found", responseData);
        }
    } catch (e) {
        console.log("/--- error occured in getAdsPositionsList | get -----/");
        next(e)
    }
};
exports.addAdsType = async function (req, res, next) {
    try {
        console.log('RequestBody>>>>>>>>>>>>>>\n', req.body);
        var isExists = await serv_adsType.findAdsType(req);
        //console.log(isExists)
        if (isExists == true) {
            let msg = req.body.adsType + ' already exists with the following configuration.'
            return apiResponse.validationErrorWithData(res, msg, []);
        } else {
            // Opeartaion Save
            if (req.body._id == "") {
                var addRequestData = await serv_adsType.addAdsType(req);
                if (addRequestData == false) {
                    let msg = 'Something Went Wrong!.'
                    return apiResponse.validationErrorWithData(res, msg, []);
                } else {
                    let msg = 'Ads Type Added Successfully';
                    // console.log('Save>>>>\n',addRequestData);
                    return apiResponse.successResponseWithData(res, msg, addRequestData);
                }
            } // Operation Updated 
            else {
                var updateRequestData = await serv_adsType.updateAdsType(req);
                if (updateRequestData == false) {
                    let msg = 'Something Went Wrong!.'
                    return apiResponse.validationErrorWithData(res, msg, []);
                } else {
                    let msg = 'Ads Type Updated Successfully';
                    //console.log('Update>>>>\n',updateRequestData);
                    return apiResponse.successResponseWithData(res, msg, updateRequestData);
                }
            }
        }
    } catch (e) {
        console.log("/--- error occured in addAdsType.| post  -----/");
        next(e)
    }
};

exports.getAdsTypeList = async function (req, res, next) {
    try {
        var Data = await serv_adsType.getAdsTypeList(req);
        if (Data == false) {
            return apiResponse.successResponseWithData(res, "No Data Found", []);
        } else {
            return apiResponse.successResponseWithData(res, "Data Found", Data);
        }
    } catch (e) {
        console.log("/--- error occured in getAdsTypeList | get -----/");
        next(e)
    }
};

exports.editAdsType = async function (req, res, next) {
    try {
        //console.log(req)
        var Data = await serv_adsType.editAdsType(req.params._id);
        // console.log('Edit Data',Data);
        if (Data == false) {
            return apiResponse.successResponseWithData(res, "No Data Found", []);
        } else {
            return apiResponse.successResponseWithData(res, "Data Found", Data);
        }
    } catch (e) {
        console.log("/--- error occured in getAdsTypeList | get -----/");
        next(e)
    }
};

exports.deleteAdsType = async function (req, res, next) {
    try {
        const {
            _id,
            userId
        } = req.params;
        var Data = await serv_adsType.deleteAdsType(userId, _id);
        if (Data == false) {
            return apiResponse.validationErrorWithData(res, "Something Went Wrong !", []);
        } else {
            return apiResponse.successResponseWithData(res, "Deleted Successfully!", Data);
        }
    } catch (e) {
        console.log("/--- error occured in deleteAdsType | get -----/");
        next(e)
    }
};