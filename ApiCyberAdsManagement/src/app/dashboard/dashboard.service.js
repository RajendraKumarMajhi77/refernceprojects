const {
    await
} = require('asyncawait');
var mongoose = require('mongoose');
const userModel = require("../../models/user");
const userPasswordModel = require("../../models/userPassword");
const adsCenterCollection = require("../../models/adsCenter");

exports.getUserDetails = async function (_id) {
    try {
        return new Promise((resolve, reject) => {
            userModel.findById(_id, {}, (err, success) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(success);
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};
exports.getUserPasswordDetails = async function (_id) {
    try {
        return new Promise((resolve, reject) => {
            userPasswordModel.findOne({
                userId: mongoose.Types.ObjectId(_id)
            }, {}, (err, success) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(success);
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};



// Dashboard Analytics Details 

exports.findTopTrending = async function () {
    try {
        return new Promise((resolve, reject) => {
            const aggregate = adsCenterCollection.aggregate([{
                '$match': {
                    'isActive': true,
                    'isDeleted': false,
                    'status': {
                        '$eq': 'PUBLISHED'
                    }
                }
            }, {
                '$unwind': {
                    'path': '$adsDetails',
                    'preserveNullAndEmptyArrays': true
                }
            }, {
                '$project': {
                    '_id': 1,
                    'status': 1,
                    'adsDetails_id': '$adsDetails._id',
                    'adsCode': '$adsDetails.adsCode',
                    'adsTittle': '$adsDetails.adsTittle',
                    'adsRefUrl': '$adsDetails.adsRefUrl',
                    'fileName': '$adsDetails.fileName',
                    'fileUrl': '$adsDetails.fileUrl',
                    'expiredOn': 1,
                    'publishedOn': 1,
                    'createdAt': 1,
                    'visitors': {
                        '$size': '$adsDetails.clickDetails'
                    }
                }
            }, {
                '$match': {
                    'visitors': {
                        '$gt': 0
                    }
                }
            }, {
                '$sort': {
                    'visitors': -1
                }
            }, {
                '$limit': 5
            }]);
            aggregate.exec((err, success) => {
                if (err) {
                    throw (Error(err));
                } else if (success.length == 0) {
                    resolve(false);
                } else {
                    resolve(success);
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};

exports.findAdsEngagement = async function () {
    try {
        return new Promise((resolve, reject) => {
            const aggregate = adsCenterCollection.aggregate([{
                '$match': {
                    'isActive': true,
                    'isDeleted': false,
                    'status': {
                        '$eq': 'PUBLISHED'
                    }
                }
            }, {
                '$project': {
                    '_id': 1,
                    'adsLocationId': 1
                }
            }, {
                '$lookup': {
                    'from': 'adsLocation',
                    'localField': 'adsLocationId',
                    'foreignField': '_id',
                    'as': 'adsLocationCol'
                }
            }, {
                '$unwind': {
                    'path': '$adsLocationCol',
                    'preserveNullAndEmptyArrays': true
                }
            }, {
                '$project': {
                    '_id': 1,
                    'adsLocationId': 1,
                    'pageLevel': '$adsLocationCol.pageLevel'
                }
            }, {
                '$group': {
                    '_id': '$pageLevel',
                    'count': {
                        '$sum': 1
                    }
                }
            }, {
                '$project': {
                    '_id': 0,
                    'type': {
                        '$concat': [{
                            '$cond': [{
                                '$eq': [
                                    '$_id', 'Level-0'
                                ]
                            }, 'Home Page', '']
                        }, {
                            '$cond': [{
                                '$eq': [
                                    '$_id', 'Level-1'
                                ]
                            }, 'Listing Page', '']
                        }, {
                            '$cond': [{
                                '$eq': [
                                    '$_id', 'Level-2'
                                ]
                            }, 'Details Page', '']
                        }]
                    },
                    'pageLevel': '$_id',
                    'count': 1
                }
            }, {
                '$sort': {
                    'pageLevel': 1
                }
            }]);
            aggregate.exec((err, success) => {
                if (err) {
                    throw (Error(err));
                } else if (success.length == 0) {
                    resolve(false);
                } else {
                    resolve(success);
                }
            });
        });
    } catch (e) {
        console.log(e);
        throw e;
    }
};