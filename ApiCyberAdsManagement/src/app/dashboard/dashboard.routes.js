let express = require("express");
var router = express.Router();
const {
    sanitizeBody,
    check,
    validationResult
} = require("express-validator");
const {
    await
} = require("asyncawait");
let jwt = require("../../middleWares/jwt")
let Validate = require("../../middleWares/validation");
const apiResponse = require("../../utils/apiResponse");
const apiDashboardCtrl = require("./dashboard.ctrl");


/**
 * 
 * * Author : rAJENDRA -- Dt: 25th Feb 2021
 * * Api: v1/dashboard/changepasswordstatus
 * @method : get
 * @description : for checking the status of reset password
 * 
 * @param {string}  _id [userId]
 *  
 * @returns {Object}
 */
router.put("/changepasswordstatus/:_id",Validate._idValidation, async (req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            let msg ='Api Parameter Validation Error.';
            return apiResponse.validationErrorWithData(res, msg , errors.array());
        } else {
            next();
        }
    } catch (err) {
        //throw error in json response with status 500.
        return apiResponse.ErrorResponse(res, err);
    }
},apiDashboardCtrl.changePasswordStatus);

/**
 * 
 * * Author : rAJENDRA -- Dt: 25th Feb 2021
 * * Api: v1/dashboard/get-dashboardanalytics
 * @method : get
 * @description : for getting dashboard analytics
 * @param {string}  _id [userId]
 *  
 * @returns {Object}
 * */
 router.get("/get-dashboardanalytics", async (req, res, next) => {
    try {
        next();
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
}, apiDashboardCtrl.getDashboardAnalytics);



module.exports = router;