const {
    await
} = require('asyncawait');
const config = require("../app.config")
const utility = require("../../utils/utility");
const apiResponse = require("../../utils/apiResponse");
const serv_dashBoard = require("./dashboard.service");

exports.changePasswordStatus = async function (req, res, next) {
    try {
        let _id = req.params._id //[userId]
        let msg = 'Change Password Required.';
        var userDetails = await serv_dashBoard.getUserDetails(_id);
        var lastUpdatePassword = await serv_dashBoard.getUserPasswordDetails(_id);
        if (userDetails.resetPasswordCount == 0) {
            return apiResponse.successResponseWithData(res, msg, userDetails);
        } else if (userDetails.resetPasswordCount > 0) {
            //console.log(lastUpdatePassword);
            let currentDate = new Date();
            let updatedAt = new Date(lastUpdatePassword.updatedAt);
            let daysDifference = Math.floor((currentDate - updatedAt) / (1000 * 60 * 60 * 24));
            if (daysDifference > 100) {
                return apiResponse.successResponseWithData(res, msg, []);
            } else {
                return apiResponse.successResponseWithData(res, 'NoIssues', []);
            }
        }
    } catch (e) {
        next(e);
    }
};

exports.getDashboardAnalytics = async function (req, res, next) {
    try {
        let finalObjectOfList = {};
        // Top Trending Analytics
        var findTopTrending = await serv_dashBoard.findTopTrending();
        if (Object.keys(findTopTrending).length > 0) {
            finalObjectOfList.TopTrending = findTopTrending;
        } else {
            finalObjectOfList.TopTrending = [];
        }
        // Ads ENGAGEMENT Analytics
        var findAdsEngagement = await serv_dashBoard.findAdsEngagement();
        if (Object.keys(findAdsEngagement).length > 0) {
            finalObjectOfList.AdsEngagement = findAdsEngagement;
        } else {
            finalObjectOfList.AdsEngagement = [];
        }
        //console.log('Data:', JSON.stringify(finalObjectOfList));
        if (finalObjectOfList.length == 0) {
            return apiResponse.validationErrorWithData(res, "No Data Found", []);
        } else {
            return apiResponse.successResponseWithData(res, "Data Found", finalObjectOfList);
        }
    } catch (e) {
        console.log("/--- error occured in getDashboarad Analylits | get -----/");
        next(e)
    }
};