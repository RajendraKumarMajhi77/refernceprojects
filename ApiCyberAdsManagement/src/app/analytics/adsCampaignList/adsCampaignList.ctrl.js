const {
    await
} = require('asyncawait');
var mongoose = require('mongoose');
const utility = require("../../../utils/utility");
const apiResponse = require("../../../utils/apiResponse");
const serv_adsCampaignList = require("./adsCampaignList.service");

exports.getCampList = async function (req, res, next) {
    try {
        var Data = await serv_adsCampaignList.getCampaignList();
        console.log(Data)
        if (Data == false) {
            return apiResponse.validationErrorWithData(res, "No Data Found", []);
        } else {
            return apiResponse.successResponseWithData(res, "Data Found", Data);
        }
    } catch (e) {
        console.log("/--- error occured in getCampaignList | get -----/");
        next(e)
    }
};

exports.getCampListClicksDetails = async function (req, res, next) {
    try {
        let finalObjectOfList = {};
        console.log('Request Params :',req.params);
        // aDS dETAILS ONLY 
        var getAdsDetails = await serv_adsCampaignList.getAdsDetails(req.params);
        var expireDate , publishDate ;
        //console.log(getAdsDetails)
        if (getAdsDetails != false) {
            if (Object.keys(getAdsDetails).length > 0) {
                finalObjectOfList.Ads = getAdsDetails;
            }
            expireDate = getAdsDetails[0].expiredOn;
            publishDate = getAdsDetails[0].publishedOn;
        }
        // dATE WISE CLICKS dATA 
        var getDateWise = await serv_adsCampaignList.getDateWiseClickDetails(req.params._id,req.params._adsDetailsId);
        //console.log(getDateWise)
        if (getDateWise != false) {
            var clickCount = [];
            for(var i =0 ; i<getDateWise.length; i++){
                clickCount.push({ x: new Date(getDateWise[i].clickTime_at).getTime(),y:getDateWise[i].count});
            }
            if (Object.keys(clickCount).length > 0) {
                finalObjectOfList.clickCounts = clickCount;
                finalObjectOfList.clickCountActivity = getDateWise;
            }
        }
        // dATE WISE CLICKS dATA 
        console.log('Final dATA',JSON.stringify(finalObjectOfList));
        if (finalObjectOfList.length == 0) {
            return apiResponse.validationErrorWithData(res, "No Data Found", []);
        } else {
            return apiResponse.successResponseWithData(res, "Data Found", finalObjectOfList);
        }

    } catch (e) {
        console.log("/--- error occured in getCampListClicksDetails | get -----/");
        next(e)
    }
}


