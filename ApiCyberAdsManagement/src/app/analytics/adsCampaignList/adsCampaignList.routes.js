let express = require("express");
var router = express.Router();
const {
    await
} = require("asyncawait");
const {
    sanitizeBody,
    validationResult
} = require("express-validator");
let jwt = require("../../../middleWares/jwt")
let Validate = require("../../../middleWares/validation");
const apiResponse = require("../../../utils/apiResponse");
const apiadsCampaignListCtrl = require("./adsCampaignList.ctrl");

/**
 * @Api : v1/analytics-adscampaign/getcampaignlist
 * @method : get
 * @description : for get campaign list
 *
 * @returns {Object}
 */
 router.get("/getcampaignlist", async (req, res, next) => {
    try {
        next();
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
}, apiadsCampaignListCtrl.getCampList);
/**
 * @Api : v1/analytics-adscampaign/get-campaignlist
 * @method : get
 * @description : for get campaign list
 *
 * @returns {Object}
 */
 router.get("/get-campaignlist/:_id/:_adsDetailsId", async (req, res, next) => {
    try {
        next();
    } catch (err) {
        console.log(err);
        return apiResponse.ErrorResponse(res, err);
    }
}, apiadsCampaignListCtrl.getCampListClicksDetails);

module.exports = router;
