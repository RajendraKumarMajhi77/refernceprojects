const {
  await
} = require('asyncawait');
var mongoose = require('mongoose');
const adsCenterCollection = require("../../../models/adsCenter");
const utility = require("../../../utils/utility");

exports.getCampaignList = async function () {
  try {
    return new Promise((resolve, reject) => {
      const aggregate = adsCenterCollection.aggregate([
        {
          '$unwind': {
            'path': '$adsDetails', 
            'preserveNullAndEmptyArrays': true
          }
        }, {
          '$lookup': {
            'from': 'user', 
            'localField': 'userId', 
            'foreignField': '_id', 
            'as': 'userCol'
          }
        }, {
          '$unwind': {
            'path': '$userCol', 
            'preserveNullAndEmptyArrays': true
          }
        }, {
          '$lookup': {
            'from': 'role', 
            'localField': 'userCol.roleId', 
            'foreignField': '_id', 
            'as': 'roleCol'
          }
        }, {
          '$unwind': {
            'path': '$roleCol', 
            'preserveNullAndEmptyArrays': true
          }
        }, {
          '$lookup': {
            'from': 'adsLocation', 
            'localField': 'adsLocationId', 
            'foreignField': '_id', 
            'as': 'adsLocationCol'
          }
        }, {
          '$unwind': {
            'path': '$adsLocationCol', 
            'preserveNullAndEmptyArrays': true
          }
        }, {
          '$project': {
            '_id': 1, 
            'adsLocationId': 1, 
            'page': '$adsLocationCol.pageName', 
            'pageUrl': '$adsLocationCol.pageUrl', 
            'status': 1, 
            'adsDetails': 1, 
            'addedBy_firstName': '$userCol.firstName', 
            'addedBy_lastName': '$userCol.lastName', 
            'role': '$roleCol.roleName', 
            'createdAt': 1, 
            'expiredOn': {
              '$ifNull': [
                '$expiredOn', 'N/A'
              ]
            }, 
            'publishedOn': {
              '$ifNull': [
                '$publishedOn', 'N/A'
              ]
            }
          }
        }, {
          '$match': {
            'status': {
              '$ne': 'PENDING'
            }
          }
        }
      ]);
      aggregate.exec((err, success) => {
        if (err) {
          throw (Error(err));
        } else if (success.length == 0) {
          resolve(false);
        } else {
          resolve(success);
        }
      });
    });
  } catch (e) {
    console.log(e);
    throw e;
  }
};


exports.getAdsDetails = async function (ids) {
  try {
    return new Promise((resolve, reject) => {
      const aggregate = adsCenterCollection.aggregate(
        [
          {
            '$match': {
              '_id': mongoose.Types.ObjectId(ids._id), 
              'status': {
                '$eq': 'PUBLISHED'
              }
            }
          }, {
            '$unwind': {
              'path': '$adsDetails', 
              'preserveNullAndEmptyArrays': true
            }
          }, {
            '$match': {
              'adsDetails._id': mongoose.Types.ObjectId(ids._adsDetailsId)
            }
          } ,{
            '$lookup': {
              'from': 'user', 
              'localField': 'userId', 
              'foreignField': '_id', 
              'as': 'userCol'
            }
          }, {
            '$unwind': {
              'path': '$userCol', 
              'preserveNullAndEmptyArrays': true
            }
          }, {
            '$lookup': {
              'from': 'role', 
              'localField': 'userCol.roleId', 
              'foreignField': '_id', 
              'as': 'roleCol'
            }
          }, {
            '$unwind': {
              'path': '$roleCol', 
              'preserveNullAndEmptyArrays': true
            }
          }, {
            '$lookup': {
              'from': 'adsLocation', 
              'localField': 'adsLocationId', 
              'foreignField': '_id', 
              'as': 'adsLocationCol'
            }
          }, {
            '$unwind': {
              'path': '$adsLocationCol', 
              'preserveNullAndEmptyArrays': true
            }
          }, {
            '$project': {
              '_id': 1, 
              'adsLocationId': 1, 
              'page': '$adsLocationCol.pageName', 
              'pageUrl': '$adsLocationCol.pageUrl', 
              'status': 1, 
              'adsDetails': {
                '_id': 1, 
                'adsTypeId': 1, 
                'adsCode': 1, 
                'adsTittle': 1, 
                'adsRefUrl': 1, 
                'fileName': 1, 
                'fileUrl': 1, 
                'isActive': 1, 
                'seq': 1, 
                'serchSeq': 1, 
                'serchSeqDetail': 1
              }, 
              'addedBy_firstName': '$userCol.firstName', 
              'addedBy_lastName': '$userCol.lastName', 
              'role': '$roleCol.roleName', 
              'createdAt': 1, 
              'expiredOn': {
                '$ifNull': [
                  '$expiredOn', 'N/A'
                ]
              }, 
              'publishedOn': {
                '$ifNull': [
                  '$publishedOn', 'N/A'
                ]
              }, 
              'totalClicks': {
                '$size': '$adsDetails.clickDetails'
              }
            }
          }
        ]);
      aggregate.exec((err, success) => {
        if (err) {
          throw (Error(err));
        } else if (success.length == 0) {
          resolve(false);
        } else {
          resolve(success);
        }
      });
    });
  } catch (e) {
    console.log(e);
    throw e;
  }
};
exports.getDateWiseClickDetails = async function (_id,_adsDetailsId) {
  try {
    return new Promise((resolve, reject) => {
      const aggregate = adsCenterCollection.aggregate(
        [
          {
            '$match': {
              '_id':mongoose.Types.ObjectId(_id),
              'status': {
                '$eq': 'PUBLISHED'
              }
            }
          }, {
            '$unwind': {
              'path': '$adsDetails', 
              'preserveNullAndEmptyArrays': true
            }
          }, {
            '$match': {
              'adsDetails._id':mongoose.Types.ObjectId(_adsDetailsId)
            }
          }, {
            '$project': {
              '_id': 0, 
              'clickDetails': '$adsDetails.clickDetails'
            }
          }, {
            '$unwind': {
              'path': '$clickDetails', 
              'preserveNullAndEmptyArrays': true
            }
          }, {
            '$project': {
              'Time': '$clickDetails.clickTime', 
              'clickTime': {
                '$dateToString': {
                  'format': '%Y-%m-%d', 
                  'date': '$clickDetails.clickTime'
                }
              }, 
              'day': {
                '$dayOfMonth': '$clickDetails.clickTime'
              }, 
              'month': {
                '$month': '$clickDetails.clickTime'
              }, 
              'year': {
                '$year': '$clickDetails.clickTime'
              }
            }
          }, {
            '$group': {
              '_id': {
                'day': '$day', 
                'month': '$month', 
                'year': '$year'
              }, 
              'Time': {
                '$first': '$Time'
              }, 
              'clickTime': {
                '$first': '$clickTime'
              }, 
              'count': {
                '$sum': 1
              }
            }
          }, {
            '$project': {
              '_id': 0, 
              'clickTime_at': '$clickTime', 
              'Time': 1, 
              'count': 1
            }
          }, {
            '$sort': {
              'clickTime_at': 1
            }
          }, {
            '$limit': 20
          }
        ]);
      aggregate.exec((err, success) => {
        if (err) {
          throw (Error(err));
        } else if (success.length == 0) {
          resolve(false);
        } else {
          resolve(success);
        }
      });
    });
  } catch (e) {
    console.log(e);
    throw e;
  }
};










