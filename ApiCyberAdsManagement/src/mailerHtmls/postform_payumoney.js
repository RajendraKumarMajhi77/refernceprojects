var postForm = {
    "PayU": (payload) => {
        var Form_packageSubscription =
            `<form id="PostForm"  name="PostForm"" action='${payload.action}' method="POST">` +
            `<input type="hidden" name="txnid" value='${payload.txnid}'>` +
            `<input type="hidden" name="key" value='${payload.key}'>` +
            `<input type="hidden" name="firstname" value='${payload.firstname}'>` +
            `<input type="hidden" name="email" value='${payload.email}'>` +
            `<input type="hidden" name="phone" value='${payload.phone}'>` +
            `<input type="hidden" name="productinfo" value='${payload.productinfo}'>` +
            `<input type="hidden" name="amount" value='${payload.amount}'>` +
            `<input type="hidden" name="service_provider" value='${payload.service_provider}'>` +
            `<input type="hidden" name="udf1" value='${payload.udf1}'>` +
            `<input type="hidden" name="udf2" value='${payload.udf2}'>` +
            `<input type="hidden" name="udf3" value='${payload.udf3}'>` +
            `<input type="hidden" name="udf4" value='${payload.udf4}'>` +
            `<input type="hidden" name="udf5" value='${payload.udf5}'>` +
            `<input type="hidden" name="udf6" value='${payload.udf6}'>` +
            `<input type="hidden" name="udf7" value='${payload.udf7}'>` +
            `<input type="hidden" name="udf8" value='${payload.udf8}'>` +
            `<input type="hidden" name="udf9" value='${payload.udf9}'>` +
            `<input type="hidden" name="udf10" value='${payload.udf10}'>` +
            `<input type="hidden" name="surl" value='${payload.surl}'>` +
            `<input type="hidden" name="furl" value='${payload.furl}'>` +
            `<input type="hidden" name="hash" value='${payload.hash}'>` +
            `</form>` +
            `<script language='javascript'>var vPostForm = document.PostForm;vPostForm.submit();</script>` ;
        return Form_packageSubscription;
    }
}
module.exports = postForm;

// `<!doctype html>` +
// `<html>` +
// `<head >` +
// `<script>` +
// ` var hash = '${payload.hash}';` +
// `function submitPayuForm() {` +
// ` if(hash == '') {` +
// ` return;` +
// ` }` +
// `var payuForm = document.forms.payuForm;` +
// ` payuForm.submit();` +
// ` }` +
// `</script>` +
// `</head>` +
// `<body onload="submitPayuForm()">` +
// `<h1>PayUMoney Payment Request Form </h1>` +