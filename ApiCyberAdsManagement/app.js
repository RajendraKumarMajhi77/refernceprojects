// =========================================================================
// =================   All Modules are imported here  ======================
// =========================================================================
const express = require("express");
const app = express();
const logger = require("morgan");
const express_FileUpload = require('express-fileupload');
const fs = require("fs");
const mv = require('mv');
const bodyParser = require("body-parser");
const cors = require("cors");
const responseTime = require('response-time');
const path = require("path");
const http = require("http");
const https = require('https');
const cookieParser = require("cookie-parser");
const dns = require('dns');
const request = require('request');
const crypto = require('crypto-js');
const uuid = require('uuid');
const jsSHA = require("jssha");
const _ = require('lodash');
const schedule = require('node-schedule');
const time_recuurence = new schedule.RecurrenceRule();

/** * Project Routes */
const config = require("./src/app/app.config");
//const indexRouter = require("./server/routes/index");
const apiRouter = require("./src/app/app.routes");
const apiResponse = require("./src/utils/apiResponse");

/** * Database connection. */
let connectWithDatabase = require("./src/dbConnection/db");

//Create a middleware that adds a X-Response-Time header to responses.
var resintercept = require("./src/utils//ResponseInterceptor");
app.use(resintercept.modifyResponseBody);

app.use(express_FileUpload());

app.use(bodyParser.json({
    limit: "50mb",
    extended: true
}));
app.use(bodyParser.urlencoded({
    limit: "50mb",
    extended: true,
    parameterLimit: 50000
}));
app.use(responseTime());
app.use(logger("dev"));

// Allow to access image 
app.use(express.static(__dirname + '/wwwroot'));
app.use(express.static(__dirname + '/script/test.js'));
//app.use(express.static(path.join(__dirname, "public")));

app.use(cors());

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    if ("OPTIONS" === req.method) {
        res.sendStatus(200);
    } else {
        next();
    }
});

//Route Prefixes
//app.use("/",);
app.use("/api/", apiRouter);
// throw 404 if URL not found
app.all("*", function (req, res) {
    return apiResponse.notFoundResponse(res, "404\nPlease Check Api Url");
});

/* http.createServer((req,res)=>{ 
    res.writeHead.bind(200,{"content-Type":"text/plain"});
    res.end();
}) */
// var server = app.listen(config.port, function () {
//     var host = server.address().address;
//     var port = server.address().port;
//     require('dns').lookup(require('os').hostname(), function (err, add, family) {
//         console.info('\nServer is listening at http://%s:%s\n', add, port);
//     })
// });


// address: "93.184.216.34" family: IPv4

app.listen(config.port, () => console.info('Server is listening at http://localhost:%s', config.port));
time_recuurence.minute = 12;
//time_recuurence.hour = ;

const job = schedule.scheduleJob(time_recuurence, function(){
  console.log('Only Call when the minute is 12');
});

// const job = schedule.scheduleJob('55 * * * *', function(){
//   console.log('The answer to life, the universe, and everything!' + new Date());
// });

// const year = new Date().getFullYear();//
// const date = new Date(2021,04,08, 19, 06, 0);
// console.log(JSON.stringify(date))
// //year month date hour minute sec
// const job = schedule.scheduleJob(date, function(){
//   console.log('The world is going to end today.');
// });