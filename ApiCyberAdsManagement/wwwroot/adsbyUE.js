// Pull the ID parameter from URL to place in API
var url_string = window.location.href;
var url = new URL(url_string);
var origin = url.origin;
var dev_origin = 'http://localhost:59568';
var live_origin = 'https://dev.ulyanaeducation.com';
var previousUrl, currentUrl = '';
this.previousUrl = this.currentUrl;
this.currentUrl = url_string;
var locationData = [];

console.log('Url-String', url_string);
console.log('Url', url);

function connect() {
    if (origin == dev_origin || live_origin) {
        if (currentUrl != previousUrl) {
            _connect = true;
        }
    } else {
        _connect = false;
    }
};
connect();
//console.log('_connectionStatus: ',_connect);
function fetchData(_connect) {
    fetch('http://localhost:1677/api/v1/UE/ads/' + encodeURIComponent(url_string))
        .then(response => {

            if (!response.ok) {
                throw new Error();
            }
            return response.json();
        })
        .then(result => {

            console.log('DATA: ', result);
            let pageLevel = result.data.pageLevel;
            let SecOneData = result.data.firstSec;
            let SecTwoData = result.data.secondSec;
            let Seq = [];

            if (pageLevel == 'Level-0') {
                if (SecOneData.length > 0) {
                    if (document.getElementById('adsUEHome_1').innerHTML != null) {
                        //document.getElementById('adsUEHome_1').innerHTML = "";
                        let html = ``;
                        for (let item of SecOneData) {
                            let c_id = item._id;
                            let l_id = item.adsLocationId;
                            let d_id = item.adsDetailsid;
                            let t_id = item.adsTypeId;
                            html += `
                        <div class="row"> 
                        <div class="col-md-12">
                        <a href="${item.adsRefUrl}" target="_blank">
                        <img src="${item.fileUrl}" alt="" class="img-responsive" onclick=adsClick('${c_id}','${l_id}','${d_id}','${t_id}')></a>
                        <span><i title="${item.adsTittle}" style="background-color:#FFFFFF;color:#000000;text-decoration:none">i</i></span>
                        <input type = "button" onclick = "myfunction()" value = "x"> 
                        </div></div>`;
                        }
                        document.getElementById('adsUEHome_1').innerHTML = html;
                    }
                }
                if (SecTwoData.length > 0) {
                    if (document.getElementById('adsUEHome_2').innerHTML != null) {
                        document.getElementById('adsUEHome_2').innerHTML = "";
                        //console.log(SecTwoData);
                        let html = `<ul class="ads_ul">`;
                        for (let item of SecTwoData) {
                            let c_id = item._id;
                            let l_id = item.adsLocationId;
                            let d_id = item.adsDetailsid;
                            let t_id = item.adsTypeId;
                            html += `
                            <li class="" onclick=adsClick('${c_id}','${l_id}','${d_id}','${t_id}')>
                            <a href="${item.adsRefUrl}" target="_blank"><img src="${item.fileUrl}" alt="" class="img-responsive"></a>
                            <span><i title="${item.adsTittle}" style="background-color:#FFFFFF;color:#000000;text-decoration:none">i</i></span>
                            <input type="button" value="x" onclick = "myfunction()" value = "x">
                           
                            </li>`;
                        }
                        html += `</ul>`;
                        document.getElementById('adsUEHome_2').innerHTML = html;
                    }
                }
            }
            if (pageLevel == 'Level-1') {
                if (SecOneData.length > 0) {
                    if (document.getElementById('adsUEListing_1').innerHTML != null) {
                        document.getElementById('adsUEListing_1').innerHTML = "";
                        let html = ``;
                        for (let item of SecOneData) {
                            let c_id = item._id;
                            let l_id = item.adsLocationId;
                            let d_id = item.adsDetailsid;
                            let t_id = item.adsTypeId;
                            html += `
                        <div class="row" onclick=adsClick('${c_id}','${l_id}','${d_id}','${t_id}')><div class="col-md-12">
                        <a href="${item.adsRefUrl}" target="_blank"><img src="${item.fileUrl}" alt="" class="img-responsive"></a>
                        <span><i title="${item.adsTittle}" style="background-color:#FFFFFF;color:#000000;text-decoration:none">i</i></span>
                        <input type = "button" onclick = "myfunction()" value = "x"> </div></div>`;
                        }
                        document.getElementById('adsUEListing_1').innerHTML = html;
                    }
                }
                setTimeout(() => {
                    if (SecTwoData.length > 0) {
                        Seq = result.data.secondSecSeq;
                        var node = document.getElementById('adsUEListing_2').children.length;
                        for (var i = 0; i < node; i++) {
                            var childNode = document.getElementById('adsUEListing_2').children[i];
                            childNode.classList.add("adsitems");
                        }
                        document.querySelectorAll('.ads_ele').forEach(function (a) {
                            a.remove()
                        })
                        var flag = 1;
                        var index = 0;

                        for (let i of Seq) {
                            var section = document.getElementsByClassName('adsitems')[i.index - flag],
                                ele = document.createElement("div");
                            ele.className = "ads_ele";
                            let c_id = SecTwoData[index]._id;
                            let l_id = SecTwoData[index].adsLocationId;
                            let d_id = SecTwoData[index].adsDetailsid;
                            let t_id = SecTwoData[index].adsTypeId;
                            let html = `<div class="row">
                             <div class="col-md-12">`;
                            html += `<img onclick=adsClick('${c_id}','${l_id}','${d_id}','${t_id}') src="${SecTwoData[index].fileUrl}" alt="" style="cursor: pointer;" class="img-responsive">
                            <span><i title="${SecTwoData[index].adsTittle}" style="background-color:#FFFFFF;color:#000000;text-decoration:none;cursor: pointer;">i</i></span><input type = "button" onclick = "myfunction()" value = "x"></input>`;
                            html += `</div></div>`;
                            ele.innerHTML = html;
                            section.parentNode.insertBefore(ele, section.nextSibling);
                            index = index + 1;
                            flag = flag + 1;
                        }
                    }
                }, 2000);
            }
            if (pageLevel == 'Level-2') {
                if (SecOneData.length > 0) {
                    if (document.getElementById('adsUEListingDetails_1').innerHTML != null) {
                        //document.getElementById('adsUEHome_1').innerHTML = "";
                        let html = ``;
                        for (let item of SecOneData) {
                            let c_id = item._id;
                            let l_id = item.adsLocationId;
                            let d_id = item.adsDetailsid;
                            let t_id = item.adsTypeId;
                            html += `
                        <div class="row" onclick=adsClick('${c_id}','${l_id}','${d_id}','${t_id}')> 
                        <div class="col-md-12">
                        <a href="${item.adsRefUrl}" target="_blank"><img src="${item.fileUrl}" alt="" class="img-responsive"></a>
                        <span><i title="${item.adsTittle}" style="background-color:#FFFFFF;color:#000000;text-decoration:none">i</i></span>
                        <input type = "button" onclick = "myfunction()" value = "x"> </div></div>`;
                        }
                        document.getElementById('adsUEListingDetails_1').innerHTML = html;
                    }

                }
            }
        })
}
fetchData();
setInterval(fetchData, 30000);

function fetchLocationData() {
    fetch('http://ip-api.com/json/')
        .then(response => {
            if (!response.ok) {
                throw new Error();
            }
            return response.json();
        })
        .then(result => {

            locationData = result;
        })
}
fetchLocationData();


function adsClick(c_id, l_id, d_id, t_id) {
    //console.log(c_id, l_id, d_id, t_id)
    const data = {
        "locationData": locationData,
        "adsCenterId": c_id,
        "adsLocationId": l_id,
        "adsDetailsId": d_id,
        "adsTypeId": t_id,
        "sourceUrl": window.location.origin,
        "destUrl": window.location.href
    };
    fetch('http://localhost:1677/api/v1/UE/ads-clicked', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        })
        .then(response => {
            if (!response.ok) {
                throw new Error();
            }
            return response.json();
        })
        .then(data => {
            //console.log('Success:', data);
        })
        .catch((error) => {
            ///console.error('Error:', error);
        });
}

// let js_fb_centerid, js_fb_adsdetailsid;

// function js_feedback_modal(c_id, d_id) {
//     $('#js_fb_tarea_other').hide();
//     document.getElementById("js_fb_rbtn1").checked  = false;
//     document.getElementById("js_fb_rbtn2").checked  = false;
//     document.getElementById("js_fb_rbtn3").checked  = false;
//     $("#ads_feedback").modal("show");
//     if(document.getElementById('drop-area').querySelector("img") != null){
//         let restoreForm = 
//         `<form class="my-form">
//         <p>Drag & drop your image <br />Or upload from your <a>computer</a> Allows *.jpeg, *.jpg and *.png - up to 2MB</p>
//         <input type="file" id="fileElem" accept="image/*">
//         <button>Browse File</button>
//       </form>`; 
//       feedbackfrm_dropArea.classList.add("js_img_drop");
//       document.getElementById('drop-area').innerHTML= restoreForm; 
//     }
//     js_fb_centerid = c_id;
//     js_fb_adsdetailsid = d_id;
// }
// function js_feedback_modal_cancel(){ //421303  6372284408 
//     //js_feedback_modal();
// }

// document.getElementById('js_fb_rbtn1').onclick = function () {
//     $('#js_fb_tarea_other').hide();
//     document.getElementById("js_fb_tarea_other").value = "";
// }
// document.getElementById('js_fb_rbtn2').onclick = function () {
//     $('#js_fb_tarea_other').hide();
//     document.getElementById("js_fb_tarea_other").value = "";
// }
// document.getElementById('js_fb_rbtn3').onclick = function () {
//     var check = document.getElementById("js_fb_rbtn3").checked;
//     if (check) {
//         $('#js_fb_tarea_other').show();
//     } else {
//         $('#js_fb_tarea_other').hide();
//         document.getElementById("js_fb_tarea_other").value = "";
//     }
// }
// document.getElementById('js_fb_validate').onclick = function () {
//     if ($("input[type=radio][name=js_fb_rbtn]").filter(":checked").length == 0) {
//         alert("Please select one of these options!");
//     } else {
//         if (document.getElementById("js_fb_rbtn3").checked == true) {
//             if (document.getElementById("js_fb_tarea_other").value == "") {
//                 alert("Please add your feedback.");
//             }
//         }
//     }
// }

// //Code For Ads Feedback Drag And Drop 

// //selecting all required elements
// const feedbackfrm_dropArea = document.getElementById('drop-area'),
// feedbackfrm_dropParagraphText = feedbackfrm_dropArea.querySelector("p"),
// feedbackfrm_dropButton = feedbackfrm_dropArea.querySelector("button"),
// feedbackfrm_dropInput = feedbackfrm_dropArea.querySelector("input");
// let file; //this is a global variable and we'll use it inside multiple functions
// function js_feedback_browseFile(){


// }
// feedbackfrm_dropButton.onclick = ()=>{
//     feedbackfrm_dropInput.click(); //if user click on the button then the input also clicked
// }

// feedbackfrm_dropInput.addEventListener("change", function(){
//     debugger;
//     //getting user select file and [0] this means if user select multiple files then we'll select only the first one
//     file = this.files[0];
//     //dropArea.classList.add("active");
//     showFile(); //calling function
//   });

//   function showFile(){
//     debugger;
//     let fileType = file.type; //getting selected file type
//     let validExtensions = ["image/jpeg", "image/jpg", "image/png"]; //adding some valid image extensions in array
//     if(validExtensions.includes(fileType)){ //if user selected file is an image file
//       let fileReader = new FileReader(); //creating new FileReader object
//       fileReader.onload = ()=>{
//           debugger;
//         let fileURL = fileReader.result; //passing user file source in fileURL variable
//           // UNCOMMENT THIS BELOW LINE. I GOT AN ERROR WHILE UPLOADING THIS POST SO I COMMENTED IT
//          let imgTag = `<img src="${fileURL}" alt="image">`; //creating an img tag and passing user selected file source inside src attribute
//          feedbackfrm_dropArea.innerHTML = imgTag; //adding that created img tag inside dropArea container
//          feedbackfrm_dropArea.classList.remove("js_img_drop");
//         }
//       fileReader.readAsDataURL(file);
//     }else{
//       alert("This is not an Image File!");
//      // dropArea.classList.remove("active");
//       //dragText.textContent = "Drag & Drop to Upload File";
//     }
//   }
  

//console.log(dropArea,dragText,button,input);






























// Open external links in a popup modal notice
// $(window).on('load', function(){
// 	debugger;
// 	$.expr[":"].external = function(a) {
// 		//var linkHref = a.hostname;
// 		//var domainHref = location.hostname;

// 		var linkhn = a.hostname.split('.').reverse();
// 		var linkHref = linkhn[1] + "." + linkhn[0];

// 		var domainhn = window.location.hostname.split('.').reverse();
// 		var domainHref = domainhn[1] + "." + domainhn[0];

// 		return !a.href.match(/^mailto\:/) && !a.href.match(/^tel\:/) && linkHref !== domainHref;
// 	};

// 	//$("a:external").addClass("ext_link");

// 	// $(function() {

// 	// 	$('a.ext_link').click(function() {
// 	// 		 // open a modal 
// 	// 		$('a:external').attr('data-toggle', 'modal');
// 	// 		$('a:external').attr('data-target', '#speedbump');
// 	// 		//go to link on modal close
// 	// 		var url = $(this).attr('href');
// 	// 		$('.btn-modal.btn-continue').click(function() {
// 	// 			window.open(url);
// 	// 			$('.btn-modal.btn-continue').off();
// 	// 		});
// 	// 		$('.btn-modal.btn-close').click(function() {
// 	// 			$('#speedbump').modal('hide');
// 	// 			$('.btn-modal.btn-close').off();
// 	// 		}); 
// 	// 	});
// <p class = "report_add">
//<a onclick=js_feedback_modal('${c_id}','${d_id}')>Report this Ad</a>
//</p> 
// 	// });  
// });